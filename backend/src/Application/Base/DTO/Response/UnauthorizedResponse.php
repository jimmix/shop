<?php

declare(strict_types=1);

namespace App\Application\Base\DTO\Response;

use Symfony\Component\HttpFoundation\Response;

class UnauthorizedResponse extends ErrorResponse
{
    public function __construct(int $code, string $message = 'Неверный токен или токен доступа устарел.')
    {
        parent::__construct($code, Response::HTTP_UNAUTHORIZED, $message);
    }
}
