<?php

declare(strict_types=1);

namespace App\Application\Base\DTO\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SuccessResponse extends JsonResponse
{
    public function __construct(array $data = [], int $status = Response::HTTP_OK)
    {
        parent::__construct($data, $status);
    }
}