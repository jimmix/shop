<?php

declare(strict_types=1);

namespace App\Application\Base\DTO\Response;

use Symfony\Component\HttpFoundation\Response;

class BadRequestResponse extends ErrorResponse
{
    public function __construct(array $errors = [])
    {
        parent::__construct(1000, Response::HTTP_BAD_REQUEST, 'Неверный запрос.', $errors);
    }
}
