<?php

declare(strict_types=1);

namespace App\Application\Base\DTO\Response;

use Symfony\Component\HttpFoundation\Response;

class InternalServerErrorResponse extends ErrorResponse
{
    public function __construct(string $message = 'Сервер временно недоступен.')
    {
        parent::__construct(5000, Response::HTTP_INTERNAL_SERVER_ERROR, $message);
    }
}