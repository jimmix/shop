<?php

namespace App\Application\Base\UseCase;

use App\Application\Auth\DTO\Request\ISignUpRequest;
use Symfony\Component\HttpFoundation\Response;

interface ISignUp
{
    public function execute(ISignUpRequest $signUpRequest): Response;
}