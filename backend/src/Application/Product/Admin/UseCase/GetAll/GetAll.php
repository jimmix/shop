<?php

declare(strict_types=1);

namespace App\Application\Product\Admin\UseCase\GetAll;

use App\Application\Product\Admin\DTO\Response\ProductDTO;
use App\Application\Product\Admin\DTO\Response\ProductsDTO;
use App\Core\Pagination\PaginationParams;
use App\Core\Pagination\SortParams;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class GetAll
{
    public function __construct(
        private readonly ProductRepository $productRepository
    ) {
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function execute(PaginationParams $paginationParams, SortParams $sortParams): ProductsDTO
    {
        $products = $this->productRepository->all($paginationParams, $sortParams);

        $productsDTO = array_map(
            function (Product $product) {
                $image = $product->getImage();
                $imagePath = $image?->getPath();

                return (new ProductDTO())
                    ->setId($product->getId())
                    ->setName($product->getName())
                    ->setSlug($product->getSlug())
                    ->setDiscount($product->getDiscount())
                    ->setDateDiscount($product->getDateDiscount())
                    ->setDescription($product->getDescription())
                    ->setBasePrice($product->getBasePrice())
                    ->setQuantity($product->getQuantity())
                    ->setImagePath($imagePath)
                    ->setPrice($product->getPrice())
                    ->setAvailableDays($product->getAvailableDays())
                    ->setWeight($product->getWeight());
            },
            $products->products
        );

        return new ProductsDTO($productsDTO, $products->pagination);
    }
}
