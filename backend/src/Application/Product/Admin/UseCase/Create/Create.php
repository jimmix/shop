<?php

declare(strict_types=1);

namespace App\Application\Product\Admin\UseCase\Create;

use App\Application\Product\Admin\DTO\Request\CreateRequest;
use App\Core\Doctrine\Flusher;
use App\Entity\Product;
use App\Repository\ProductRepository;
use DateTimeImmutable;
use Exception;

class Create
{
    public function __construct(
     private readonly Flusher $flusher,
     private readonly ProductRepository $productRepository
    ) {
    }

    /**
     * @throws Exception
     */
    public function execute(CreateRequest $productRequest): void
    {
        $product = (new Product())
            ->setName($productRequest->getName())
            ->setSlug($productRequest->getSlug())
            ->setDescription($productRequest->getDescription())
            ->setBasePrice($productRequest->getBasePrice())
            ->setDiscount($productRequest->getDiscount())
            ->setQuantity($productRequest->getQuantity())
            ->setWeight($productRequest->getWeight());

        if ($productRequest->getDateDiscount()) { //TODO: предусмотреть, что скидка заполнена
            $product->setDateDiscount(new DateTimeImmutable($productRequest->getDateDiscount()));
        }

        $this->productRepository->save($product);
        $this->flusher->flush();
    }
}