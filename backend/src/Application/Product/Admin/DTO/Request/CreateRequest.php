<?php

declare(strict_types=1);

namespace App\Application\Product\Admin\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CreateRequest
{
    #[Assert\Type(type: 'string', message: 'Имя должно содержать буквы и цифры')]
    #[Assert\NotBlank(message: 'Имя не может быть пустым')]
    private string $name;

    #[Assert\Type(type: 'string', message: 'Slug должен содержать буквы и цифры')]
    #[Assert\NotBlank(message: 'Поле не может быть пустым')]
    private string $slug;

    #[Assert\Type(type: 'string', message: 'Описание должно быть строкой.')]
    private ?string $description = null;

    #[Assert\Type(type: 'float', message: 'Базовая цена должна содержать число.')]
    #[Assert\NotBlank(message: 'Базовая цена не может быть пустой')]
    private float $basePrice;

    #[Assert\Type(type: 'float', message: 'Скидка должна быть числовым значением')]
    private ?float $discount = null;

    #[Assert\DateTime(message: 'Поле даты окончания скидки должно содержать дату и время')]
    private string $dateDiscount;

    #[Assert\Type(type: 'int', message: 'Поле количество товара должно содержать число')]
    private int $quantity;

    #[Assert\Type(type: 'float', message: 'Поле вес должно быть числовым значением')]
    private ?float $weight = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getBasePrice(): float
    {
        return $this->basePrice;
    }

    public function setBasePrice(float $basePrice): self
    {
        $this->basePrice = $basePrice;
        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDateDiscount(): string
    {
        return $this->dateDiscount;
    }

    public function setDateDiscount(string $dateDiscount): self
    {
        $this->dateDiscount = $dateDiscount;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }
}