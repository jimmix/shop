<?php

declare(strict_types=1);

namespace App\Application\Product\Admin\DTO\Response;

class ProductDTO
{
    private string $id;

    private string $name;

    private string $slug;

    private ?string $description = null;

    private float $basePrice;

    private ?float $discount = null;

    private ?\DateTimeImmutable $dateDiscount = null;

    private int $quantity;

    private ?float $weight = null;

    private ?string $image = null;

    private array $availableDays;

    private float $price;

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getBasePrice(): float
    {
        return $this->basePrice;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }


    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function setBasePrice(float $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }


    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    public function getDateDiscount(): ?\DateTimeImmutable
    {
        return $this->dateDiscount;
    }

    public function setDateDiscount(?\DateTimeImmutable $dateDiscount): self
    {
        $this->dateDiscount = $dateDiscount;
        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->image;
    }

    public function setImagePath(?string $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getAvailableDays(): array
    {
        return $this->availableDays;
    }

    public function setAvailableDays(array $availableDays): self
    {
        $this->availableDays = $availableDays;
        return $this;
    }


}
