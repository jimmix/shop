<?php

declare(strict_types=1);

namespace App\Application\Product\Admin\DTO\Response;

use App\Core\Pagination\Pagination;

class ProductsDTO
{
    /** @var ProductDTO[] */
    private array $products;
    private Pagination $pagination;

    /** @param ProductDTO[] $productDTO */
    public function __construct(array $productDTO, Pagination $pagination)
    {
        $this->products = $productDTO;
        $this->pagination = $pagination;
    }

    /** @return ProductDTO[] */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function toArray(): array
    {
        return [
            'items' => $this->products,
            'itemsPerPage' =>$this->pagination->itemsPerPage,
            'pageIndex' =>$this->pagination->pageIndex,
            'totalItems' =>$this->pagination->totalItems,
            'totalPage' =>$this->pagination->totalPages
        ];
    }
}