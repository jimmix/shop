<?php

namespace App\Application\Auth\UseCase\SignUp;

use App\Application\Auth\DTO\Request\ISignUpRequest;
use App\Application\Base\UseCase\ISignUp;
use App\Service\Auth\SignUpService;
use Symfony\Component\HttpFoundation\Response;

class SignUpPhone implements ISignUp
{
    public function __construct(private readonly SignUpService $signUpService)
    {
    }

    public function execute(ISignUpRequest $signUpRequest): Response
    {
        return $this->signUpService->signUpPhone($signUpRequest);
    }
}