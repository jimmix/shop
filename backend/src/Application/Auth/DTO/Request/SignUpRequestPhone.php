<?php

namespace App\Application\Auth\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;
use App\Core\Validator as CustomAssert;

class SignUpRequestPhone implements ISignUpRequest
{
    #[Assert\NotBlank]
    private string $name;

    #[Assert\NotBlank]
    #[CustomAssert\Phone]
    private string $phone;

    #[Assert\NotBlank]
    #[Assert\Length(min: 8,minMessage: "Пароль должен быть не менее 8 символов")]
    private string $password;

    #[Assert\NotBlank]
    #[Assert\EqualTo(propertyPath: 'password', message: "Пароль и подтверждение пароля не совпадают!")]
    private string $confirmPassword;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getConfirmPassword(): string
    {
        return $this->confirmPassword;
    }

    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;

        return $this;
    }
}
