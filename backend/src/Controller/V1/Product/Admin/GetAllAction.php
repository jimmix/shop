<?php

declare(strict_types=1);

namespace App\Controller\V1\Product\Admin;

use App\Application\Product\Admin\UseCase\GetAll\GetAll;
use App\Core\Pagination\PaginationParams;
use App\Core\Pagination\SortParams;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route(path: '/api/v1/products', methods: ['GET'])]
class GetAllAction extends AbstractController
{
    public function __construct(
        private readonly GetAll $getAll
    ) {
    }

    public function __invoke(
        PaginationParams $paginationParams,
        SortParams $sortParams,
        #[CurrentUser] ?User $user
    ): Response {
        return $this->json($this->getAll->execute($paginationParams, $sortParams)->toArray());
    }
}
