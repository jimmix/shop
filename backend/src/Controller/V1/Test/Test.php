<?php

declare(strict_types=1);

namespace App\Controller\V1\Test;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/v1/test', methods: ['POST'])]
class Test extends AbstractController
{
    /**
     * @throws \Exception
     */
    public function __invoke(): Response
    {
        return $this->json(['test' => '312']);
    }
}