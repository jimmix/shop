<?php

declare(strict_types=1);

namespace App\Controller\V1\Auth\SignUp;

use App\Application\Auth\DTO\Request\SignUpRequestPhone;
use App\Application\Base\UseCase\ISignUp;
use App\Core\ArgumentResolver\RequestResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/v1/auth/signUp/phone', methods: ['POST'])]
class SignUpPhone extends AbstractController
{
    public function __construct(
        private readonly ISignUp $signUp,
    ) {
    }

    public function __invoke(#[RequestResolver] SignUpRequestPhone $signUpRequest): Response
    {
        return $this->signUp->execute($signUpRequest);
    }
}