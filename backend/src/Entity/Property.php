<?php

namespace App\Entity;

use App\Core\ValueObject\Property\Type;
use App\Repository\PropertyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'property')]
#[ORM\HasLifecycleCallbacks]
class Property extends BaseEntity
{
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description;

    #[ORM\Column(type: 'string', length: 36, nullable: true)]
    private ?string $parentId;

    #[ORM\OneToMany(mappedBy: 'property', targetEntity: ProductProperty::class)]
    private Collection $products;

    #[ORM\Column(type: 'string', nullable: false, enumType: Type::class)]
    private Type $type;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $floatValue = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $intValue = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $stringValue = null;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private ?bool $checkbox = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $dateValue = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function setType(Type $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getCheckbox(): ?bool
    {
        return $this->checkbox;
    }

    public function setCheckbox(?bool $checkbox): self
    {
        $this->checkbox = $checkbox;
        return $this;
    }

    public function getFloatValue(): ?float
    {
        return $this->floatValue;
    }

    public function setFloatValue(?float $floatValue): self
    {
        $this->floatValue = $floatValue;
        return $this;
    }

    public function getIntValue(): ?int
    {
        return $this->intValue;
    }

    public function setIntValue(?int $intValue): self
    {
        $this->intValue = $intValue;
        return $this;
    }

    public function getStringValue(): ?string
    {
        return $this->stringValue;
    }

    public function setStringValue(?string $stringValue): self
    {
        $this->stringValue = $stringValue;
        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts(Collection $products): self
    {
        $this->products = $products;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    public function setParentId(?string $parentId): self
    {
        $this->parentId = $parentId;
        return $this;
    }

    public function getDateValue(): ?\DateTimeImmutable
    {
        return $this->dateValue;
    }

    public function setDateValue(?\DateTimeImmutable $dateValue): self
    {
        $this->dateValue = $dateValue;
        return $this;
    }
}