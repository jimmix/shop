<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'feedbacks_images')]
#[ORM\HasLifecycleCallbacks]
class FeedbackImage extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: Feedback::class, inversedBy: 'feedbackImages')]
    #[ORM\JoinColumn(name: 'feedback_id', referencedColumnName: 'id', nullable: true)]
    private ?Feedback $feedbacks;

    #[ORM\ManyToOne(targetEntity: Image::class, inversedBy: 'images')]
    #[ORM\JoinColumn(name: 'image_id', referencedColumnName: 'id', nullable: true)]
    private ?Image $images;

    public function getFeedbacks(): ?Feedback
    {
        return $this->feedbacks;
    }

    public function setFeedbacks(?Feedback $feedbacks): self
    {
        $this->feedbacks = $feedbacks;
        return $this;
    }
}
