<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'products_properties')]
#[ORM\HasLifecycleCallbacks]
class ProductProperty extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'productProperties')]
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id')]
    private Product $products;

    #[ORM\ManyToOne(targetEntity: Property::class, inversedBy: 'productProperties')]
    #[ORM\JoinColumn(name: 'property_id', referencedColumnName: 'id')]
    private Property $properties;

    public function getProducts(): Product
    {
        return $this->products;
    }

    public function setProducts(Product $products): self
    {
        $this->products = $products;
        return $this;
    }

    public function getProperties(): Property
    {
        return $this->properties;
    }

    public function setProperties(Property $properties): self
    {
        $this->properties = $properties;
        return $this;
    }


}
