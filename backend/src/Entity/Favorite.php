<?php

namespace App\Entity;

use App\Repository\FavoriteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'favorite')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Favorite extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'favorites')]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Product::class, fetch: 'EXTRA_LAZY', inversedBy: 'products')]
    private Product $product;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }
}
