<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'orders_products')]
#[ORM\HasLifecycleCallbacks]
class OrderProduct extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'orderProducts')]
    #[ORM\JoinColumn(name: 'product_id', referencedColumnName: 'id')]
    private Product $products;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'orderProducts')]
    #[ORM\JoinColumn(name: 'order_id', referencedColumnName: 'id')]
    private Order $orders;

    public function getProducts(): Product
    {
        return $this->products;
    }

    public function setProducts(Product $products): self
    {
        $this->products = $products;
        return $this;
    }

    public function getOrders(): Order
    {
        return $this->orders;
    }

    public function setOrders(Order $orders): self
    {
        $this->orders = $orders;
        return $this;
    }
}
