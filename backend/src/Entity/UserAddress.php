<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'users_addresses')]
#[ORM\HasLifecycleCallbacks]
class UserAddress extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userAddresses')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private User $users;

    #[ORM\ManyToOne(targetEntity: Address::class, inversedBy: 'userAddresses')]
    #[ORM\JoinColumn(name: 'address_id', referencedColumnName: 'id')]
    private Address $addresses;

    public function getUsers(): User
    {
        return $this->users;
    }

    public function setUsers(User $users): self
    {
        $this->users = $users;
        return $this;
    }

    public function getAddresses(): Address
    {
        return $this->addresses;
    }

    public function setAddresses(Address $addresses): self
    {
        $this->addresses = $addresses;
        return $this;
    }
}
