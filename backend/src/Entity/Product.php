<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sunrise\Slugger\Slugger;

#[ORM\Table(name: 'product')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Product extends BaseEntity
{
    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $slug;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description;

    #[ORM\Column(type: 'float', nullable: false)]
    private float $basePrice;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $discount;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $dateDiscount;

    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    private int $quantity = 0;

    #[ORM\Column(type: 'float', nullable: true)]
    private ?float $weight = null;

    #[ORM\Column(type: 'json', nullable: false)]
    private array $availableDays = [false, false, false, false, false, false, false];

    #[ORM\ManyToOne(targetEntity: Image::class, inversedBy: 'products')]
    private Image $image;

    #[ORM\OneToMany(mappedBy: "product", targetEntity: ProductProperty::class, fetch: 'EXTRA_LAZY')]
    private Collection $properties;

    #[ORM\OneToMany(mappedBy: "product", targetEntity: OrderProduct::class, fetch: 'EXTRA_LAZY')]
    private Collection $orders;

    #[ORM\OneToMany(mappedBy: "product", targetEntity: ProductCategory::class, fetch: 'EXTRA_LAZY')]
    private Collection $categories;

    #[ORM\OneToMany(mappedBy: "product", targetEntity: Favorite::class, fetch: 'EXTRA_LAZY')]
    private Collection $favorites;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->favorites = new ArrayCollection();
    }

    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    public function setFavorites(Collection $favorites): self
    {
        $this->favorites = $favorites;
        return $this;
    }

    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): self
    {
        $this->categories = $categories;
        return $this;
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function setOrders(Collection $orders): self
    {
        $this->orders = $orders;
        return $this;
    }

    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function setProperties(Collection $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    #[ORM\PrePersist]
    public function persistSlug(): self
    {
        $slugger = new Slugger();
        $salt = bin2hex(random_bytes(12));
        $this->slug = $slugger->slugify($this->name .'-'. $salt);

        return $this;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getBasePrice(): float
    {
        return $this->basePrice;
    }

    public function setBasePrice(float $basePrice): self
    {
        $this->basePrice = $basePrice;
        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDateDiscount(): ?DateTimeImmutable
    {
        return $this->dateDiscount;
    }

    public function setDateDiscount(?DateTimeImmutable $dateDiscount): self
    {
        $this->dateDiscount = $dateDiscount;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function setImage(Image $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function getAvailableDays(): array
    {
        return $this->availableDays;
    }

    public function setAvailableDays(array $availableDays): self
    {
        $this->availableDays = $availableDays;
        return $this;
    }

    public function getPrice(): ?float
    {
        $absoluteDiscount = $this->discount ? (100 - $this->discount) : 0;
        return $absoluteDiscount
            ? ($this->basePrice * $absoluteDiscount) / 100
            : $this->basePrice;
    }
}
