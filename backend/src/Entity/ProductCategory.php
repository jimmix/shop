<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'products_categories')]
#[ORM\HasLifecycleCallbacks]
class ProductCategory extends BaseEntity
{
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'productCategories')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $products;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'productCategories')]
    #[ORM\JoinColumn(nullable: false)]
    private Category $categories;

    public function getProducts(): ?Product
    {
        return $this->products;
    }

    public function setProducts(?Product $products): self
    {
        $this->products = $products;
        return $this;
    }

    public function getCategories(): Category
    {
        return $this->categories;
    }

    public function setCategories(Category $categories): self
    {
        $this->categories = $categories;
        return $this;
    }
}
