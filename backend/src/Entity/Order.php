<?php

namespace App\Entity;

use App\Core\ValueObject\Order\Type;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: '`order`')]
#[ORM\HasLifecycleCallbacks]
class Order extends BaseEntity
{
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $deliveryDate = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $plannedDate;

    #[ORM\Column(type: 'string', nullable: false, enumType: Type::class)]
    private Type $type;

    #[ORM\Column(type: 'string', nullable: false)]
    private string $status = 'status'; // TODO: enum

    #[ORM\Column(type: 'float', nullable: false)]
    private float $price;

    #[ORM\OneToMany(mappedBy: "order", targetEntity: OrderProduct::class)]
    private Collection $products;

    #[ORM\ManyToOne(targetEntity: Address::class, inversedBy: 'orders')]
    private Address $address;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'orders')]
    private ?User $user = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts(Collection $products): self
    {
        $this->products = $products;
        return $this;
    }

    public function getDeliveryDate(): ?DateTimeImmutable
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?DateTimeImmutable $deliveryDate): self
    {
        $this->deliveryDate = $deliveryDate;
        return $this;
    }

    public function getPlannedDate(): ?DateTimeImmutable
    {
        return $this->plannedDate;
    }

    public function setPlannedDate(?DateTimeImmutable $plannedDate): self
    {
        $this->plannedDate = $plannedDate;
        return $this;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function setType(Type $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }
}
