<?php

namespace App\Repository;

use App\Core\Exception\Favorite\FavoriteNotFoundException;
use App\Entity\Favorite;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class FavoriteRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Favorite> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Favorite::class);
    }

    /**
     * @throws FavoriteNotFoundException
     */
    public function getById(string $id): Favorite
    {
        $favorite = $this->repository->find($id);

        if (null === $favorite) {
            throw new FavoriteNotFoundException();
        }

        return $favorite;
    }

    /**
     * @throws FavoriteNotFoundException
     */
    public function get(array $criteria): Favorite
    {
        $favorite = $this->repository->findOneBy($criteria);

        if (null === $favorite) {
            throw new FavoriteNotFoundException();
        }

        return $favorite;
    }

    /**
     * @return Favorite[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Favorite $favorite): void
    {
        $this->em->persist($favorite);
    }

    public function exists(array $criteria): bool
    {
        $favorite = $this->repository->findOneBy($criteria);

        return null !== $favorite;
    }
}