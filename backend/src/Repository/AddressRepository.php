<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Address\AddressNotFoundException;
use App\Entity\Address;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class AddressRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Address> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Address::class);
    }

    /**
     * @throws AddressNotFoundException
     */
    public function getById(string $id): Address
    {
        $order = $this->repository->find($id);

        if (null === $order) {
            throw new AddressNotFoundException();
        }

        return $order;
    }

    /**
     * @return Address[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Address $address): void
    {
        $this->em->persist($address);
    }

    public function exists(array $criteria): bool
    {
        $address = $this->repository->findOneBy($criteria);

        return null !== $address;
    }

    public function existsByEmail(string $email): bool
    {
        return null !== $this->repository->findOneBy(['email' => $email]);
    }

    /**
     * @throws AddressNotFoundException
     */
    public function get(array $criteria): Address
    {
        $address =  $this->repository->findOneBy($criteria);

        if (null === $address) {
            throw new AddressNotFoundException();
        }

        return $address;
    }
}