<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Category\CategoryNotFoundException;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class CategoryRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Category> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Category::class);
    }

    /**
     * @throws CategoryNotFoundException
     */
    public function getById(string $id): Category
    {
        $category = $this->repository->find($id);

        if (null === $category) {
            throw new CategoryNotFoundException();
        }

        return $category;
    }

    /**
     * @throws CategoryNotFoundException
     */
    public function get(array $criteria): Category
    {
        $category = $this->repository->findOneBy($criteria);

        if (null === $category) {
            throw new CategoryNotFoundException();
        }

        return $category;
    }

    /**
     * @return Category[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Category $category): void
    {
        $this->em->persist($category);
    }

    public function exists(array $criteria): bool
    {
        $entity = $this->repository->findOneBy($criteria);

        return null !== $entity;
    }
}