<?php

namespace App\Repository;

use App\Core\Exception\Product\ProductNotFoundException;
use App\Core\Pagination\PaginationParams;
use App\Core\Pagination\Paginator;
use App\Core\Pagination\Product\PaginatedProducts;
use App\Core\Pagination\SortParams;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class ProductRepository extends BaseRepository
{
    use Paginator;

    protected EntityManagerInterface $em;

    /** @var EntityRepository<Product> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Product::class);
    }

    /**
     * @throws ProductNotFoundException
     */
    public function getById(string $id): Product
    {
        $product = $this->repository->find($id);

        if (null === $product) {
            throw new ProductNotFoundException();
        }

        return $product;
    }

    /**
     * @throws ProductNotFoundException
     */
    public function get(array $criteria): Product
    {
        $category = $this->repository->findOneBy($criteria);

        if (null === $category) {
            throw new ProductNotFoundException();
        }

        return $category;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function all(PaginationParams $paginationParams, SortParams $sortParams): PaginatedProducts
    {
        $qb = $this->repository->createQueryBuilder('p');

        $qb->select('p');
        $paginationParams->setOrder($sortParams->order)->setSort("p.$sortParams->sortedBy");
        $pagination = $this->paginate($qb, $paginationParams);

        $products = $qb->getQuery()->getResult();

        return new PaginatedProducts($products, $pagination);
    }

    public function save(Product $product): void
    {
        $this->em->persist($product);
    }

    public function exists(array $criteria): bool
    {
        $product = $this->repository->findOneBy($criteria);

        return null !== $product;
    }
}
