<?php

namespace App\Repository;

use App\Core\Exception\User\UserNotFoundException;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<User> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
    }

    /**
     * @throws UserNotFoundException
     */
    public function getById(string $id): User
    {
        $order = $this->repository->find($id);

        if (null === $order) {
            throw new UserNotFoundException();
        }

        return $order;
    }

    /**
     * @return User[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(User $user): void
    {
        $this->em->persist($user);
    }

    public function exists(array $criteria): bool
    {
        $user = $this->repository->findOneBy($criteria);

        return null !== $user;
    }

    public function existsByEmail(string $email): bool
    {
        return null !== $this->repository->findOneBy(['email' => $email]);
    }

    public function existsByPhoneNumber(string $phoneNumber): bool
    {
        return null !== $this->repository->findOneBy(['phoneNumber' => $phoneNumber]);
    }

    public function get(array $criteria): User
    {
        $user =  $this->repository->findOneBy($criteria);

        if (null === $user) {
            throw new UserNotFoundException();
        }

        return $user;
    }
}
