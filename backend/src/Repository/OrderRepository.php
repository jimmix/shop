<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Order\OrderNotFoundException;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Order> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Order::class);
    }

    /**
     * @throws OrderNotFoundException
     */
    public function getById(string $id): Order
    {
        $order = $this->repository->find($id);

        if (null === $order) {
            throw new OrderNotFoundException();
        }

        return $order;
    }

    /**
     * @throws OrderNotFoundException
     */
    public function get(array $criteria): Order
    {
        $category = $this->repository->findOneBy($criteria);

        if (null === $category) {
            throw new OrderNotFoundException();
        }

        return $category;
    }

    /**
     * @return Order[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Order $order): void
    {
        $this->em->persist($order);
    }

    public function exists(array $criteria): bool
    {
        $order = $this->repository->findOneBy($criteria);

        return null !== $order;
    }
}