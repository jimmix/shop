<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Feedback\FeedbackNotFoundException;
use App\Entity\Feedback;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class FeedbackRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Feedback> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Feedback::class);
    }

    /**
     * @throws FeedbackNotFoundException
     */
    public function getById(string $id): Feedback
    {
        $feedback = $this->repository->find($id);

        if (null === $feedback) {
            throw new FeedbackNotFoundException();
        }

        return $feedback;
    }

    /**
     * @throws FeedbackNotFoundException
     */
    public function get(array $criteria): Feedback
    {
        $category = $this->repository->findOneBy($criteria);

        if (null === $category) {
            throw new FeedbackNotFoundException();
        }

        return $category;
    }

    /**
     * @return Feedback[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Feedback $feedback): void
    {
        $this->em->persist($feedback);
    }

    public function exists(array $criteria): bool
    {
        $feedback = $this->repository->findOneBy($criteria);

        return null !== $feedback;
    }
}