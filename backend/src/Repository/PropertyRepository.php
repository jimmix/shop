<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Property\PropertyNotFoundException;
use App\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class PropertyRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Property> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Property::class);
    }

    /**
     * @throws PropertyNotFoundException
     */
    public function getById(string $id): Property
    {
        $property = $this->repository->find($id);

        if (null === $property) {
            throw new PropertyNotFoundException();
        }

        return $property;
    }

    /**
     * @throws PropertyNotFoundException
     */
    public function get(array $criteria): Property
    {
        $property = $this->repository->findOneBy($criteria);

        if (null === $property) {
            throw new PropertyNotFoundException();
        }

        return $property;
    }

    /**
     * @return Property[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Property $property): void
    {
        $this->em->persist($property);
    }

    public function exists(array $criteria): bool
    {
        $property = $this->repository->findOneBy($criteria);

        return null !== $property;
    }
}