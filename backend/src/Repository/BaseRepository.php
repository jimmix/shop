<?php

namespace App\Repository;

use App\Core\Doctrine\Filters\DeletedEnum;
use Doctrine\ORM\EntityManagerInterface;

abstract class BaseRepository
{
    public function __construct(protected EntityManagerInterface $em)
    {
    }

    protected function setFilterDeleted(string $flag): void
    {
        $filter = $this->em->getFilters()->enable(DeletedEnum::FALSE->value);
        $filter->setParameter('flag', $flag);
    }
}
