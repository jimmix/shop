<?php

declare(strict_types=1);

namespace App\Repository;

use App\Core\Exception\Image\ImageNotFoundException;
use App\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class ImageRepository extends BaseRepository
{
    protected EntityManagerInterface $em;

    /** @var EntityRepository<Image> */
    private EntityRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->em = $em;
        $this->repository = $this->em->getRepository(Image::class);
    }

    /**
     * @throws ImageNotFoundException
     */
    public function getById(string $id): Image
    {
        $image = $this->repository->find($id);

        if (null === $image) {
            throw new ImageNotFoundException();
        }

        return $image;
    }

    /**
     * @throws ImageNotFoundException
     */
    public function get(array $criteria): Image
    {
        $image = $this->repository->findOneBy($criteria);

        if (null === $image) {
            throw new ImageNotFoundException();
        }

        return $image;
    }

    /**
     * @return Image[]
     */
    public function all(): array
    {
        return $this->repository->findAll();
    }

    public function save(Image $image): void
    {
        $this->em->persist($image);
    }

    public function exists(array $criteria): bool
    {
        $image = $this->repository->findOneBy($criteria);

        return null !== $image;
    }
}