<?php

namespace App\Service\Auth;

use App\Application\Auth\DTO\Request\ISignUpRequest;
use App\Application\Auth\DTO\Request\SignUpRequestEmail;
use App\Application\Auth\DTO\Request\SignUpRequestPhone;
use App\Core\Doctrine\Flusher;
use App\Core\Exception\User\UserAlreadyExistsException;
use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SignUpService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $hasher,
        private readonly Flusher $flusher,
        private readonly AuthenticationSuccessHandler $successHandler
    ) {
    }

    /** @param SignUpRequestEmail $signUpRequest */
    public function signUpEmail(ISignUpRequest $signUpRequest): Response
    {
        if ($this->userRepository->existsByEmail($signUpRequest->getEmail())) {
            throw new UserAlreadyExistsException();
        }
        $user = (new User())
            ->setRoles()
            ->setName($signUpRequest->getName())
            ->setEmail($signUpRequest->getEmail())
            ->setPassword($signUpRequest->getPassword(), $this->hasher);

        $this->userRepository->save($user);
        $this->flusher->flush();

        return $this->successHandler->handleAuthenticationSuccess($user);
    }

    /** @param SignUpRequestPhone $signUpRequest */
    public function signUpPhone(ISignUpRequest $signUpRequest): Response
    {
        if ($this->userRepository->existsByPhoneNumber($signUpRequest->getPhone())) {
            throw new UserAlreadyExistsException();
        }

        $user = (new User())
            ->setRoles()
            ->setName($signUpRequest->getName())
            ->setPhoneNumber($signUpRequest->getPhone())
            ->setPassword($signUpRequest->getPassword(), $this->hasher);

        $this->userRepository->save($user);
        $this->flusher->flush();

        return $this->successHandler->handleAuthenticationSuccess($user);
    }
}
