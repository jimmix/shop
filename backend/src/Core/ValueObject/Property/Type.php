<?php

namespace App\Core\ValueObject\Property;

enum Type: string
{
    case Tag = 'Тэг';
    case Checkbox = 'Чекбокс';
    case Int = 'Целое число';
    case Float = 'Дробное число';
    case Text = 'Текст';
    case Parent = 'Подкатегория';
    case Date = 'Дата';
}