<?php

namespace App\Core\ValueObject\Order;

enum Type: string
{
    case Cashless = 'Оплата картой';
    case Cash = 'Наличный расчёт';
}
