<?php

namespace App\Core\Exception;

use RuntimeException;
use Throwable;

class RequestBodyConvertException extends RuntimeException
{
    public function __construct(Throwable $previous)
    {
        parent::__construct('Error parse request body', 0, $previous);
    }
}
