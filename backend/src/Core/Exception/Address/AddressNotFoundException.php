<?php

declare(strict_types=1);

namespace App\Core\Exception\Address;

class AddressNotFoundException extends \Exception
{
    protected $message = 'Адрес не найден';
    protected $code = 1401;
}