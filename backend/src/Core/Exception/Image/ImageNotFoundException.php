<?php

declare(strict_types=1);

namespace App\Core\Exception\Image;

class ImageNotFoundException extends \Exception
{
    protected $message = 'Товар не найден';
    protected $code = 1201;
}