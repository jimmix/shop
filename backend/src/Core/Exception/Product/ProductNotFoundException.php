<?php

namespace App\Core\Exception\Product;

class ProductNotFoundException extends \Exception
{
    protected $message = 'Товар не найден';
    protected $code = 1001;
}