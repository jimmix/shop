<?php

declare(strict_types=1);

namespace App\Core\Exception\Order;

class OrderNotFoundException extends \Exception
{
    protected $message = 'Заказ не найден';
    protected $code = 1501;
}