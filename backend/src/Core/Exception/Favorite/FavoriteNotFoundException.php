<?php

namespace App\Core\Exception\Favorite;

use Exception;

class FavoriteNotFoundException extends Exception
{
    protected $message = 'Избранный товар не найден';
    protected $code = 1701;
}