<?php

declare(strict_types=1);

namespace App\Core\Exception\Feedback;

class FeedbackNotFoundException extends \Exception
{
    protected $message = 'Отзыв не найден';
    protected $code = 1601;
}