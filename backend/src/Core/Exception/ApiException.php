<?php

namespace App\Core\Exception;

use RuntimeException;

class ApiException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct("Непредвиденная ошибка сервера", 500);
    }
}