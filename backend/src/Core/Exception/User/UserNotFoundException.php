<?php

namespace App\Core\Exception\User;
use RuntimeException;

class UserNotFoundException extends RuntimeException
{
    protected $message = 'Пользователь не найден';
    protected $code = 801;
}