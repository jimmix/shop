<?php

namespace App\Core\Exception\User;

use RuntimeException;

class UserAlreadyExistsException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct("Пользователь уже существует", 800);
    }
}