<?php

declare(strict_types=1);

namespace App\Core\Exception\Category;

class CategoryNotFoundException extends \Exception
{
    protected $message = 'Категория товара не найдена';
    protected $code = 1101;
}