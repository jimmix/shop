<?php

declare(strict_types=1);

namespace App\Core\Exception\Property;

class PropertyNotFoundException extends \Exception
{
    protected $message = 'Характеристика товара не найдена';
    protected $code = 1301;
}