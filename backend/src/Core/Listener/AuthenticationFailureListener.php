<?php

declare(strict_types=1);

namespace App\Core\Listener;

use App\Application\Base\DTO\Response\UnauthorizedResponse;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\Response;

#[AsEventListener(event: 'lexik_jwt_authentication.on_authentication_failure', method: 'onAuthenticationFailureResponse')]
class AuthenticationFailureListener
{
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event): void
    {
        $response = new UnauthorizedResponse(1002, 'Неверные учетные данные. Убедитесь, что ваши имя пользователя и пароль установлены правильно.');
        $event->setResponse($response);
    }
}
