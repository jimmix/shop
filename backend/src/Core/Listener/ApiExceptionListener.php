<?php

namespace App\Core\Listener;

use App\Application\Base\DTO\Response\ErrorResponse;
use App\Application\Base\DTO\Response\ForbiddenResponse;
use App\Core\ExceptionHandler\ExceptionMapping;
use App\Core\ExceptionHandler\ExceptionMappingResolver;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Throwable;

class ApiExceptionListener
{
    public function __construct(
        private readonly ExceptionMappingResolver $resolver,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function __invoke(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();
        if ($this->isSecurityException($throwable)) {
            return;
        }
        if ($throwable instanceof AccessDeniedException) {
            $response = new ForbiddenResponse();
            $event->setResponse($response);

            return;
        }

        $mapping = $this->resolver->resolve(get_class($throwable));

        if (null === $mapping) {
            $mapping = ExceptionMapping::fromCode(
                $throwable->getCode() ? $throwable->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        $this->logger->error($throwable->getMessage(), [
            'trace' => $throwable->getTraceAsString(),
            'previous' => null !== $throwable->getPrevious() ? $throwable->getPrevious()->getMessage() : '',
        ]);

        if ($mapping->getCode() >= Response::HTTP_INTERNAL_SERVER_ERROR || $mapping->isLoggable()) {
            $this->logger->error($throwable->getMessage(), [
                'trace' => $throwable->getTraceAsString(),
                'previous' => null !== $throwable->getPrevious() ? $throwable->getPrevious()->getMessage() : '',
            ]);
        }

        if ($throwable instanceof NotFoundHttpException) {
            return;
        }

        $message = $throwable->getMessage();
        $code =  $throwable->getCode();
        $status = $code < 200 || $code > 502 ? Response::HTTP_BAD_REQUEST : $code;
        $response = new ErrorResponse($code, $status, $message);
        $event->setResponse($response);
    }

    private function isSecurityException(Throwable $throwable): bool
    {
        return $throwable instanceof AuthenticationException;
    }
}
