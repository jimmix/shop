<?php

namespace App\Core\Listener;

use App\Application\Base\DTO\Response\ErrorResponse;
use App\Core\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class ValidationExceptionListener
{
    public function __invoke(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();
        if (!($throwable instanceof ValidationException)) {
            return;
        }

        $response = new ErrorResponse(code: 1005, message: $throwable->getMessage(), errors: ['violations' => $throwable->getViolation()]);
        $event->setResponse($response);
    }
}
