<?php

declare(strict_types=1);

namespace App\Core\Validator;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class Phone extends Constraint
{
    public string $message = 'Номер телефона {{ string }} имеет неправильный формат.';
}