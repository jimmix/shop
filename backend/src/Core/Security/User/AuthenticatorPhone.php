<?php

declare(strict_types=1);

namespace App\Core\Security\User;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\JsonLoginAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

class AuthenticatorPhone extends Authenticator implements InteractiveAuthenticatorInterface
{
    protected array $options = ['username_path' => 'phoneNumber', 'password_path' => 'password'];

    public function supports(Request $request): ?bool
    {
        try {
            $this->getCredentials($request);
        } catch (\Throwable) {
            return false;
        }
        return true;
    }
}