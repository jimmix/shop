<?php

namespace App\Core\Security\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Core\Exception\User\UserNotFoundException as UserNotFoundExceptionBase;

class JwtUserProvider implements PayloadAwareUserProviderInterface
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    // deprecated
    public function loadUserByUsernameAndPayload(string $username, array $payload): ?UserInterface
    {
        return null;
    }

    // session auth
    public function refreshUser(UserInterface $user): ?UserInterface
    {
        return null;
    }

    public function loadUserByIdentifierAndPayload(string $identifier, array $payload): UserInterface
    {
        return $this->getUser('id', $payload['id']);
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->getUser('id', $identifier);
    }

    private function getUser(string $key, string $value): UserInterface
    {
        try {
            $user = $this->userRepository->get([$key => $value]);
        } catch (UserNotFoundExceptionBase) {
            $e = new UserNotFoundException('Пользователь с значением '. json_encode($value) . 'не найден');
            $e->setUserIdentifier(json_encode($value));

            throw $e;
        }

        return $user;
    }
}