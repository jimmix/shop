<?php

declare(strict_types=1);

namespace App\Core\Security\User;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\InteractiveAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\JsonLoginAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

 abstract  class Authenticator implements InteractiveAuthenticatorInterface
{
    protected array $options = ['username_path' => 'email', 'password_path' => 'password'];

    public function __construct(
        private readonly JsonLoginAuthenticator $jsonLoginAuthenticator,
        private readonly UserRepository         $userRepository,
    ) {
    }

    public function authenticate(Request $request): Passport
    {
        try {
            $credentials = $this->getCredentials($request);
        } catch (BadRequestHttpException $e) {
            $request->setRequestFormat('json');

            throw $e;
        }
        return new Passport(
            new UserBadge($credentials[$this->options['username_path']], function (string $userIdentifier) use ($credentials) {
                return $this->userRepository->get([$this->options['username_path'] => $userIdentifier]);
            }),
            new PasswordCredentials($credentials[$this->options['password_path']])
        );
    }

    public function supports(Request $request): ?bool
    {
        return $this->jsonLoginAuthenticator->supports($request);
    }

    public function createToken(Passport $passport, string $firewallName): TokenInterface
    {
        return $this->jsonLoginAuthenticator->createToken($passport, $firewallName);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return $this->jsonLoginAuthenticator->onAuthenticationSuccess($request, $token, $firewallName);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return $this->jsonLoginAuthenticator->onAuthenticationFailure($request, $exception);
    }

    public function isInteractive(): bool
    {
        return true;
    }

    public function getCredentials(Request $request): array
    {
        $data = json_decode($request->getContent());
        if (!$data instanceof \stdClass) {
            throw new BadRequestHttpException('Invalid JSON.');
        }
        $data = (array) $data;

        if (empty($data[$this->options['username_path']]) || empty($data[$this->options['password_path']])) {
            throw new BadRequestHttpException('Не передан идентификатор пользователя или пароль');
        }

        return [
            $this->options['username_path'] => $data[$this->options['username_path']],
            $this->options['password_path'] => $data[$this->options['password_path']],
        ];
    }
}