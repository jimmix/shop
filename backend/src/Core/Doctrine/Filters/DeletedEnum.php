<?php

namespace App\Core\Doctrine\Filters;

enum DeletedEnum: string
{
    case TRUE = 'true';
    case FALSE = 'false';
    case ALL = 'all';
}
