<?php

declare(strict_types=1);

namespace App\Core\Doctrine\Filters;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class DeletedFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (!$this->hasParameter('flag'))
        {
            return $targetTableAlias.'.deleted = false';
        }

        if ($this->getParameter('flag') == DeletedEnum::ALL)
        {
            return "";
        }

        return $targetTableAlias.'.deleted = ' . $this->getParameter('flag');
    }
}