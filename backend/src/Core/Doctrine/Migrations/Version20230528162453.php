<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230528162453 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id UUID NOT NULL, state VARCHAR(255) DEFAULT NULL, locality VARCHAR(255) DEFAULT NULL, district VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, street VARCHAR(255) DEFAULT NULL, building VARCHAR(255) DEFAULT NULL, city_number VARCHAR(255) DEFAULT NULL, flat VARCHAR(255) DEFAULT NULL, house VARCHAR(255) DEFAULT NULL, house_dop VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN address.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN address.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN address.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE category (id UUID NOT NULL, name VARCHAR(255) NOT NULL, parent_id VARCHAR(255) DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN category.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN category.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN category.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE favorite (id UUID NOT NULL, user_id UUID DEFAULT NULL, product_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_68C58ED9A76ED395 ON favorite (user_id)');
        $this->addSql('CREATE INDEX IDX_68C58ED94584665A ON favorite (product_id)');
        $this->addSql('COMMENT ON COLUMN favorite.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN favorite.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN favorite.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN favorite.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN favorite.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE feedback (id UUID NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, estimation DOUBLE PRECISION NOT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN feedback.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN feedback.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN feedback.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE feedbacks_images (id UUID NOT NULL, feedback_id UUID DEFAULT NULL, image_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_26AC0E53D249A887 ON feedbacks_images (feedback_id)');
        $this->addSql('CREATE INDEX IDX_26AC0E533DA5256D ON feedbacks_images (image_id)');
        $this->addSql('COMMENT ON COLUMN feedbacks_images.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks_images.feedback_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks_images.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks_images.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN feedbacks_images.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE image (id UUID NOT NULL, path VARCHAR(255) NOT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN image.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN image.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN image.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "order" (id UUID NOT NULL, address_id UUID DEFAULT NULL, user_id UUID DEFAULT NULL, delivery_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, planned_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F5299398F5B7AF75 ON "order" (address_id)');
        $this->addSql('CREATE INDEX IDX_F5299398A76ED395 ON "order" (user_id)');
        $this->addSql('COMMENT ON COLUMN "order".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "order".address_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "order".user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "order".delivery_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "order".planned_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "order".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "order".updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE orders_products (id UUID NOT NULL, product_id UUID DEFAULT NULL, order_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_749C879C4584665A ON orders_products (product_id)');
        $this->addSql('CREATE INDEX IDX_749C879C8D9F6D38 ON orders_products (order_id)');
        $this->addSql('COMMENT ON COLUMN orders_products.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN orders_products.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN orders_products.order_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN orders_products.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN orders_products.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE product (id UUID NOT NULL, image_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, base_price DOUBLE PRECISION NOT NULL, discount DOUBLE PRECISION DEFAULT NULL, date_discount TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, quantity INT DEFAULT 0 NOT NULL, weight DOUBLE PRECISION DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04AD3DA5256D ON product (image_id)');
        $this->addSql('COMMENT ON COLUMN product.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN product.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN product.date_discount IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN product.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN product.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE products_categories (id UUID NOT NULL, products_id UUID NOT NULL, categories_id UUID NOT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E8ACBE766C8A81A9 ON products_categories (products_id)');
        $this->addSql('CREATE INDEX IDX_E8ACBE76A21214B7 ON products_categories (categories_id)');
        $this->addSql('COMMENT ON COLUMN products_categories.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_categories.products_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_categories.categories_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_categories.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN products_categories.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE products_images (id UUID NOT NULL, product_id UUID DEFAULT NULL, image_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_662C35404584665A ON products_images (product_id)');
        $this->addSql('CREATE INDEX IDX_662C35403DA5256D ON products_images (image_id)');
        $this->addSql('COMMENT ON COLUMN products_images.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN products_images.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE products_properties (id UUID NOT NULL, product_id UUID DEFAULT NULL, property_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_559CC9D94584665A ON products_properties (product_id)');
        $this->addSql('CREATE INDEX IDX_559CC9D9549213EC ON products_properties (property_id)');
        $this->addSql('COMMENT ON COLUMN products_properties.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_properties.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_properties.property_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_properties.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN products_properties.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE property (id UUID NOT NULL, name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, parent_id VARCHAR(36) DEFAULT NULL, type VARCHAR(255) NOT NULL, float_value DOUBLE PRECISION DEFAULT NULL, int_value INT DEFAULT NULL, string_value TEXT DEFAULT NULL, checkbox BOOLEAN DEFAULT NULL, date_value TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN property.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN property.date_value IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN property.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN property.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "refresh_tokens" (id INT NOT NULL, user_id UUID NOT NULL, refresh_token VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9BACE7E1A76ED395 ON "refresh_tokens" (user_id)');
        $this->addSql('COMMENT ON COLUMN "refresh_tokens".user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "refresh_tokens".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, roles JSON NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6496B01BC5B ON "user" (phone_number)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE users_addresses (id UUID NOT NULL, user_id UUID DEFAULT NULL, address_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9B70FF7A76ED395 ON users_addresses (user_id)');
        $this->addSql('CREATE INDEX IDX_9B70FF7F5B7AF75 ON users_addresses (address_id)');
        $this->addSql('COMMENT ON COLUMN users_addresses.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN users_addresses.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN users_addresses.address_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN users_addresses.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN users_addresses.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED9A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE favorite ADD CONSTRAINT FK_68C58ED94584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feedbacks_images ADD CONSTRAINT FK_26AC0E53D249A887 FOREIGN KEY (feedback_id) REFERENCES feedback (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feedbacks_images ADD CONSTRAINT FK_26AC0E533DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_products ADD CONSTRAINT FK_749C879C4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_products ADD CONSTRAINT FK_749C879C8D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_categories ADD CONSTRAINT FK_E8ACBE766C8A81A9 FOREIGN KEY (products_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_categories ADD CONSTRAINT FK_E8ACBE76A21214B7 FOREIGN KEY (categories_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_images ADD CONSTRAINT FK_662C35404584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_images ADD CONSTRAINT FK_662C35403DA5256D FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_properties ADD CONSTRAINT FK_559CC9D94584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_properties ADD CONSTRAINT FK_559CC9D9549213EC FOREIGN KEY (property_id) REFERENCES property (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "refresh_tokens" ADD CONSTRAINT FK_9BACE7E1A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF7A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_addresses ADD CONSTRAINT FK_9B70FF7F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE favorite DROP CONSTRAINT FK_68C58ED9A76ED395');
        $this->addSql('ALTER TABLE favorite DROP CONSTRAINT FK_68C58ED94584665A');
        $this->addSql('ALTER TABLE feedbacks_images DROP CONSTRAINT FK_26AC0E53D249A887');
        $this->addSql('ALTER TABLE feedbacks_images DROP CONSTRAINT FK_26AC0E533DA5256D');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398F5B7AF75');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398A76ED395');
        $this->addSql('ALTER TABLE orders_products DROP CONSTRAINT FK_749C879C4584665A');
        $this->addSql('ALTER TABLE orders_products DROP CONSTRAINT FK_749C879C8D9F6D38');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD3DA5256D');
        $this->addSql('ALTER TABLE products_categories DROP CONSTRAINT FK_E8ACBE766C8A81A9');
        $this->addSql('ALTER TABLE products_categories DROP CONSTRAINT FK_E8ACBE76A21214B7');
        $this->addSql('ALTER TABLE products_images DROP CONSTRAINT FK_662C35404584665A');
        $this->addSql('ALTER TABLE products_images DROP CONSTRAINT FK_662C35403DA5256D');
        $this->addSql('ALTER TABLE products_properties DROP CONSTRAINT FK_559CC9D94584665A');
        $this->addSql('ALTER TABLE products_properties DROP CONSTRAINT FK_559CC9D9549213EC');
        $this->addSql('ALTER TABLE "refresh_tokens" DROP CONSTRAINT FK_9BACE7E1A76ED395');
        $this->addSql('ALTER TABLE users_addresses DROP CONSTRAINT FK_9B70FF7A76ED395');
        $this->addSql('ALTER TABLE users_addresses DROP CONSTRAINT FK_9B70FF7F5B7AF75');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE favorite');
        $this->addSql('DROP TABLE feedback');
        $this->addSql('DROP TABLE feedbacks_images');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE orders_products');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE products_categories');
        $this->addSql('DROP TABLE products_images');
        $this->addSql('DROP TABLE products_properties');
        $this->addSql('DROP TABLE property');
        $this->addSql('DROP TABLE "refresh_tokens"');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE users_addresses');
    }
}
