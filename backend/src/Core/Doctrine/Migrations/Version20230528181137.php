<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230528181137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE products_images DROP CONSTRAINT fk_662c35404584665a');
        $this->addSql('ALTER TABLE products_images DROP CONSTRAINT fk_662c35403da5256d');
        $this->addSql('DROP TABLE products_images');
        $this->addSql('ALTER TABLE product ADD available_days JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE products_images (id UUID NOT NULL, product_id UUID DEFAULT NULL, image_id UUID DEFAULT NULL, deleted BOOLEAN DEFAULT false, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_662c35403da5256d ON products_images (image_id)');
        $this->addSql('CREATE INDEX idx_662c35404584665a ON products_images (product_id)');
        $this->addSql('COMMENT ON COLUMN products_images.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN products_images.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN products_images.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE products_images ADD CONSTRAINT fk_662c35404584665a FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE products_images ADD CONSTRAINT fk_662c35403da5256d FOREIGN KEY (image_id) REFERENCES image (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product DROP available_days');
    }
}
