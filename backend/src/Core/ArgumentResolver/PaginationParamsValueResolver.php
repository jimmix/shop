<?php

declare(strict_types=1);

namespace App\Core\ArgumentResolver;

use App\Core\Exception\ValidationException;
use App\Core\Pagination\PaginationParams;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PaginationParamsValueResolver implements ValueResolverInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly int $defaultPaginationLimit
    ) {
    }

    /**
     * @return PaginationParams[]
     *
     * @throws ValidationException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (PaginationParams::class !== $argument->getType()) {
            return [];
        }

        $this->validate($request);

        $paginationParams = new PaginationParams($this->defaultPaginationLimit);
        if ($request->query->getInt('page')) {
            $paginationParams->setPage($request->query->getInt('page'));
        }
        if ($request->query->getInt('limit')) {
            $paginationParams->setLimit($request->query->getInt('limit'));
        }

        return [$paginationParams];
    }

    /**
     * @throws ValidationException
     */
    private function validate(Request $request): void
    {
        $params = $request->query->all();

        $violations = $this->validator->validate(
            $params,
            new Assert\Collection(
                fields: [
                    'page' => new Assert\Optional([
                        new Assert\Type(type: 'numeric'),
                    ]),
                    'limit' => new Assert\Optional([
                        new Assert\Type(type: 'numeric'),
                    ]),
                ],
                allowExtraFields: true,
                allowMissingFields: false
            )
        );

        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }
    }
}
