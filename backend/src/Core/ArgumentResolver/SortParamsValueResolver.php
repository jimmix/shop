<?php

declare(strict_types=1);

namespace App\Core\ArgumentResolver;

use App\Core\Exception\ValidationException;
use App\Core\Pagination\SortParams;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SortParamsValueResolver implements ValueResolverInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @return SortParams[]
     *
     * @throws ValidationException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (SortParams::class !== $argument->getType()) {
            return [];
        }

        $this->validate($request);

        $sortBy = $request->query->get('sortedBy', 'createdAt');
        $order = $request->query->get('order', 'DESC');

        return [new SortParams($sortBy, $order)];
    }

    /**
     * @throws ValidationException
     */
    private function validate(Request $request): void
    {
        $params = $request->query->all();

        $violations = $this->validator->validate(
            $params,
            new Assert\Collection(
                fields: [
                    'sortedBy' => new Assert\Optional([
                        new Assert\Type(type: 'string'),
                        new Assert\Choice(choices: [
                            'name',
                            'slug',
                            'description',
                            'basePrice',
                            'discount',
                            'quantity',
                            'weight',
                            'dateDiscount',
                            'id',
                            'createdAt',
                            'updatedAt'
                        ], message: 'Выбранное Вами значение {{ value }} недопустимо. Доступные значения: {{ choices }}'),
                    ]),
                    'order' => new Assert\Optional([
                        new Assert\Type(type: 'string'),
                        new Assert\Choice(choices: ['ASC', 'DESC'], message: 'Выбранное Вами значение {{ value }} недопустимо. Доступные значения: {{ choices }}'),
                    ]),
                ],
                allowExtraFields: true,
                allowMissingFields: false
            )
        );

        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }
    }
}
