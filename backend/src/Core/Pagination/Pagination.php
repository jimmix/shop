<?php

declare(strict_types=1);

namespace App\Core\Pagination;

class Pagination
{
    public function __construct(
        public readonly int $pageIndex,
        public readonly int $totalPages,
        public readonly int $itemsPerPage,
        public readonly int $totalItems
    ) {
    }
}
