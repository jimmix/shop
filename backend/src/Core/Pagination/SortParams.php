<?php

declare(strict_types=1);

namespace App\Core\Pagination;

class SortParams
{
    public function __construct(
        public readonly string $sortedBy,
        public readonly string $order
    ) {
    }
}
