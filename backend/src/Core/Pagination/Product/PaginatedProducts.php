<?php

declare(strict_types=1);

namespace App\Core\Pagination\Product;

use App\Core\Pagination\Pagination;

class PaginatedProducts
{
    public function __construct(
        public readonly array $products,
        public readonly Pagination $pagination
    ) {
    }
}