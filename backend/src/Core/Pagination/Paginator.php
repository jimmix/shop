<?php

declare(strict_types=1);

namespace App\Core\Pagination;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

trait Paginator
{
    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    protected function paginate(
        QueryBuilder $qb,
        PaginationParams $paginationParams
    ): Pagination {
        $alias = $qb->getRootAliases()[0];
        $qbCount = clone $qb;

        /** @var int $total */
        $total = $qbCount->select("COUNT(DISTINCT $alias.id)")->getQuery()->getSingleScalarResult();
        $totalPages = ceil($total / $paginationParams->getLimit());

        $qb->setMaxResults($paginationParams->getLimit())
            ->setFirstResult($paginationParams->getOffset());

        if ($paginationParams->getSort()) {
            $qb->orderBy($paginationParams->getSort(), $paginationParams->getOrder());
        }

        return new Pagination($paginationParams->getPage(), (int) $totalPages, $paginationParams->getLimit(), $total);
    }

}