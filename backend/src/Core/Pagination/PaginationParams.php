<?php

declare(strict_types=1);

namespace App\Core\Pagination;

class PaginationParams
{
    private int $page = 1;
    private int $limit;
    private ?string $sort = null;
    private string $order = 'ASC';

    public function __construct(private readonly int $defaultPaginationLimit)
    {
        $this->limit = $this->defaultPaginationLimit;
    }

    public function setPage(int $page): self
    {
        if (0 == $page) {
            $this->page = 1;

            return $this;
        }

        $this->page = $page;

        return $this;
    }

    public function setLimit(int $limit): self
    {
        if ($limit > $this->defaultPaginationLimit) {
            $this->limit = $this->defaultPaginationLimit;

            return $this;
        }

        $this->limit = $limit;

        return $this;
    }

    public function setSort(string $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getOffset(): int
    {
        $offset = 0;

        if ($this->page > 1) {
            $offset = ($this->page - 1) * $this->limit;
        }

        return $offset;
    }

    public function getSort(): ?string
    {
        return $this->sort;
    }

    public function getOrder(): string
    {
        return $this->order;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getPage(): int
    {
        return $this->page;
    }
}
