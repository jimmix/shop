<?php

namespace App\Core\Hydrator;


use App\Core\ValueObject\Order\Type;
use Laminas\Hydrator\Strategy\StrategyInterface;

class TypeHydratorStrategy implements StrategyInterface
{
    public function extract($value, ?object $object = null): ?int
    {
        return null === $value ? null : Type::tryFrom($value)->value;
    }

    public function hydrate($value, ?array $data): ?Type
    {
        return null === $value ? null : Type::tryFrom($value);
    }

}