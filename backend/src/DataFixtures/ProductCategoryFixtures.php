<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductCategoryFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*  @var Product $banana1Product
         *  @var Product $banana2Product
         *  @var Product $apple1Product
         *  @var Product $apple2Product
         *  @var Product $mango1Product
         *  @var Product $mango2Product
         *  @var Product $cherry1Product
         *  @var Product $cherry2Product
         */
        $banana1Product = $this->getReference(ProductFixtures::BANANA1);
        $banana2Product = $this->getReference(ProductFixtures::BANANA2);

        $mango1Product = $this->getReference(ProductFixtures::MANGO_1);
        $mango2Product = $this->getReference(ProductFixtures::MANGO_2);

        $apple1Product = $this->getReference(ProductFixtures::APPLE1);
        $apple2Product = $this->getReference(ProductFixtures::APPLE2);

        $cherry1Product = $this->getReference(ProductFixtures::CHERRY_1);
        $cherry2Product = $this->getReference(ProductFixtures::CHERY_2);

        /**
         * @var Category $categoryBanana
         * @var Category $categoryApple
         * @var Category $categoryMango
         * @var Category $categoryCherry
         */
        $categoryBanana = $this->getReference(CategoryFixtures::BANANA);
        $categoryApple = $this->getReference(CategoryFixtures::BANANA);
        $categoryMango = $this->getReference(CategoryFixtures::MANGO);
        $categoryCherry = $this->getReference(CategoryFixtures::CHERRY);

        $productCategories = [
            (new ProductCategory())
                ->setProducts($banana1Product)
                ->setCategories($categoryBanana),
            (new ProductCategory())
                ->setProducts($banana2Product)
                ->setCategories($categoryBanana),

            (new ProductCategory())
                ->setProducts($apple1Product)
                ->setCategories($categoryApple),
            (new ProductCategory())
                ->setProducts($apple2Product)
                ->setCategories($categoryApple),

            (new ProductCategory())
                ->setProducts($mango1Product)
                ->setCategories($categoryMango),
            (new ProductCategory())
                ->setProducts($mango2Product)
                ->setCategories($categoryMango),

            (new ProductCategory())
                ->setProducts($cherry1Product)
                ->setCategories($categoryCherry),
            (new ProductCategory())
                ->setProducts($cherry2Product)
                ->setCategories($categoryCherry),
        ];

        foreach ($productCategories as $productCategory) {
            $manager->persist($productCategory);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ProductFixtures::class,
            CategoryFixtures::class
        ];
    }
}