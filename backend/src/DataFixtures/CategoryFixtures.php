<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public const FRUITS = 'fruits';
    public const APPLE = 'apple';
    public const BANANA = 'banana';
    public const MANGO = 'mango';
    public const CHERRY = 'cherry';

    public function load(ObjectManager $manager)
    {

        $fruits = (new Category())->setName('фрукты');
        $manager->persist($fruits);

        $categories = [
            self::APPLE =>  (new Category())->setName('Яблоки')->setParentId($fruits->getId()),
            self::BANANA =>(new Category())->setName('Бананы')->setParentId($fruits->getId()),
            self::MANGO => (new Category())->setName('Манго')->setParentId($fruits->getId()),
            self::CHERRY => (new Category())->setName('Вишня')->setParentId($fruits->getId())
        ];

        foreach ($categories as $category) {
            $manager->persist($category);
        }
        $manager->flush();

        foreach ($categories as $key => $category) {
            $this->addReference($key, $category);
        }
    }
}