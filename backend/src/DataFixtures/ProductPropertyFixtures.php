<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\ProductProperty;
use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductPropertyFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /*  @var Product $banana1Product
         *  @var Product $banana2Product
         *  @var Product $apple1Product
         *  @var Product $apple2Product
         *  @var Product $mango1Product
         *  @var Product $mango2Product
         *  @var Product $cherry1Product
         *  @var Product $cherry2Product
         */
        $banana1Product = $this->getReference(ProductFixtures::BANANA1);
        $banana2Product = $this->getReference(ProductFixtures::BANANA2);

        $mango1Product = $this->getReference(ProductFixtures::MANGO_1);
        $mango2Product = $this->getReference(ProductFixtures::MANGO_2);

        $apple1Product = $this->getReference(ProductFixtures::APPLE1);
        $apple2Product = $this->getReference(ProductFixtures::APPLE2);

        $cherry1Product = $this->getReference(ProductFixtures::CHERRY_1);
        $cherry2Product = $this->getReference(ProductFixtures::CHERY_2);

        /**
         * @var Property $colorRed
         * @var Property $colorBlue
         * @var Property $weight1000
         * @var Property $weight2000
         * @var Property $providerUlitkin
         * @var Property $dateStartNow
         * @var Property $delivery
         * @var Property $notDelivery
         */
        $colorRed = $this->getReference(PropertyFixtures::COLOR_RED);
        $colorBlue = $this->getReference(PropertyFixtures::COLOR_BLUE);
        $weight1000 = $this->getReference(PropertyFixtures::WEIGHT_1000);
        $weight2000 = $this->getReference(PropertyFixtures::WEIGHT_2000);
        $providerUlitkin = $this->getReference(PropertyFixtures::ULITKIN);
        $dateStartNow = $this->getReference(PropertyFixtures::NOW);
        $delivery = $this->getReference(PropertyFixtures::DELIVERY);
        $notDelivery = $this->getReference(PropertyFixtures::NOT_DELIVERY);

        $pp1 = (new ProductProperty())
            ->setProducts($banana1Product)
            ->setProperties($weight1000)
            ->setProperties($providerUlitkin)
            ->setProperties($delivery)
            ->setProperties($dateStartNow);

        $pp2 = (new ProductProperty())
            ->setProducts($banana2Product)
            ->setProperties($colorRed)
            ->setProperties($delivery)
            ->setProperties($weight1000);

        $pp3 = (new ProductProperty())
            ->setProducts($apple1Product)
            ->setProperties($notDelivery)
            ->setProperties($weight2000);

        $pp4 = (new ProductProperty())
            ->setProducts($apple2Product)
            ->setProperties($colorBlue)
            ->setProperties($notDelivery)
            ->setProperties($weight2000);

        $pp5 = (new ProductProperty())
            ->setProducts($mango1Product)
            ->setProperties($colorBlue)
            ->setProperties($dateStartNow);
        $pp6 = (new ProductProperty())
            ->setProducts($mango2Product)
            ->setProperties($dateStartNow)
            ->setProperties($providerUlitkin);

        $properties = [
            $pp1,
            $pp2,
            $pp3,
            $pp4,
            $pp5,
            $pp6,
        ];
        foreach ($properties as $property) {
            $manager->persist($property);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ProductFixtures::class,
            PropertyFixtures::class
        ];
    }
}