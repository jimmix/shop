<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AddressFixture extends Fixture
{
    public const HOME_ONE = 'one';

    public function load(ObjectManager $manager)
    {
        $one = (new Address())
            ->setCity('Новосибирск')
            ->setStreet('Новосибирская')
            ->setHouse('4')
            ->setDescription('description')
            ->setFlat('111');
        $manager->persist($one);
        $manager->flush();

        $this->addReference(self::HOME_ONE, $one);
    }
}