<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $hasher)
    {
    }
    public const ADMIN = 'admin';
    public function load(ObjectManager $manager)
    {
        $admin = (new User())
            ->setName('Администратор')
            ->setEmail('admin@shop.ru')
            ->setPhoneNumber('79588533431')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword('123456789', $this->hasher);

        $customer = (new User())
            ->setName('Клиент')
            ->setEmail('customer@shop.ru')
            ->setPhoneNumber('79588533432')
            ->setPassword('123456789', $this->hasher);

        $manager->persist($admin);
        $manager->persist($customer);
        $manager->flush();

        $this->addReference(self::ADMIN, $admin);
    }
}