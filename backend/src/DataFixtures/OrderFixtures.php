<?php

namespace App\DataFixtures;

use App\Core\ValueObject\Order\Type;
use App\Entity\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture
{
    public const ORDER_1 = 'order1';
    public const ORDER_2 = 'order2';
    public const ORDER_3 = 'order3';
    public const ORDER_4 = 'order4';
    public const ORDER_5 = 'order5';
    public const ORDER_6 = 'order6';

    public function load(ObjectManager $manager)
    {
        $order1 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('01.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('01.10.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(5000)
            ->setStatus('Завершен');

        $order2 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('02.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('02.10.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(3333)
            ->setStatus('Завершен');

        $order3 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('04.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('04.10.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(4444)
            ->setStatus('В пути');

        $order4 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('03.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('03.11.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(500)
            ->setStatus('Отклонен');

        $order5 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('05.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('05.11.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(520)
            ->setStatus('В пути');

        $order6 = (new Order())
            ->setDeliveryDate(new \DateTimeImmutable('06.10.2023 15:30'))
            ->setPlannedDate(new \DateTimeImmutable('06.10.2023 15:25'))
            ->setType(Type::Cashless)
            ->setPrice(10000)
            ->setStatus('В пути');

        $orders = [
            self::ORDER_1 => $order1,
            self::ORDER_2 => $order2,
            self::ORDER_3 => $order3,
            self::ORDER_4 => $order4,
            self::ORDER_5 => $order5,
            self::ORDER_6 => $order6,
        ];

        foreach ($orders as $order) {
            $manager->persist($order);
        }
        $manager->flush();

        foreach ($orders as $key => $order) {
            $this->addReference($key, $order);
        }
    }
}