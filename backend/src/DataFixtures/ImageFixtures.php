<?php

namespace App\DataFixtures;

use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ImageFixtures extends Fixture
{
    public const GREAT_IMAGE = 'great';
    public const CUSTOMER_IMAGE = 'customer';
    public const CAR_IMAGE = 'car';
    public const SPACE_IMAGE = 'space';

    public function load(ObjectManager $manager)
    {
        $images = [
            self::GREAT_IMAGE => (new Image())->setPath('https://i.ytimg.com/vi/r99Vg8hweqI/maxresdefault.jpg'),
            self::CUSTOMER_IMAGE => (new Image())->setPath('https://dirclub.ru/wp-content/uploads/9878-00.jpg'),
            self::CAR_IMAGE => (new Image())->setPath('https://5koleso.ru/wp-content/uploads/2023/01/autodealer_16.jpg'),
            self::SPACE_IMAGE => (new Image())->setPath('https://naukatehnika.com/files/vse_zhurnaly/2019/12.19/img-6.jpg'),
        ];

        foreach ($images as $image) {
            $manager->persist($image);
        }
        $manager->flush();

        foreach ($images as $key => $image) {
            $this->addReference($key, $image);
        }
    }
}
