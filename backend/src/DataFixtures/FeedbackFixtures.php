<?php

namespace App\DataFixtures;

use App\Entity\Feedback;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FeedbackFixtures extends Fixture implements DependentFixtureInterface
{

    public const POSITIVE_1 = 'positive_1';
    public const POSITIVE_2 = 'positive_2';
    public const NEGATIVE_1 = 'negative_1';
    public const NEGATIVE_2 = 'negative_2';

    public function load(ObjectManager $manager)
    {
        /** @var Image $greatImage */
        $greatImage = $this->getReference(ImageFixtures::GREAT_IMAGE);

        $positive1 = (new Feedback())
            ->setName('Какой позитивный отзыв')
            ->setEstimation(4.5)
            ->setImages(new ArrayCollection([$greatImage]))
            ->setDescription('Просто пушка товар отвечаю');

        $positive2 = (new Feedback())
            ->setName('Какой позитивный отзыв альтернативный')
            ->setEstimation(5)
            ->setImages(new ArrayCollection([$greatImage]))
            ->setDescription('Очень качественный товар, доставили вовремя, оставили скидку на следующую покупку!');


        $negative1 = (new Feedback())
            ->setName('Какой негативный отзыв')
            ->setEstimation(1)
            ->setImages(new ArrayCollection([$greatImage]))
            ->setDescription('Товар пришел поздно, курьер нахомил!');

        $negative2 = (new Feedback())
            ->setName('Негативный отзыв от анонимного покупателя')
            ->setEstimation(1)
            ->setImages(new ArrayCollection([$greatImage]))
            ->setDescription('Вместо трех бананов доставили две вишни, это моя первая и последняя покупка в данном магазине!');

        $feedbacks = [
            self::POSITIVE_1 => $positive1,
            self::POSITIVE_2 => $positive2,
            self::NEGATIVE_1 => $negative1,
            self::NEGATIVE_2 => $negative2
        ];

        foreach ($feedbacks as $feedback) {
            $manager->persist($feedback);
        }
        $manager->flush();

        foreach ($feedbacks as $key => $feedback) {
            $this->addReference($key, $feedback);
        }
    }

    public function getDependencies(): array
    {
        return [
            ImageFixtures::class,
        ];
    }
}