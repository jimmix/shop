<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public const BANANA1 = 'banana1';
    public const BANANA2 = 'banana2';
    public const APPLE1 = 'apple1';
    public const APPLE2 = 'apple2';
    public const MANGO_1 = 'mango1';
    public const MANGO_2 = 'mango2';
    public const CHERRY_1 = 'cherry1';
    public const CHERY_2 = 'cherry2';

    public function load(ObjectManager $manager)
    {
        $banana1 = (new Product())
            ->setName('Банан средней категории')
            ->setDescription('Бананы вкусный, средней категории')
            ->setBasePrice(100)
            ->setQuantity(123)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(1.4);

        $banana2 = (new Product())
            ->setName('Банан высшей категории')
            ->setDescription('Бананы вкусный, высшей категории')
            ->setBasePrice(150)
            ->setQuantity(123)
            ->setWeight(1.4)
            ->setDiscount(10)
            ->setAvailableDays([
                true,
                true,
                false,
                false,
                true,
                true,
                true
            ])
            ->setDateDiscount((new \DateTimeImmutable())->modify('+3 months'));

        $apple1 = (new Product())
            ->setName('Банан средней категории')
            ->setDescription('Яблоки вкусные, средней категории')
            ->setBasePrice(450)
            ->setQuantity(10)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(2);

        $apple2 = (new Product())
            ->setName('Банан высшей категории')
            ->setDescription('Яблоки вкусные, высшей категории')
            ->setBasePrice(700)
            ->setQuantity(123)
            ->setWeight(3.4)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setDiscount(15)
            ->setDateDiscount((new \DateTimeImmutable())->modify('+3 months'));

        $mango1 = (new Product())
            ->setName('Манго средней категории')
            ->setDescription('Манго очень вкусное, средней категории')
            ->setBasePrice(333)
            ->setQuantity(100)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(1);

        $mango2 = (new Product())
            ->setName('Манго высшей категории')
            ->setDescription('Манго очень вкусное, высшей категории')
            ->setBasePrice(400)
            ->setQuantity(30)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(0.5);

        $cherry1 = (new Product())
            ->setName('Вишня средней категории')
            ->setDescription('Вишня очень вкусная, средней категории')
            ->setBasePrice(110)
            ->setQuantity(13)
            ->setAvailableDays([
                false,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(1);

        $cherry2 = (new Product())
            ->setName('Вишня высшей категории')
            ->setDescription('Вишня очень вкусная, высшей категории')
            ->setBasePrice(240)
            ->setQuantity(133)
            ->setAvailableDays([
                true,
                true,
                true,
                true,
                true,
                true,
                true
            ])
            ->setWeight(2);

        $products = [
            self::BANANA1 => $banana1,
            self::BANANA2 => $banana2,
            self::APPLE1 => $apple1,
            self::APPLE2 => $apple2,
            self::MANGO_1 => $mango1,
            self::MANGO_2 => $mango2,
            self::CHERRY_1 => $cherry1,
            self::CHERY_2 => $cherry2
        ];

        foreach ($products as $product) {
            $manager->persist($product);
        }

        $manager->flush();

        foreach ($products as $key => $product) {
            $this->addReference($key, $product);
        }
    }
}
