<?php

namespace App\DataFixtures;

use App\Core\ValueObject\Property\Type;
use App\Entity\Property;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PropertyFixtures extends Fixture
{

    public const COLOR_RED = 'color_red';
    public const COLOR_BLUE = 'color_blue';
    public const WEIGHT_1000 = 'weight_1000';
    public const WEIGHT_2000 = 'weight_2000';
    public const ULITKIN = 'ulitkin';
    public const NOW = 'now';
    public const DELIVERY = 'delivery';
    public const NOT_DELIVERY = 'notDelivery';

    public function load(ObjectManager $manager)
    {
        $color = (new Property())
            ->setName('цвет')
            ->setType(Type::Parent);

        $weight = (new Property())
            ->setName('вес')
            ->setType(Type::Parent);

        $provider = (new Property())
            ->setName('Поставщик')
            ->setType(Type::Parent);

        $dateStart = (new Property())
            ->setName('Дата упаковки')
            ->setType(Type::Parent);

        $isDelivery = (new Property())
            ->setName('Есть доставка')
            ->setType(Type::Checkbox);

        $manager->persist($color);
        $manager->persist($weight);
        $manager->persist($provider);
        $manager->persist($dateStart);
        $manager->persist($isDelivery);

        $properties = [
            self::COLOR_RED => (new Property())
                ->setType(Type::Text)
                ->setParentId($color->getId())
                ->setStringValue('красный'),

            self::COLOR_BLUE => (new Property())
                ->setType(Type::Text)
                ->setParentId($color->getId())
                ->setStringValue('красный'),

            self::WEIGHT_1000 => (new Property())
                ->setDescription('Вес указан в киллограмах')
                ->setType(Type::Float)
                ->setParentId($weight->getId())
                ->setFloatValue(1),

            self::WEIGHT_2000 => (new Property())
                ->setDescription('Вес указан в киллограмах')
                ->setType(Type::Float)
                ->setParentId($weight->getId())
                ->setFloatValue(2),

            self::ULITKIN => (new Property())
                ->setStringValue('Улиткин дом')
                ->setType(Type::Text)
                ->setParentId($provider->getId()),

            self::NOW => (new Property())
                ->setDateValue((new \DateTimeImmutable()))
                ->setParentId($dateStart->getId())
                ->setType(Type::Date),

            self::NOT_DELIVERY => (new Property())
                ->setCheckbox(false)
                ->setParentId($isDelivery->getId())
                ->setType(Type::Checkbox),

            self::DELIVERY => (new Property())
                ->setCheckbox(true)
                ->setParentId($isDelivery->getId())
                ->setType(Type::Checkbox)
        ];

        foreach ($properties as $property) {
            $manager->persist($property);
        }
        $manager->flush();

        foreach ($properties as $key => $property) {
            $this->addReference($key,  $property);
        }
    }
}