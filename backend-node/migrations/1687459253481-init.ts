import { MigrationInterface, QueryRunner } from "typeorm"

export class Init1687459253481 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `alter table products add test varchar(150)`
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `alter table products drop column test`
        )
    }

}
