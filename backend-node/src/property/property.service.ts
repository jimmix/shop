import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Property} from "./property.entity";

@Injectable()
export class PropertyService {
    public constructor(
        @InjectRepository(Property)
        private readonly propertyRepository: Repository<Property>
    ) {
    }

    public create()
    {
        return this.propertyRepository.create({})
    }

    public async save(property: Property): Promise<Property>
    {
        return await this.propertyRepository.save(property)
    }
}
