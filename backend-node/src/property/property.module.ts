import { Module } from '@nestjs/common'
import { PropertyController } from './property.controller'
import { PropertyService } from './property.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "../product/product.entity"
import { Property } from "./property.entity"

@Module({
    controllers: [PropertyController],
    providers: [PropertyService],
    imports: [TypeOrmModule.forFeature([Product, Property])],
    exports: [TypeOrmModule]
})
export class PropertyModule {}
