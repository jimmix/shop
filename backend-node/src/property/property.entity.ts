import {Column, CreateDateColumn, Entity, JoinColumn, ManyToMany, ManyToOne} from "typeorm"
import { BaseEntity } from "../core/BaseEntity"
import { ApiProperty } from "@nestjs/swagger"
import { Product } from "../product/product.entity"

export type enumType = 'time'| 'link' | 'string' | 'boolean' | 'number' | undefined | null
@Entity('properties')
export class Property extends BaseEntity {
    @ApiProperty({example: 'Год'})
    @Column('varchar', {nullable: false})
    public name: string

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @CreateDateColumn({name: 'time_value', nullable: true, default: null})
    public timeValue?: Date

    @Column('text', {nullable: true, name: 'string_value'})
    public stringValue?: string

    @Column('boolean', {nullable: true, name: 'bool_value'})
    public boolValue?: boolean

    @Column('float', {nullable: true, name: 'number_value'})
    public numberValue?: number

    @Column('varchar', {nullable: false})
    public typeValue?: enumType

    @ManyToOne(() => Product, (product) => product.properties, {eager: true})
    @JoinColumn({name: 'product_id'})
    public product: Product
}
