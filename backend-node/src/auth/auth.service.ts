import { HttpException, HttpStatus, Injectable, UnauthorizedException } from "@nestjs/common"
import { RegistrationUserByEmailDto } from "./dto/RegistrationUserByEmailDto"
import { UsersService } from "../users/users.service"
import { JwtService } from "@nestjs/jwt"
import * as bcrypt from 'bcryptjs'
import { User } from "../users/users.entity"
import { LoginUserByEmailDto } from "./dto/LoginUserByEmailDto"
import { LoginUserByPhoneDto } from "./dto/LoginUserByPhoneDto"
import { RegistrationUserByPhoneDto } from "./dto/RegistrationUserByPhoneDto"
import { ValidateDtoUserByEmail } from "./dto/ValidateDtoUserByEmail"
import { ValidateDtoUserByPhone } from "./dto/ValidateDtoUserByPhone"
import { UserNotFoundException } from "../users/exceptions/userNotFoundException"
import { Repository } from "typeorm"
import { RefreshToken } from "./refresh.entity"
import { InjectRepository } from "@nestjs/typeorm/dist"

@Injectable()
export class AuthService {
    public constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService,
        @InjectRepository(RefreshToken)
        private readonly refreshTokenRepository: Repository<RefreshToken>
    ) {
    }

    public async loginByEmail(userDto: LoginUserByEmailDto): Promise<{token: string, refreshToken:string}> {
        const validateUser = new ValidateDtoUserByEmail(
            userDto.email,
            userDto.password
        )
        const user = await this.validateUserByEmail(validateUser)
        return this.generateToken(user)
    }

    public async loginByPhone(userDto: LoginUserByPhoneDto): Promise<{token: string, refreshToken: string}> {
        const validateUser = new ValidateDtoUserByPhone(
            userDto.phoneNumber,
            userDto.password
        )
        const user = await this.validateUserByPhone(validateUser)
        return this.generateToken(user)
    }

    public async registrationByEmail(userDto: RegistrationUserByEmailDto) {
        let candidate: User | null
        try {
            candidate = await this.userService.getUserByEmail(userDto.email)
        } catch (e) {
            if (!(e instanceof UserNotFoundException)) {
                throw new HttpException(
                    'Непредвиденная ошибка сервера',
                    HttpStatus.BAD_REQUEST
                )
            }

            candidate = null
        }

        if (candidate) {
            throw new HttpException(
                'Пользователь с таким email уже существует',
                HttpStatus.BAD_REQUEST
            )
        }

        if (candidate) {
            throw new HttpException(
                'Пользователь с таким email уже существует',
                HttpStatus.BAD_REQUEST
            )
        }

        const hashPassword = await bcrypt.hash(userDto.password, 5)
        const user = await this.userService.createUser({...userDto, password: hashPassword})
        return this.generateToken(user)
    }

    public async registrationByPhone(userDto: RegistrationUserByPhoneDto) {
        let candidate: User | null

        try {
            candidate = await this.userService.getUserByPhone(userDto.phoneNumber)
        } catch (e) {
            if (!(e instanceof UserNotFoundException)) {
                throw new HttpException(
                    'Непредвиденная ошибка сервера',
                    HttpStatus.BAD_REQUEST
                )
            }

            candidate = null
        }

        if (candidate) {
            throw new HttpException(
                'Пользователь с таким телефоном уже существует',
                HttpStatus.BAD_REQUEST
            )
        }

        const hashPassword = await bcrypt.hash(userDto.password, 5)
        const user = await this.userService.createUser({...userDto, password: hashPassword})
        return this.generateToken(user)
    }

    public generateToken(user: User): {token: string, refreshToken: string} {
        const payload = {
            email: user.email,
            id: user.id,
            roles: user.roles,
            name: user.name,
            imagePath: user?.imagePath,
            phone: user.phoneNumber
        }
        const token = this.jwtService.sign(payload, {expiresIn: '15s'})
        const refreshToken =  this.jwtService.sign(payload, {expiresIn: '30d'})
        const refreshTokenEntity = new RefreshToken()
        refreshTokenEntity.user = user
        refreshTokenEntity.refreshToken = refreshToken
        this.refreshTokenRepository.save(refreshTokenEntity)
        return {
            token,
            refreshToken
        }
    }

    private async validateUserByEmail(userDto: ValidateDtoUserByEmail): Promise<User> {
        const user = await this.userService.getUserByEmail(userDto.email)
        if(!user) {
            throw new UnauthorizedException({message: 'Пользователь с таким email не найден'})
        }

        const passwordEquals = await bcrypt.compare(userDto.password, user.password)
        if (passwordEquals) {
            return user
        }

        throw new UnauthorizedException({message: 'Неверный пароль'})
    }

    private async validateUserByPhone(userDto: ValidateDtoUserByPhone): Promise<User> {
        const user = await this.userService.getUserByEmail(userDto.phoneNumber)
        if(!user) {
            throw new UnauthorizedException({message: 'Пользователь с таким номером телефона не найден'})
        }
        const passwordEquals = await bcrypt.compare(userDto.password, user.password)
        if (user && passwordEquals) {
            return user
        }

        throw new UnauthorizedException({message: 'Неверный пароль'})
    }

    public async saveToken(user: User, refreshToken: string): Promise<void>
    {
        const refreshTokenEntity = new RefreshToken()
        refreshTokenEntity.user = user
        refreshTokenEntity.refreshToken = refreshToken
        await this.refreshTokenRepository.save<RefreshToken>(refreshTokenEntity)
    }

    public async refresh(refreshToken: string): Promise<{token: string, refreshToken: string}>
    {
        const refreshTokenEntity:  RefreshToken|null = await this.refreshTokenRepository.findOne({where: {
            refreshToken: refreshToken
        }})

        if (!refreshTokenEntity) {
            throw new HttpException('Ошибка авторизации, попробуйте войти снова', HttpStatus.UNAUTHORIZED);
        }

        const user = refreshTokenEntity.user

        const pairTokens = this.generateToken(user)
        await this.refreshTokenRepository.remove(refreshTokenEntity)

        return pairTokens
    }
}
