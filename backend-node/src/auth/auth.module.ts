import { forwardRef, Module } from "@nestjs/common"
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { UsersModule } from "../users/users.module"
import { JwtModule } from "@nestjs/jwt"
import * as process from "process"
import { User } from "src/users/users.entity"
import { TypeOrmModule } from "@nestjs/typeorm"
import { RefreshToken } from "./refresh.entity"

@Module({
    controllers: [AuthController],
    providers: [AuthService],
    imports: [
        forwardRef(() => UsersModule),
        JwtModule.register({
            secret: process.env.PRIVATE_KEY || 'SECRET'
        }),
        TypeOrmModule.forFeature([User, RefreshToken])
    ],
    exports: [
        AuthService,
        JwtModule
    ]
})
export class AuthModule {}
