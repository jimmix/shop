import { ApiProperty } from "@nestjs/swagger"
import { IsString, Length } from "class-validator"

export class LoginUserByPhoneDto {
    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    public password: string

    @ApiProperty({example: '79588533431'})
    @IsString({message: 'Должен быть строкой'})
    @Length(9, 12)
    public phoneNumber?: string
}
