import { ApiProperty } from "@nestjs/swagger"
import { IsPhoneNumber,} from "class-validator"

export class SendCodeByPhoneDto {
    @ApiProperty({example: '79588533431'})
    @IsPhoneNumber('RU', {
        message: 'Невалидный формат номера телефона. Пример: +79139139130'
      })
    public phoneNumber?: string
}
