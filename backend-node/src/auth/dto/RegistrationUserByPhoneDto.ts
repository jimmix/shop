import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsNumberString, IsOptional, IsPhoneNumber, IsString, Length, Validate, } from "class-validator"

export class RegistrationUserByPhoneDto {
    @ApiProperty({example: 'Василий Васильев'})
    @IsString({message: 'Должен быть строкой'})
    @Length(1, 255, {message: 'Длина имени должна быть от 1 до 255 символов'})
    public name: string

    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    public password: string

    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    @Validate((value: any, { object }: any) => value === object?.password && !value, {
        message: 'Пароли не совпадают',
      })
    public confirmPassword: string

    @ApiProperty({example: '79588533431'})
    @IsString({message: 'Должен быть строкой'})
    @IsPhoneNumber('RU', {
        message: 'Невалидный формат номера телефона. Пример: +79139139130'
      })
    public phoneNumber?: string

    @ApiProperty({example: 1111})
    @IsNumberString(undefined, {message: 'Формат кода ответа с телефона должен быть числом!'})
    @Length(4, 4, {message: 'Длина кода с телефона должна быть ровно 4 символа'})
    public code?: string
}
