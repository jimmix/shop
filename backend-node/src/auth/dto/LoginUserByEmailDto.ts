import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsString, Length } from "class-validator"

export class LoginUserByEmailDto {
    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    public password: string

    @IsString({message: 'Должен быть строкой'})
    @IsEmail({}, {message: 'некорректный email'})
    @ApiProperty({example: 'vasya@ya.ru'})
    public email?: string
}
