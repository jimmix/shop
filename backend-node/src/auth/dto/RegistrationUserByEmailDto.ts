import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsOptional, IsString, Length, Validate } from "class-validator"

export class RegistrationUserByEmailDto {
    @ApiProperty({example: 'Василий Васильев'})
    @IsString({message: 'Должен быть строкой'})
    @Length(1, 255)
    public name: string

    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    public password: string

    @ApiProperty({example: '1234324234'})
    @IsString({message: 'Должен быть строкой'})
    @Length(4, 16, {message: 'Пароль от 4 до 16 символов'})
    @Validate((value: any, { object }: any) => value === object?.password && !value, {
        message: 'Пароли не совпадают',
      })
    public confirmPassword: string

    @IsString({message: 'Должен быть строкой'})
    @IsEmail({}, {message: 'некорректный email'})
    @ApiProperty({example: 'vasya@ya.ru'})
    public email?: string

    @ApiProperty({example: '79588533431'})
    @IsOptional()
    @IsString({message: 'Должен быть строкой'})
    @Length(9, 12)
    public phoneNumber?: string
}
