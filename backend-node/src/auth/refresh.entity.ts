import { User } from "src/users/users.entity"
import {
    Column,
    Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn
} from "typeorm"

@Entity('refresh_token')
export class RefreshToken{
    @PrimaryGeneratedColumn('uuid')
    public id: string

    @Column('varchar', {nullable: false, name: 'refresh_token'})
    public refreshToken: string

    @ManyToOne(() => User, (user) => user.refreshTokens, {eager: true})
    @JoinColumn({name: 'user_id'})
    public user: User
}
