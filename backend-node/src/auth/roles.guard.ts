import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException } from "@nestjs/common"
import { Observable } from "rxjs"
import { JwtService } from "@nestjs/jwt"
import { Reflector } from "@nestjs/core"
import { ROLES_KEY } from "./roles-auth.decorator"

@Injectable()
export class RolesGuard implements CanActivate {
    public constructor(
        private jwtService: JwtService,
        private readonly reflector: Reflector
    ) {}

    public canActivate(context: ExecutionContext): boolean|Promise<boolean>|Observable<boolean> {
        const requiredRoles = this.reflector.getAllAndOverride<string[]>(ROLES_KEY, [
            context.getHandler(),
            context.getClass()
        ])

        if (!requiredRoles) {
            return true
        }

        try {
            const req = context.switchToHttp().getRequest()
            const bearer = req.headers.authorization.split(' ')[0]
            const token = req.headers.authorization.split(' ')[1]
            if (bearer !== 'Bearer' || !token) {
                throw new UnauthorizedException({
                    message: 'Пользователь не авторизован'
                })
            }

            const user = this.jwtService.verify(token)
            const isGranted = user.roles.some(role => requiredRoles.includes(role))
            if (false === isGranted) {
                throw new HttpException('Недостаточно прав', HttpStatus.FORBIDDEN)
            }

            return isGranted
        } catch (e) {
            if (e instanceof HttpException) {
                throw e;
            }
            
            throw new UnauthorizedException({message: 'Пользователь не авторизован'})
        }
    }
}
