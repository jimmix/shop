import { Body, Controller, HttpException, HttpStatus, Post, Req, Res, UsePipes } from "@nestjs/common"
import { ApiTags } from "@nestjs/swagger"
import { RegistrationUserByEmailDto } from "./dto/RegistrationUserByEmailDto"
import { AuthService } from "./auth.service"
import { LoginUserByEmailDto } from "./dto/LoginUserByEmailDto"
import { LoginUserByPhoneDto } from "./dto/LoginUserByPhoneDto"
import { RegistrationUserByPhoneDto } from "./dto/RegistrationUserByPhoneDto"
import { ValidationPipe } from "../core/pipes/validation.pipe"
import { SendCodeByPhoneDto } from "./dto/SendCodeByPhoneDto"
import { Request, Response } from "express"

@ApiTags('Авторизация')
@Controller('api/v1/auth')
export class AuthController {
    TIME_LIFE_TOKEN = 30 * 24 * 60 * 60 * 1000

    public constructor(private readonly authService: AuthService) {}

    @UsePipes(ValidationPipe)
    @Post('/login/email')
    public async loginByEmail(@Body() userDto: LoginUserByEmailDto, @Res() res: Response) {

        const token =  await this.authService.loginByEmail(userDto)
        res.cookie('refreshToken', token.refreshToken, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})

        res.cookie('accessToken', token.token, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})
        res.status(200)
        res.json(token)
    }
    @UsePipes(ValidationPipe)
    @Post('/login/phone')
    public async loginByPhone(@Body() userDto: LoginUserByPhoneDto) {
        return await this.authService.loginByPhone(userDto)
    }

    @UsePipes(ValidationPipe)
    @Post('/registration/email')
    public async registrationByEmail(@Body() userDto: RegistrationUserByEmailDto, @Res()res: Response) {
        const token =  await this.authService.registrationByEmail(userDto)
        res.cookie('refreshToken', token.refreshToken, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})
        res.cookie('accessToken', token.token, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})

        res.status(200)
        res.json(token)
    }

    @UsePipes(ValidationPipe)
    @Post('/registration/phone')
    public async registrationByPhone(@Body() userDto: RegistrationUserByPhoneDto) {
        if (userDto.code !== '1111') {
            throw new HttpException('Неверный код подтверждения',
                HttpStatus.NOT_ACCEPTABLE
            )
        }

        return await this.authService.registrationByPhone(userDto)
    }

    @UsePipes(ValidationPipe)
    @Post('/code')
    public sendCode(@Body() sendCodeDto: SendCodeByPhoneDto) {
        return {status: true}
    }

    @Post('/refresh')
    public async refresh(@Req() req: Request, @Res()res: Response) {
        const refreshToken: string|null = req.cookies?.refreshToken
        if (!refreshToken) {
            throw new HttpException('Ошибка авторизации, попробуйте войти в профиль.', HttpStatus.UNAUTHORIZED)
        }

        const pairTokens = await this.authService.refresh(refreshToken)
        res.cookie('refreshToken', pairTokens.refreshToken, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})
        res.cookie('accessToken', pairTokens.token, {maxAge: this.TIME_LIFE_TOKEN, httpOnly: true})

        res.status(200)
        res.json(pairTokens)
    }
}
