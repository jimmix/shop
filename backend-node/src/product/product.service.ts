import { Injectable } from '@nestjs/common'
import { InjectRepository } from "@nestjs/typeorm"
import { Repository } from "typeorm"
import { Product } from "./product.entity"
import { PaginationDto } from "./dto/PaginationDto"
import {  IFilterProduct } from "./dto/FilterDto"
import {CreateProductDto} from "./dto/CreateProductDto";

@Injectable()
export class ProductService {
    public constructor(
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>
    ) {
    }

    public findAll(paginationDto: PaginationDto, filterDto: IFilterProduct): Promise<Product[]>
    {
        const qb =  this.productRepository.createQueryBuilder('p')
            .take(paginationDto.limit)
            .innerJoinAndSelect('p.image', 'i')
            .skip(paginationDto.page -1)

        if (filterDto?.availableDays) {
            qb.andWhere('p.availableDays & :bit = :bit', {bit: filterDto.availableDays})
        }

        if (filterDto.order && filterDto.sort) {
            qb.orderBy('p.' + filterDto.order, filterDto.sort)
        }

        if (filterDto.categoryId) {
            qb.innerJoin(
                'p.category', 'category',
                'category.id = :categoryId or category.parentId = :categoryId',
                {categoryId: filterDto.categoryId}
            )
        }

        return qb.getMany()
    }

    public async create(dto: CreateProductDto): Promise<Product>
    {
        const product = this.productRepository.create(dto)
        return this.productRepository.save(product)
    }
}
