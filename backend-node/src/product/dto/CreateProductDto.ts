import {Category} from "../../category/category.entity";
import {Property} from "../../property/property.entity";
import {Image} from "../../image/image.entity";

export interface ImageItem {
    buffer: ArrayBuffer
    originalname: string
}
export class CreateProductDto {
    public name: string;
    public description?: string
    public basePrice?: number
    public provider?: string
    public discount?: number
    public quantity: number
    public weight?: number
    public typeWeight?: string
    public createdAt: Date
    public updatedAt: Date
    public category?: Category
    public properties: Property[]
    public image?: Image
    public availableDays: number
    public skladId: string
    public structure?: string
    public protein?: number
    public fats?: number
    public carbs?: number
}
