import { ApiProperty } from "@nestjs/swagger"
import {  Max, Min } from "class-validator"
import { Type } from "class-transformer"

export class PaginationDto {
    @ApiProperty({example: 'Молочные продукты'})
    @Min(1)
    @Type(() => Number)
    @Max(20)
    public readonly limit: number = 20

    @ApiProperty({example: 'fffdd269-336e-466a-9b22-edfdb85b50fc', required: false, nullable: true})
    @Min(1)
    @Type(() => Number)
    @Max(20)
    public readonly page?: number = 1
}
