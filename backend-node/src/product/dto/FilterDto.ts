import { IsIn, IsOptional, IsUUID, Max, Min, ValidateNested } from "class-validator"
import { Type } from "class-transformer"

export class IFilterProduct {
    @IsUUID()
    @IsOptional()
    public readonly categoryId?: string = null

    @Min(1)
    @Type(() => Number)
    @Max(127)
    @IsOptional()
    public readonly availableDays?: number = null

    @IsOptional()
    @IsIn(['createdAt', 'updatedAt', 'name', 'quantity', 'discount'])
    @Type(() => String)
    public readonly order?: string = null

    @IsIn(['ASC', 'DESC'])
    @IsOptional()
    public readonly sort?: 'ASC' | 'DESC' = null
}

export class FilterDto {
    @Type(() => IFilterProduct)
    @IsOptional()
    @ValidateNested({each: true})
    public readonly filters?: IFilterProduct = {}
}
