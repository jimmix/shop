import { Controller, Get, Query, UsePipes, ValidationPipe } from "@nestjs/common"
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger"
import { Product } from "./product.entity"
import { ProductService } from "./product.service"
import { PaginationDto } from "./dto/PaginationDto"
import { FilterDto } from "./dto/FilterDto"

@ApiTags('Товары')
@Controller('api/v1/products')
export class ProductController {
    public constructor(
        public readonly productService: ProductService
    ) {
    }

    @ApiOperation({summary: 'Получить список товаров'})
    @ApiResponse({status: 200, type: [Product]})
    @Get()
    @UsePipes(ValidationPipe)
    public async allAction(
        @Query() dto: PaginationDto,
        @Query() filterDto: FilterDto

    ): Promise<Product[]> {
        return this.productService.findAll(dto, filterDto.filters)
    }
}
