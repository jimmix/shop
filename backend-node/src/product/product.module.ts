import { Module } from '@nestjs/common'
import { ProductController } from './product.controller'
import { ProductService } from './product.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "./product.entity"
import { Category } from "../category/category.entity"
import { Property } from "../property/property.entity"
import { Image } from "../image/image.entity"
import { Order } from "../order/order.entity"

@Module({
    controllers: [ProductController],
    providers: [ProductService],
    imports: [TypeOrmModule.forFeature([Product, Category, Property, Image, Order])],
    exports: [TypeOrmModule]
})
export class ProductModule {}
