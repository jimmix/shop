import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm"
import { ApiProperty } from "@nestjs/swagger"
import { BaseEntity } from "../core/BaseEntity"
import { Category } from "../category/category.entity"
import { Property } from "../property/property.entity"
import { Image } from "../image/image.entity"
import { Order } from "../order/order.entity"

@Entity('products')
export class Product extends BaseEntity{
    @ApiProperty({example: 'fffdd269-336e-466a-9b22-edfdb85b50fc'})
    @PrimaryGeneratedColumn('uuid')
    public id: string

    @ApiProperty({example: 'Голень филе ципленка', description: 'Пустым быть не может'})
    @Column('varchar', {nullable: false, length: 1000})
    public name: string

    @ApiProperty({example: 'Вкусная голень филе ципленка'})
    @Column('text', {nullable: true})
    public description?: string

    @ApiProperty({example: 100.5, description: 'Базовая цена товара без скидок.'})
    @Column('float', {name: 'base_price'})
    public basePrice: number

    @ApiProperty({example: 5.5, description: 'Скидка на товар в процентах'})
    @Column('float', {nullable: true})
    public discount?: number

    @ApiProperty({example: 15, description: 'Количество товара на складе'})
    @Column('int', {nullable: false})
    public quantity: number

    @ApiProperty({example: 150.5, description: 'Вес товара'})
    @Column('float', {nullable: true})
    public weight?: number

    @ApiProperty({example: 'кг', description: 'Тип веса товара'})
    @Column('varchar', {nullable: false, length: 1000})
    public typeWeight: string

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @CreateDateColumn({name: 'created_at'})
    public createdAt: Date

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @UpdateDateColumn({name: 'updated_at'})
    public updatedAt: Date

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @DeleteDateColumn()
    public deleted: Date

    @ManyToOne(() => Category, (category) => category.products, {
        eager: true
    })
    @JoinColumn({name: 'category_id'})
    public category: Category

    @OneToMany(() => Property, (property: Property) => property.product)
    @JoinTable({name: 'products_properties'})
    public properties: Property[]

    @ManyToOne(() => Image, (image: Image) => image.products, {
        eager: true
    })
    @JoinColumn({ name: 'image_id' })
    public image: Image

    @ManyToMany(() => Order, order => order.products)
    public orders: Order[]

    @ApiProperty({example: '0de54d8c-cb74-49e5-85b1-2a0f5d8d1dd2', description: 'ID с мой склад'})
    @Column('varchar', {nullable: false, length: 40})
    public skladId: string

    @ApiProperty({example: 'Фер-мир', description: 'поставщик'})
    @Column('varchar', {nullable: true, length: 1000})
    public provider?: string

    @ApiProperty({example: 'Мясо, кости', description: 'Состав'})
    @Column('text', {nullable: true})
    public structure?: string

    @ApiProperty({example: 100, description: 'белки' })
    @Column('float', {nullable: true})
    public protein?: number

    @ApiProperty({example: 100, description: 'жиры' })
    @Column('float', {nullable: true})
    public fats?: number

    @ApiProperty({example: 100, description: 'углеводы' })
    @Column('float', {nullable: true})
    public carbs?: number

    @ApiProperty({
        example: 127,
        description: 'Доступные дни доставки'
    })
    @Column('int', {nullable: false, name: 'available_days', default: 0})
    /**
     * см. побитовое "И" .
     * 1 - понедельник
     * 2 - вторник
     * 4 - среда
     * 8 - четверг
     * 16- пятница
     * 32- суббота
     * 64- воскресенье
     */
    public availableDays: number
}
