import { Module } from '@nestjs/common'
import { FeedbackController } from './feedback.controller'
import { FeedbackService } from './feedback.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "../product/product.entity"
import { User } from "../users/users.entity"
import { Feedback } from "./feedback.entity"
import { Image } from "../image/image.entity"

@Module({
    controllers: [FeedbackController],
    providers: [FeedbackService],
    imports: [TypeOrmModule.forFeature([Product, User, Feedback, Image])],
    exports: [TypeOrmModule]
})
export class FeedbackModule {}
