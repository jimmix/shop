import { BaseEntity } from "../core/BaseEntity"
import { ApiProperty } from "@nestjs/swagger"
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from "typeorm"
import { Image } from "../image/image.entity"
import { User } from "../users/users.entity"

@Entity('feedbacks')
export class Feedback extends BaseEntity {
    @ApiProperty({example: 'Заголовок отзыва'})
    @Column('varchar', {nullable: true})
    public name: string

    @ApiProperty({example: 'описание отзыва'})
    @Column('text', {nullable: true})
    public description?: string

    @ApiProperty({example: 3.5})
    @Column("float")
    public estimation: number

    @ApiProperty({type: () => User})
    @ManyToOne(() => User, (user: User) => user.feedbacks)
    @JoinColumn({ name: 'user_id' })
    public user: User

    @ApiProperty({type: () => [Image]})
    @ManyToMany(() => Image, {onDelete: 'SET NULL'})
    @JoinTable({name: 'feedbacks_images'})
    public images: Image[]
}
