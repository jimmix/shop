import { CoreException } from "../../core/exception/coreException"

export class UserNotFoundException extends CoreException {
    public constructor(message = 'Пользователь не найден') {
        super(message)
    }
}
