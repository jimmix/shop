import { forwardRef, Module } from "@nestjs/common"
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { User } from "./users.entity"
import { AuthModule } from "../auth/auth.module"
import { Feedback } from "../feedback/feedback.entity"
import { Product } from "../product/product.entity"
import { FilesService } from "src/core/files/files.service"
import { AuthService } from "src/auth/auth.service"
import { RefreshToken } from "src/auth/refresh.entity"

@Module({
    controllers: [UsersController,],
    imports: [
        forwardRef(() => AuthModule),
        TypeOrmModule.forFeature([User, Feedback, Product, RefreshToken])
    ],
    exports: [
        TypeOrmModule,
        UsersService
    ],
    providers: [UsersService, FilesService, AuthService],
})
export class UsersModule {}
