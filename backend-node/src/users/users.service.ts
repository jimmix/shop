import { Injectable } from '@nestjs/common'
import { InjectRepository } from "@nestjs/typeorm"
import { User } from "./users.entity"
import { Repository } from "typeorm"
import { RegistrationUserByEmailDto } from "../auth/dto/RegistrationUserByEmailDto"
import { UserNotFoundException } from "./exceptions/userNotFoundException"
import { FilesService } from 'src/core/files/files.service'
import { UpdateUserDto } from './dto/UpdateUserDto'

@Injectable()
export class UsersService {
    public constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly fileService: FilesService
    ) {
    }

    public findAll(): Promise<User[]>
    {
        return this.userRepository.find()
    }

    public findOne(id: string): Promise<User | null>
    {
        return this.userRepository.findOneBy({id})
    }

    public async remove(id: string): Promise<void>
    {
        await this.userRepository.softDelete(id)
    }

    public async createUser(dto: RegistrationUserByEmailDto): Promise<User> {
        const user = this.userRepository.create(dto)
        return await this.userRepository.save<User>(user)
    }

    public async getUserByEmail(email: string): Promise<User> {
        const user = await this.userRepository.findOne({where: {email}})
        if (!(user instanceof User)) {
            throw new UserNotFoundException()
        }

        return user
    }

    public async getUserByPhone(phoneNumber: string): Promise<User> {
        const user = await this.userRepository.findOne({where: {phoneNumber}})
        if (!(user instanceof User)) {
            throw new UserNotFoundException()
        }

        return user
    }

    public async updateUser(user: User, image: any, dto: UpdateUserDto): Promise<User>
    {
        const imagePath = image
            ? await this.fileService.createFile(image)
            : null
        user.imagePath = imagePath
        user.phoneNumber = dto.phoneNumber
        
        await this.userRepository.save(user)
        return user
    }
}
