import { ApiProperty } from "@nestjs/swagger"
import { IsOptional, IsPhoneNumber, IsString, Length, } from "class-validator"

export class UpdateUserDto {
  @IsOptional()
    @ApiProperty({example: 'Ул. Ленина, д 4, кв 14'})
    @IsString({message: 'Должен быть строкой'})
    @Length(10, 1000, {message: 'Адрес должен быть длиной от 10 до 1000 символов'})
    public address: string

    @IsOptional()
    @ApiProperty({example: '79588533431'})
    @IsString({message: 'Должен быть строкой'})
    @IsPhoneNumber('RU', {
        message: 'Невалидный формат номера телефона. Пример: +79139139130'
      })
    public phoneNumber?: string

    @IsOptional()
    public image?: any
}
