import { Body, Controller, Get, HttpException, HttpStatus, Param, Post, Put, Req, UploadedFile, UseGuards, UseInterceptors } from "@nestjs/common"
import { UsersService } from "./users.service"
import { User } from "./users.entity"
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger"
import { JwtAuthGuard } from "../auth/jwt-auth.guard"
import { Roles } from "../auth/roles-auth.decorator"
import { RolesGuard } from "../auth/roles.guard"
import { FileFieldsInterceptor, FileInterceptor } from "@nestjs/platform-express"
import { UpdateUserDto } from "./dto/UpdateUserDto"
import { AuthService } from "src/auth/auth.service"

@ApiTags('Пользователи')
@Controller('api/v1/users')
export class UsersController {
    public constructor(
        public readonly userService: UsersService,
        public readonly authService: AuthService
    ) {
    }

    @ApiOperation({summary: 'Получить список пользователей'})
    @ApiResponse({status: 200, type: [User]})
    @UseGuards(JwtAuthGuard)
    @Roles('ROLE_USER')
    @UseGuards(RolesGuard)
    @Get()
    public async allAction(): Promise<User[]> {
        return await this.userService.findAll()
    }

    @Put()
    @ApiResponse({status: 200})
    @UseGuards(JwtAuthGuard)
    @UseInterceptors(FileInterceptor('image'))
    public async createCategory(
        @Body() dto: UpdateUserDto,
        @Req() req,
        @UploadedFile() image,
    ) {
        if (!req?.user?.id) {
            throw new HttpException(
                'Ошибка авторизации. Попробуйте войти в аккаунт.',
                HttpStatus.UNAUTHORIZED
            )
        }

        const user = await this.userService.updateUser(req.user, image, dto)

        return this.authService.generateToken(user)
    }
}
