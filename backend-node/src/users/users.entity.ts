import {
    Column,
    DeleteDateColumn,
    Entity, JoinColumn, ManyToOne, OneToMany
} from "typeorm"
import { ApiProperty } from "@nestjs/swagger"
import { Exclude } from "class-transformer"
import { BaseEntity } from "../core/BaseEntity"
import { Feedback } from "../feedback/feedback.entity"
import { Image } from "../image/image.entity"
import { RefreshToken } from "src/auth/refresh.entity"

@Entity('users')
export class User extends BaseEntity{
    @ApiProperty({example: 'Иванов Иван', description: 'Пустым быть не может'})
    @Column('varchar', {nullable: false})
    public name: string

    @ApiProperty({example: 'ivanov@ya.ru'})
    @Column('varchar', {nullable: true})
    public email?: string

    @Column({type: 'varchar', length: 12, name: 'phone_number', nullable: true})
    public phoneNumber?: string

    @Column({type: 'json', default: ['ROLE_USER']})
    public roles: string[]

    @ApiProperty({example: 'm.xcfkcvmz4dffkjjhjsdaeqewSdzx'})
    @Exclude()
    @Column('varchar', {nullable: false})
    public password: string

    @ApiProperty({type: () => Feedback})
    @OneToMany(() => Feedback, (feedback: Feedback) => feedback.user)
    public feedbacks: Feedback[]

    @Column({type: 'varchar', length: 1000, name: 'image_path', nullable: true})
    public imagePath?: string

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @DeleteDateColumn()
    public deleted: Date

    @OneToMany(() => RefreshToken, (refreshToken: RefreshToken) => refreshToken.user)
    public refreshTokens: RefreshToken[]
}
