import { Module } from '@nestjs/common'
import { OrderController } from './order.controller'
import { OrderService } from './order.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "../product/product.entity"
import { User } from "../users/users.entity"
import { Order } from "./order.entity"

@Module({
    controllers: [OrderController],
    providers: [OrderService],
    imports: [TypeOrmModule.forFeature([Product, User, Order])],
    exports: [TypeOrmModule]
})
export class OrderModule {}
