import { BaseEntity } from "../core/BaseEntity"
import { Column, Entity, JoinTable, ManyToMany } from "typeorm"
import { ApiProperty, ApiTags } from "@nestjs/swagger"
import { Image } from "../image/image.entity"
import { Product } from "../product/product.entity"

// TODO: связка с юзерами и адресами
@ApiTags('Заказы')
@Entity('orders')
export class Order extends BaseEntity {
    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @Column('timestamp', {name: 'delivery_date', nullable: true})
    public deliveryDate?: Date

    @ApiProperty({example: 'Описание к заказу'})
    @Column('text', {nullable: true})
    public description?: string

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @Column('timestamp', {name: 'planned_date', nullable: true})
    public plannedDate?: Date

    @ApiProperty({example: 'В процессе'})
    @Column('varchar', {default: 'В процессе'})
    public status: 'В процессе' | 'Готово' | 'Отменен'

    @ApiProperty({example: 14142.33})
    @Column('float', {nullable: false})
    public price: number

    @ManyToMany(() => Product, {onDelete: 'SET NULL'})
    @JoinTable({name: 'orders_products'})
    public products: Product[]
}
