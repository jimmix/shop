import { Module } from '@nestjs/common'
import { AppService } from './app.service'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UsersModule } from './users/users.module'
import { AuthModule } from './auth/auth.module'
import { ProductModule } from './product/product.module'
import { ImageModule } from './image/image.module'
import { PropertyModule } from './property/property.module'
import { CategoryModule } from './category/category.module'
import { OrderModule } from './order/order.module'
import { FeedbackModule } from './feedback/feedback.module'
import { AddressModule } from './address/address.module'
import { FilesModule } from './core/files/files.module'
import { ServeStaticModule } from "@nestjs/serve-static"
import * as path from 'path'
import {CommandModule} from "nestjs-command";
import {TestCommand} from "./cli/commands/test.command";
import {CategoryService} from "./category/category.service";
import {ImageService} from "./image/image.service";
import {PropertyService} from "./property/property.service";
import {ProductService} from "./product/product.service";

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get<string>('DB_HOST'),
                port: configService.get<number>('DB_POST'),
                username: configService.get<string>('DB_USERNAME'),
                password: configService.get<string>('DB_PASSWORD'),
                database: configService.get<string>('DB_NAME'),
                synchronize: true,
                autoLoadEntities: true,
                entities: [__dirname + '/**/*.entity{.js, .ts}'],
                migrationsTableName: 'migrations',
                migrationsRun: false,
                migrationsTransactionMode: "each",
                migrations: [__dirname + '../**/migrations/*.{.js, .ts}']
            }),
            inject: [ConfigService]
        }),
        UsersModule,
        AuthModule,
        ProductModule,
        ImageModule,
        PropertyModule,
        CategoryModule,
        OrderModule,
        FeedbackModule,
        AddressModule,
        FilesModule,
        ServeStaticModule.forRoot({
            rootPath: path.resolve(__dirname, '..', '..','static')
        }),
        CommandModule,
    ],
    controllers: [],
    providers: [AppService, TestCommand, CategoryService, ImageService, PropertyService, ProductService]
})

export class AppModule {}
