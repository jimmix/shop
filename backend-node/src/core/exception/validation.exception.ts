import { HttpException, HttpStatus } from "@nestjs/common"

export class ValidationException extends HttpException
{
    public constructor(response) {
        const template = {
            status: 400,
            message: 'Ошибка валидации параметров',
            error: response
        }
        super(template, HttpStatus.BAD_REQUEST)
    }
}
