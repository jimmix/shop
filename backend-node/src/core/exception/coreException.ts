import { HttpException, HttpStatus } from "@nestjs/common"

export class CoreException extends HttpException {
    public constructor(message = 'Непредвиденная ошибка', status = 400, error = []) {
        super({status, message, error}, HttpStatus.BAD_REQUEST)
    }
}
