import { HttpException, HttpStatus, Injectable } from "@nestjs/common"
import * as fs from 'fs'
import * as path from 'path'
import * as uuid from 'uuid'

@Injectable()
export class FilesService {
    public async createFile(file): Promise<string> {
        try {
            const originalName: string = file.originalname
            const arrayName = originalName.split('.')
            const ext = arrayName[arrayName.length -1]
            const fileName = uuid.v4() + `.${ext}`
            const filePath = path.resolve(__dirname, '..', '..', '..', 'static')
            if (!fs.existsSync(filePath)) {
                fs.mkdirSync(filePath, {recursive: true})
            }
            fs.writeFileSync(path.join(filePath, fileName), file.buffer)

            return fileName
        } catch (e) {
            throw new HttpException(
                'Непредвиденная ошибка при записи файла',
                HttpStatus.INTERNAL_SERVER_ERROR
            )
        }
    }
}
