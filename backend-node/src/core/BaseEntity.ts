import { ApiProperty } from "@nestjs/swagger"
import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm"

export abstract class BaseEntity {
    @ApiProperty({example: 'fffdd269-336e-466a-9b22-edfdb85b50fc'})
    @PrimaryGeneratedColumn('uuid')
    public id: string
    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @CreateDateColumn({name: 'created_at'})
    public createdAt: Date

    @ApiProperty({example: '2023-06-11T13:35:39.000Z'})
    @UpdateDateColumn({name: 'updated_at'})
    public updatedAt: Date
}
