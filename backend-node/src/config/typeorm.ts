import * as dotenv from 'dotenv'
import * as dotenvExpand from 'dotenv-expand'
import { DataSource } from 'typeorm'
import * as process from "process"

dotenvExpand.expand(dotenv.config({path: '.env.local'}))

const port: number = Number(process.env.DB_POST) || 5432

export default new DataSource({
    type: 'postgres',
    host: process.env.DB_HOST,
    port: port,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: true,
    entities: [__dirname + '/**/*.entity{.js, .ts}'],
    migrationsTableName: 'migrations',
    migrationsRun: false,
    migrationsTransactionMode: "each",
    migrations: ['dist/migrations/*.js']

})
