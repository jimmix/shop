import { ApiProperty } from "@nestjs/swagger"
import { IsEmail, IsOptional, IsString, Length } from "class-validator"

export class CreateCategoryDto {
    @ApiProperty({example: 'Молочные продукты'})
    @IsString({message: 'Должен быть строкой'})
    @Length(2, 50, {message: 'Название категори от 2 до 50 символов'})
    public name: string

    @IsString({message: 'Должен быть строкой'})
    @IsOptional()
    @ApiProperty({example: 'fffdd269-336e-466a-9b22-edfdb85b50fc', required: false, nullable: true})
    public parentId?: string

    @ApiProperty({example: 'Маленькое Изображение'})
    public littleImage?: any

    @ApiProperty({example: 'Большое Изображение на главной'})
    public bigImage?: any
}
