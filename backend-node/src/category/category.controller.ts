import { Body, Controller, Get, Post, UploadedFiles, UseInterceptors, UsePipes } from "@nestjs/common"
import { CreateCategoryDto } from "./dto/CreateCategoryDto"
import { CategoryService } from "./category.service"
import { FileFieldsInterceptor } from "@nestjs/platform-express"
import { ApiResponse, ApiTags } from "@nestjs/swagger"
import { Category } from "./category.entity"
import { ValidationPipe } from "../core/pipes/validation.pipe"

@ApiTags('Категории товаров')
@Controller('api/v1/category')
export class CategoryController {
    public constructor(
        private readonly categoryService: CategoryService
    ) {
    }

    @Post()
    @UseInterceptors(FileFieldsInterceptor([
        {
            name: 'littleImage',
            maxCount: 1
        },
        {
            name: 'bigImage',
            maxCount: 1
        }
    ]))
    @ApiResponse({status: 201, type: Category})
    @UsePipes(ValidationPipe)
    public createCategory(
        @UploadedFiles() files,
        @Body() dto: CreateCategoryDto,
    ): Promise<Category> {
        dto.littleImage = files?.littleImage?.length ? files.littleImage[0] : null
        dto.bigImage = files?.bigImage?.length ? files.bigImage[0] : null
        return this.categoryService.createCategory(dto)
    }

    @Get()
    @ApiResponse({status: 200, type: [Category]})
    public findAll(): Promise<Category[]>
    {
        return this.categoryService.findAll()
    } 
}
