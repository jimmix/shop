import { Column, Entity, OneToMany, TreeChildren, TreeParent } from "typeorm"
import { BaseEntity } from "../core/BaseEntity"
import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger"
// import { Product } from "../product/product.entity"
import { Image } from "../image/image.entity"
import { Product } from "src/product/product.entity"

@Entity('categories')
export class Category extends BaseEntity {
    @ApiProperty({example: 'Фрукты', description: 'Название категории товара'})
    @Column('varchar')
    public name: string
 
    @TreeChildren()
    public children: Category[]

    @TreeParent()
    public parent?: Category

    @OneToMany(() => Product, (product) => product.category)
    public products: Product[]

    @Column('varchar', {nullable: true})
    public littleImage: string

    @Column('varchar', {nullable: true})
    public bigImage: string
}
