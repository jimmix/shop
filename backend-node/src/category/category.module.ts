import { Module } from '@nestjs/common'
import { CategoryController } from './category.controller'
import { CategoryService } from './category.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "../product/product.entity"
import { Category } from "./category.entity"
import { FilesService } from "../core/files/files.service"

@Module({
    controllers: [CategoryController],
    providers: [CategoryService, FilesService],
    imports: [
        TypeOrmModule.forFeature([Product, Category])
    ],
    exports: [TypeOrmModule]
})
export class CategoryModule {}
