import { Injectable } from '@nestjs/common'
import { InjectRepository } from "@nestjs/typeorm"
import { Repository } from "typeorm"
import { Category } from "./category.entity"
import { CreateCategoryDto } from "./dto/CreateCategoryDto"
import { FilesService } from "../core/files/files.service"

@Injectable()
export class CategoryService {
    public constructor(
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>,
        private readonly fileService: FilesService
    ) {
    }

    public findAll(): Promise<Category[]>
    {
        return this.categoryRepository.createQueryBuilder('c')
            .leftJoinAndSelect('c.children', 'children')
            .where('c.parentId is null')
            .getMany()
    }

    public async createCategory(dto: CreateCategoryDto): Promise<Category>
    {
        const littleImage = dto.littleImage
            ? await this.fileService.createFile(dto.littleImage)
            : null
        const bigImage = dto.bigImage
            ? await this.fileService.createFile(dto.bigImage)
            : null
        const parentCategory = dto.parentId
            ? await this.categoryRepository.findOneBy({id: dto.parentId})
            : undefined
        const category = this.categoryRepository.create(
            {...dto, bigImage, littleImage}
        )
        if (parentCategory) {
            category.parent = parentCategory
        }

        await this.categoryRepository.save(category)
        return category
    }

    public getByName(name: string) {
        return this.categoryRepository.findOneBy({name: name})
    }
}
