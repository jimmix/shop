import { Column, Entity } from "typeorm"
import { BaseEntity } from "../core/BaseEntity"
import { ApiProperty } from "@nestjs/swagger"

@Entity('addresses')
export class Address extends BaseEntity {
    @ApiProperty({example: 'Новосибирск'})
    @Column('varchar', {nullable: false})
    public city: string

    @ApiProperty({example: '2', description: 'Этаж'})
    @Column('varchar', {nullable: true})
    public flat?: string

    @ApiProperty({example: '14a', description: 'номер дома'})
    @Column('varchar', {nullable: true})
    public house?: string

    @ApiProperty({example: 'Ленина'})
    @Column('varchar', {nullable: false, length: 1000})
    public street: string

    @ApiProperty({example: 'Въезд со стороны главной улицы'})
    @Column('varchar', {length: 1000, nullable: true})
    public description?: string
}
