import { Module } from '@nestjs/common'
import { AddressController } from './address.controller'
import { AddressService } from './address.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { User } from "../users/users.entity"
import { Order } from "../order/order.entity"
import { Address } from "./address.entity"

@Module({
    controllers: [AddressController],
    providers: [AddressService],
    imports: [TypeOrmModule.forFeature([Order, User, Address])],
    exports: [TypeOrmModule]
})
export class AddressModule {}
