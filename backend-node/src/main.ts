import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger"
import { FastifyAdapter, NestFastifyApplication } from "@nestjs/platform-fastify"
import * as cookieParser from 'cookie-parser'
import { ValidationPipe } from "@nestjs/common"

async function bootstrap() {
    // const app: NestFastifyApplication = await NestFactory.create<NestFastifyApplication>(
    //     AppModule,
    //     new FastifyAdapter()
    // )
    const app = await NestFactory.create(AppModule)
    app.enableCors()
    app.use(cookieParser())
    app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }))

    const config: Omit<OpenAPIObject, "paths"> = new DocumentBuilder()
        .setTitle('Интернет магазин')
        .setDescription('Bff для интернет магазина')
        .setVersion('1.0.0')
        .addTag('fer-mir')
        .build()
    const document: OpenAPIObject = SwaggerModule.createDocument(app, config)
    SwaggerModule.setup('/api/doc', app, document)
    await app.listen(9090)
}
bootstrap()
