import {Injectable} from "@nestjs/common";
import {Command} from "nestjs-command";
import * as fs from 'fs'
import * as path from "path";
import {CreateCategoryDto} from "../../category/dto/CreateCategoryDto";
import {CategoryService} from "../../category/category.service";
import axios from 'axios'
import {CreateProductDto, ImageItem} from "../../product/dto/CreateProductDto";
import {ImageService} from "../../image/image.service";
import {PropertyService} from "../../property/property.service";
import {enumType, Property} from "../../property/property.entity";
import {Image} from "../../image/image.entity";
import {ProductService} from "../../product/product.service";

@Injectable()
export class TestCommand {
    public constructor(
        private readonly categoryService: CategoryService,
        private readonly imageService: ImageService,
        private readonly propertyService: PropertyService,
        private readonly productService: ProductService
    ) {
    }

    @Command({
        command: 'testik',
        describe: 'for test'
    })
    public async execute() {
        const data = fs.readFileSync('./assort.csv', 'utf8');
        const rows = data.trim().split('\n');
        const headers = rows.shift().split('^');

        const result = rows.reduce((acc,row, idx) => {
            if (idx === 0) {
                return []
            }
            const values = row.split('^');
            acc.push(headers.reduce((obj, header, index) => {
                obj[index] = values[index]
                return obj
            }, []))

            return acc
        }, []);

        await this.createUniqueCategories(result)
        await this.createProducts(result)
    }

    private async loadImageAndCreateCategory(bigImage: string, littleImage: string, isEnd, dto: CreateCategoryDto, categories: string[]) {
        const imageUrl = isEnd ? littleImage : bigImage
        const response = await axios({method: 'get', url: imageUrl, responseType: "arraybuffer"})

        const urlParsed = new URL(imageUrl);
        const filename = path.basename(urlParsed.pathname);
        const image = {originalname: filename, buffer: response.data}

        if (!isEnd) {
            dto.bigImage = image
            await this.loadImageAndCreateCategory(bigImage, littleImage, true, dto, categories)
        } else {
            dto.littleImage = image
            await this.createParentCategory(dto, categories)
        }
    }

    private async getImageByImageUrl(imageUrl): Promise<Image>
    {
        const response = await axios({method: 'get', url: imageUrl, responseType: "arraybuffer"})
        const urlParsed = new URL(imageUrl)
        const filename = path.basename(urlParsed.pathname)

        return this.imageService.create({originalname: filename, buffer: response.data})
    }

    private async createUniqueCategories(items: any[]) {
        for (const item of items) {
            const categories = item[0].split('/')
            const dto = new CreateCategoryDto()
            dto.parentId = null
            dto.name = categories[0] as string

            try {
                await this.loadImageAndCreateCategory(item[69] as string, item[70] as string, false, dto, categories)
            } catch (e) {
                console.log(e)
            }
        }
    }

    private async createParentCategory(dto: CreateCategoryDto, categories: string[]) {
        let category = await this.categoryService.getByName(dto.name)
        if (!category?.id) {
            category = await this.categoryService.createCategory(dto)
        }

        if (categories.length === 2) {
            const childCategory = await this.categoryService.getByName(categories[1])
            if (childCategory?.id) {
                return;
            }

            const childCategoryDto = new CreateCategoryDto()

            childCategoryDto.name = categories[1]
            childCategoryDto.parentId = category.id
            childCategoryDto.littleImage = undefined
            childCategoryDto.bigImage = undefined

            await this.createChildCategory(childCategoryDto)
        }
    }

    private async createChildCategory(dto: CreateCategoryDto) {
        await this.categoryService.createCategory(dto)
    }

    private async createProducts(items: any[]) {
        for (const item of items) {
            const dto = new CreateProductDto()
            dto.skladId = item[1]
            const categories = item[0].split('/')
            dto.category = await this.categoryService.getByName(categories[categories.length -1])
            dto.description = item[24]
            dto.basePrice = this.getNumber(item[8])
            item[10] = this.getNumber(item[10])
            dto.discount = item[10] == 0
                ? null
                : ((Number(item[8]) - Number(10)) / Number(item[8])) * 100
            dto.quantity = 100
            dto.weight = item[36]
            dto.typeWeight = item[7]
            dto.createdAt = new Date()
            dto.updatedAt = new Date()
            dto.image = item[68] !== '' ? await this.getImageByImageUrl(item[68]) : null
            dto.availableDays = this.getAvailableDaysByItem(item)
            dto.provider = item[32] != '' ? item[32] : null
            dto.structure = item[52]
            dto.protein = item[53] != '' ? this.getNumber(item[53]) : null
            dto.fats = item[54] != '' ? this.getNumber(item[54]) : null
            dto.carbs = item[55] != '' ? this.getNumber(item[55]) : null
            dto.properties = await this.createPropertiesByItem(item)
            dto.name = item[4]
            console.log(dto)
            await this.productService.create(dto)
        }
    }
    private getNumber(value: string)
    {
        value = value.replace(',', '.')
        const newValue: any = Number(value)

        return !isNaN(newValue) ? newValue : null
    }

    private getAvailableDaysByItem(item: any): number {
        /**
         * 60 - пн
         * 61 - вт
         * 62 - ср
         * 63 - чт
         * 64 - пт
         * 65 - сб
         * 66 - вс
         */
        const days = [item[60], item[61], item[62], item[63], item[64], item[65], item[66]]
        return days.reduce((acc, day, idx) => {
            acc += day === 'да' ? 2**idx : 0
            return acc
        }, 0)
    }

    private async createPropertiesByItem(item: any) {
        const country = item[29]
        const properties: Property[] = []
        properties.push(await this.getNewProperty('Страна', 'string', country))

        const isWeightTov = item[37] === 'да'
        properties.push(await this.getNewProperty('Весовой товар', 'boolean', isWeightTov))

        const volume = this.getNumber(item[40])
        properties.push(await this.getNewProperty('Объем', 'number', volume))

        const isBottling = item[41] === 'да'
        properties.push(await this.getNewProperty('Разливной товар', 'boolean', isBottling))

        const expirationDay = item['56']
        properties.push(await this.getNewProperty('Срок годности', "string", expirationDay))

        const storageCondition = item[57]
        properties.push(await this.getNewProperty('Условия хранения', "string", storageCondition))

        const energy = item[58]
        properties.push(await this.getNewProperty('Энергетическая ценность', "string", energy))

        const packages = item[59]
        properties.push(await this.getNewProperty('Упаковка', "string", packages))

        const weighing = item[60]
        properties.push(await this.getNewProperty('Весовой', "string", weighing))

        return properties
    }

    private async getNewProperty(name: string, type: enumType, value: any): Promise<Property>
    {
        const property = new Property()
        property.name = name
        property.typeValue = type

        if (type === 'boolean') {
            property.boolValue = value
        }

        if (type === 'string') {
            property.stringValue = value
        }

        if (type === 'time') {
            property.timeValue = new Date(value)
        }

        if (type === 'number') {
            property.numberValue = value
        }

        return this.propertyService.save(property)
    }
}
