import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Category} from "../category/category.entity";
import {Repository} from "typeorm";
import {FilesService} from "../core/files/files.service";
import {Image} from "./image.entity";
import {CreateImageDto} from "./dto/CreateImageDto";

@Injectable()
export class ImageService {
    public constructor(
        @InjectRepository(Image)
        private readonly imageRepository: Repository<Image>,
        private readonly fileService: FilesService
    ) {
    }

    public async create(dto: CreateImageDto)
    {
        const imagePath = await this.fileService.createFile(dto)
        const image = this.imageRepository.create({path: imagePath})

        return await this.imageRepository.save<Image>(image)
    }
}
