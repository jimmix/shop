import { Module } from '@nestjs/common'
import { ImageController } from './image.controller'
import { ImageService } from './image.service'
import { TypeOrmModule } from "@nestjs/typeorm"
import { Product } from "../product/product.entity"
import { Image } from "./image.entity"
import { Feedback } from "../feedback/feedback.entity"
import {FilesService} from "../core/files/files.service";

@Module({
    controllers: [ImageController],
    providers: [ImageService, FilesService],
    imports: [TypeOrmModule.forFeature([Product, Image, Feedback])],
    exports: [TypeOrmModule]
})
export class ImageModule {}
