import { Column, DeleteDateColumn, Entity, ManyToMany, OneToMany } from "typeorm"
import { ApiProperty } from "@nestjs/swagger"
import { BaseEntity } from "../core/BaseEntity"
import { Product } from "../product/product.entity"
import { Feedback } from "../feedback/feedback.entity"

@Entity('images')
export class Image  extends BaseEntity {
    @ApiProperty({example: '/images/image.jpeg'})
    @Column('varchar', {nullable: false, length: 1500})
    public path: string

    public getPath(): string 
    { 
        return 'konch'
    }

    @DeleteDateColumn()
    public deleted: Date

    @OneToMany(() => Product, (product: Product) => product.image)
    public products: Product[]

    @ManyToMany(() => Feedback, feedback => feedback.images)
    public feedbacks: Feedback[]
}
