/** @type {import('next').NextConfig} */
console.log(process.env.NEXT_PUBLIC_PORT)
console.log(process.env.NEXT_PUBLIC_HOSTNAME)
const nextConfig = {
	reactStrictMode: true,
	poweredByHeader: false,
	env: {
		APP_URL: process.env.NEXT_PUBLIC_DOMAIN,
		BACKEND_STORAGE_PATH: process.env.NEXT_PUBLIC_BACK_FILE_STORAGE
	},
	publicRuntimeConfig: {
		APP_URL: process.env.NEXT_PUBLIC_DOMAIN,
		BACKEND_STORAGE_PATH: process.env.NEXT_PUBLIC_BACK_FILE_STORAGE
	},
	serverRuntimeConfig: {
		APP_URL: process.env.NEXT_PUBLIC_DOMAIN,
		BACKEND_STORAGE_PATH: process.env.NEXT_PUBLIC_BACK_FILE_STORAGE
	},
	images: {
		remotePatterns: [
			{
				protocol: process.env.NEXT_PUBLIC_PROTOCOL,
				hostname: process.env.NEXT_PUBLIC_HOSTNAME,
				port: process.env.NEXT_PUBLIC_PORT,
				pathname: '**'
			}
		]
	}
}

module.exports = nextConfig
