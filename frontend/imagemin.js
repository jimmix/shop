import imagemin from 'imagemin';
import imageminJpegtran from 'imagemin-jpegtran';
import imageminPngquant from 'imagemin-pngquant';
import imageminWebp from "imagemin-webp";

const files = await imagemin(['./public/images/**.{png} --plugin.pngquant.quality={0.3,0.4} > '], {
    destination: 'build/images',
    plugins: [
        imageminJpegtran(),
        imageminPngquant({
            quality: [0.6, 0.8]
        }),
        imageminWebp({quality: 50})
    ]
});
