import { DetailedHTMLProps, InputHTMLAttributes, MutableRefObject } from 'react'

export interface InputProps
	extends DetailedHTMLProps<
		InputHTMLAttributes<HTMLInputElement>,
		HTMLInputElement
	> {
	innerRef?: MutableRefObject<HTMLInputElement>
}
