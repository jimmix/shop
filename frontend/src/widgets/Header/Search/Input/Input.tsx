import cl from 'classnames'

import { InputProps } from '@/widgets/Header/Search/Input/Input.props'

import styles from '../Search.module.scss'

export const Input = ({
	className,
	innerRef,
	...props
}: InputProps): JSX.Element => {
	return (
		<input
			type='text'
			{...props}
			className={cl(styles.input, className)}
			placeholder={'Куринное филе'}
			ref={innerRef}
		/>
	)
}
