import cl from 'classnames'
import Image from 'next/image'
import { MutableRefObject, createRef, useState } from 'react'

import { Input } from '@/widgets/Header/Search/Input/Input'

import styles from './Search.module.scss'

export const Search = (): JSX.Element => {
	const [isActive, setIsActive] = useState<boolean>(false)

	const inputRef = createRef() as MutableRefObject<HTMLInputElement>
	const onClickHandler = () => {
		if (window.innerWidth > 598) {
			return
		}
		if (!isActive) {
			inputRef.current.focus()
		} else if (isActive && inputRef.current.value) {
			return
		}

		setIsActive(!isActive)
	}
	return (
		<div
			className={cl(styles['search-wrapper'], {
				[styles.active]: isActive
			})}
			onClick={onClickHandler}
		>
			<Input onClick={e => e.stopPropagation()} innerRef={inputRef} />
			<Image
				className={styles.glass}
				alt={'glass'}
				src={'/images/header-glass.png'}
				width={30}
				height={26}
				priority
			/>
		</div>
	)
}
