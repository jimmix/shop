import { SVGProps } from 'react'

export interface HeartProps extends SVGProps<SVGSVGElement> {}
