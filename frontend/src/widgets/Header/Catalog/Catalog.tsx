// @ts-ignore
import cl from 'classnames'
import Image from 'next/image'
import Link from 'next/link'

// @ts-ignore
import { CatalogProps } from '@/widgets/Header/Catalog/Catalog'

// @ts-ignore
import { Button } from '@/shared/ui/Button/Button'

// @ts-ignore
import styles from './Catalog.module.scss'

export const Catalog = ({ ...props }: CatalogProps): JSX.Element => {
	return (
		<Link href={'/catalog'}>
			<Button appearance='red' className={cl(styles.catalog)} {...props}>
				<Image
					src='/images/header-catalog.png'
					alt='location'
					width={20}
					height={20}
					priority
				/>
				<p>Каталог</p>
			</Button>
		</Link>
	)
}
