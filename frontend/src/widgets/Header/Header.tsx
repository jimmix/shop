'use client'

import cl from 'classnames'

import { HeaderProps } from '@/widgets/Header/Header.props'
import { HeaderBottom } from '@/widgets/Header/HeaderBottom/HeaderBottom'
import { HeaderTop } from '@/widgets/Header/HeaderTop/HeaderTop'
import { HeaderTopMobile } from '@/widgets/Header/HeaderTop/HeaderTopMobile/HeaderTopMobile'

import styles from './Header.module.scss'

export const Header = ({
	withoutTable = false,
	withoutMobile = false,
	...props
}: HeaderProps): JSX.Element => {
	return (
		<header className={cl(styles.header)} {...props}>
			<div className={styles.headerDesktop}>
				<HeaderTop />
				<HeaderBottom />
			</div>
			<HeaderTopMobile className={styles.headerTablet} />
		</header>
	)
}
