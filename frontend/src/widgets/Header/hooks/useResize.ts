import { Dispatch, SetStateAction, useEffect } from 'react'

import { DeviceType } from '@/shared/lib/types/DeviceType'

export function useResize(
	deviceType: DeviceType,
	setDeviceType: Dispatch<SetStateAction<DeviceType>>
) {
	useEffect(() => {
		const listener = (): void => {
			const width = window.innerWidth
			switch (true) {
				case width > 997 && deviceType !== 'Desktop':
					setDeviceType('Desktop')
					break
				case width <= 997 && width > 598 && deviceType !== 'Table':
					setDeviceType('Table')
					break
				case width <= 598 && deviceType !== 'Mobile':
					setDeviceType('Mobile')
					break
			}
		}

		window.addEventListener('resize', listener)

		return () => {
			window.removeEventListener('resize', listener)
		}
	})
}
