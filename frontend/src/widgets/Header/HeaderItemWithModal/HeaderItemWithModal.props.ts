import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface HeaderItemWithModalProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}
