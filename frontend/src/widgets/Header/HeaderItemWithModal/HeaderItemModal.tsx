import cl from 'classnames'
import Link from 'next/link'
import { MutableRefObject, useRef } from 'react'

import { HeaderItemModalProps } from '@/widgets/Header/HeaderItemWithModal/HeaderItemModal.props'

import { useOnClickOutside } from '@/shared/lib/hooks/useOnClickOutside'

import styles from './HeaderItemModal.module.scss'

export const HeaderItemModal = ({
	isModal,
	setIsModal,
	parentRef,
	...props
}: HeaderItemModalProps): JSX.Element => {
	const ref = useRef<HTMLDivElement>() as MutableRefObject<HTMLDivElement>
	useOnClickOutside(ref, parentRef, () => setIsModal(false))
	return (
		<div
			ref={ref}
			className={cl(styles.modal, {
				[styles.active]: isModal
			})}
			{...props}
		>
			<div className={styles.list}>
				<Link
					href={'/about_us#how-we-work'}
					scroll={false}
					onClick={() => setIsModal(false)}
				>
					Как мы работаем
				</Link>
				<Link
					href={'/about_us#feedback'}
					scroll={false}
					onClick={() => setIsModal(false)}
				>
					Отзывы
				</Link>
				<Link
					href={'/about_us#team'}
					scroll={false}
					onClick={() => setIsModal(false)}
				>
					Команда
				</Link>
			</div>
		</div>
	)
}
