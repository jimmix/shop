import cl from 'classnames'
import { MutableRefObject, useRef, useState } from 'react'

import { HeaderItemModal } from '@/widgets/Header/HeaderItemWithModal/HeaderItemModal'
import { HeaderItemWithModalProps } from '@/widgets/Header/HeaderItemWithModal/HeaderItemWithModal.props'

import stylesParent from '../HeaderList/HeaderList.module.scss'

export const HeaderItemWithModal = ({
	...props
}: HeaderItemWithModalProps): JSX.Element => {
	const [isModal, setIsModal] = useState(false)
	const ref = useRef<HTMLDivElement>() as MutableRefObject<HTMLDivElement>
	const setVisibleModal = () => {
		setIsModal(!isModal)
	}

	return (
		<div className={stylesParent.relative} {...props} ref={ref}>
			<p onClick={() => setVisibleModal()} className={cl(stylesParent.item)}>
				О нас
			</p>
			<HeaderItemModal
				isModal={isModal}
				setIsModal={setIsModal}
				parentRef={ref}
			/>
		</div>
	)
}
