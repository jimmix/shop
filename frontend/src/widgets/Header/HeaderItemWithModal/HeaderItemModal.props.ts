import { DetailedHTMLProps, HTMLAttributes, RefObject } from 'react'

export interface HeaderItemModalProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	isModal: boolean
	setIsModal(flag: boolean): void
	parentRef?: RefObject<HTMLDivElement> | undefined | null
}
