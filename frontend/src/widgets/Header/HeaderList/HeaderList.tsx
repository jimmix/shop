import Link from 'next/link'

import { HeaderItemWithModal } from '@/widgets/Header/HeaderItemWithModal/HeaderItemWithModal'

import styles from './HeaderList.module.scss'

export const HeaderList = (): JSX.Element => {
	return (
		<div className={styles.list}>
			<Link className={styles.item} href={'/receipt'}>
				Рецепты
			</Link>
			<Link className={styles.item} href={'/#questions'} scroll={false}>
				Вопросы и ответы
			</Link>
			<HeaderItemWithModal />
			<Link className={styles.item} href={'/contact'}>
				Контакты
			</Link>
		</div>
	)
}
