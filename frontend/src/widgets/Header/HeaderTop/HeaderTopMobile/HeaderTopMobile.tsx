import cl from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { Search } from '@/widgets/Header/Search/Search'

import { Button } from '@/shared/ui/Button/Button'

import styles from '../../Header.module.scss'

export const HeaderTopMobile = ({
	className,
	...props
}: DetailedHTMLProps<
	HTMLAttributes<HTMLDivElement>,
	HTMLDivElement
>): JSX.Element => {
	return (
		<div className={cl(className, styles['header-top'])} {...props}>
			<Link href={'/'} className={styles.logoHref}>
				<Image
					className={styles.logo}
					src='/images/logo-mobile.svg'
					alt='logo'
					width={120}
					height={20}
					priority
				/>
			</Link>
			<Search />
			<Button className={cl(styles.btn, styles.heart)} appearance={'ghost'}>
				<Image
					src={'/images/header-heart.png'}
					width={22}
					height={22}
					priority
					alt={'Избранные'}
				/>
			</Button>
		</div>
	)
}
