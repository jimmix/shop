import cl from 'classnames'
import Image from 'next/image'
import Link from 'next/link'

import { HeaderList } from '@/widgets/Header/HeaderList/HeaderList'
import { HeaderTopProps } from '@/widgets/Header/HeaderTop/HeaderTop.props'

import { Button } from '@/shared/ui/Button/Button'

import styles from '../Header.module.scss'

export const HeaderTop = ({
	className,
	...props
}: HeaderTopProps): JSX.Element => {
	return (
		<div className={cl(className, styles['header-top'])} {...props}>
			<Link href={'/'}>
				<Image
					className={styles.logo}
					src='/images/logo.svg'
					alt='logo'
					width={180}
					height={32}
					priority
				/>
			</Link>
			<HeaderList />
			<Button appearance='ghost' className={cl(styles.city)}>
				<Image
					src='/images/header-location.png'
					alt='location'
					width={20}
					height={20}
					priority
				/>
				<p>Новосибирск</p>
			</Button>
		</div>
	)
}
