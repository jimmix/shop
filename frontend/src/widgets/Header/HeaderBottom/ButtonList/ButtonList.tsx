import Image from 'next/image'
import {useCallback, useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import styles from '@/widgets/Header/Header.module.scss'
import { Heart } from '@/widgets/Header/Icons/Heart'
import { NavbarModal } from '@/widgets/NavbarModal/NavbarModal'

import { LoginModal } from '@/features/AuthByEmail/ui/LoginModal/LoginModal'
import { cartModalActions } from '@/features/CartModal/model/slice/CartModalSlice'
import { CartModal } from '@/features/CartModal/ui/CartModal/CartModal'
import { RegisterModal } from '@/features/RegisterByEmail/ui/RegisterModal/RegisterModal'

import { getUserAuthData } from '@/entities/User/model/selectors/getUserAuthData/getUserAuthData'

import { Button } from '@/shared/ui/Button/Button'
import {cartActions} from "@/entities/Cart/slice/cartSlice";

export const ButtonList = (): JSX.Element => {
	const authUser = useSelector(getUserAuthData)
	const dispatch = useDispatch()
	const [isOpenLogin, setIsOpenLogin] = useState<boolean>(false)
	const onCloseLoginHandler = () => {
		setIsOpenLogin(false)
	}
	const onShowModalHandler = () => {
		if (authUser?.id) {
			setIsOpenLogin(false)
			setIsOpenNavbarModal(true)
		} else {
			setIsOpenNavbarModal(false)
			setIsOpenLogin(true)
		}
	}

	const [isOpenRegister, setIsOpenRegister] = useState<boolean>(false)
	const onCloseRegisterHandler = () => {
		setIsOpenRegister(false)
	}

	const [isOpenNavbarModal, setIsOpenNavbarModal] = useState<boolean>(false)
	const onCloseNavbarModalHandler = () => {
		setIsOpenNavbarModal(false)
	}

	const onSwapByLoginModal = () => {
		setIsOpenRegister(false)
		setIsOpenLogin(true)
	}

	const onSwapByRegisterModal = () => {
		setIsOpenLogin(false)
		setIsOpenRegister(true)
	}

	const onCloseCartModal = useCallback(() => {
		dispatch(cartModalActions.setIsOpen(false))
	}, [dispatch])
	const onOpenCartModal = useCallback(() => {
		dispatch(cartModalActions.setIsOpen(true))
	}, [dispatch])

	return (
		<div className={styles['btn-list']}>
			<Button appearance={'ghost'} className={styles.btn}>
				<Heart className={styles.heart} />
			</Button>
			<Button className={styles.btn} appearance={'red'} onClick={onOpenCartModal}>
				<Image
					src={'/images/header-basket.svg'}
					width={24}
					height={24}
					priority
					alt={'Корзина'}
				/>
			</Button>
			<Button
				className={styles.btn}
				appearance={'black'}
				onClick={onShowModalHandler}
			>
				<Image
					src={'/images/header-login.svg'}
					width={30}
					height={30}
					priority
					alt={'Вход в систему'}
				/>
			</Button>
			<>
				<LoginModal
					isOpen={isOpenLogin}
					onClose={onCloseLoginHandler}
					onSwapHandler={onSwapByRegisterModal}
				/>
				<RegisterModal
					isOpen={isOpenRegister}
					onClose={onCloseRegisterHandler}
					onSwapHandler={onSwapByLoginModal}
				/>
				<NavbarModal
					isOpen={isOpenNavbarModal}
					onClose={onCloseNavbarModalHandler}
				/>
				<CartModal onClose={onCloseCartModal} />
			</>
		</div>
	)
}
