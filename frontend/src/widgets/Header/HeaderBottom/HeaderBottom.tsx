import cl from 'classnames'

// @ts-ignore
import { Catalog } from '@/widgets/Header/Catalog/Catalog'
import { ButtonList } from '@/widgets/Header/HeaderBottom/ButtonList/ButtonList'
import { HeaderBottomProps } from '@/widgets/Header/HeaderBottom/HeaderBottom.props'
import { Search } from '@/widgets/Header/Search/Search'

import styles from '../Header.module.scss'

export const HeaderBottom = ({ ...props }: HeaderBottomProps): JSX.Element => {
	return (
		<div {...props} className={cl(styles['header-bottom'])}>
			<Catalog />
			<Search />
			<ButtonList />
		</div>
	)
}
