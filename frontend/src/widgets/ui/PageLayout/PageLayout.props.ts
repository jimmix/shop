import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface PageLayoutProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	title?: string
}
