import cl from 'classnames'
import { useEffect, useState } from 'react'

import { RatingProps } from '@/widgets/Core/Product/Rating/Rating.props'
import { StarIcon } from '@/widgets/Header/Icons/StarIcon'

import styles from './Rating.module.scss'

export const Rating = ({
	isEditable = false,
	rating = 0,
	setRating,
	className,
	...props
}: RatingProps): JSX.Element => {
	const [ratingArray, setRatingArray] = useState<JSX.Element[]>(
		new Array(5).fill(<></>)
	)

	useEffect(() => {
		constructRating(rating)
	}, [rating])

	const constructRating = (currentRating: number) => {
		const updatedArray = ratingArray.map((r: JSX.Element, i: number) => {
			return (
				<StarIcon
					key={i}
					className={cl(styles.star, {
						[styles.filled]: i < currentRating,
						[styles.cur]: isEditable
					})}
				/>
			)
		})

		setRatingArray(updatedArray)
	}
	return (
		<div {...props}>
			{ratingArray.map((r, i) => (
				<span key={i}>{r}</span>
			))}
		</div>
	)
}
