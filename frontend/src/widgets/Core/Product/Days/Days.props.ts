import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface DaysProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	availableDays: number
}
