import cl from 'classnames'
import Image from 'next/image'

import { DaysProps } from '@/widgets/Core/Product/Days/Days.props'

import styles from './Days.module.scss'

export const Days = ({ availableDays, ...props }: DaysProps): JSX.Element => {
	const mapperDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']
	return (
		<div className={styles.container} {...props}>
			{mapperDays.map((day: string, idx: number) => (
				<div
					key={day}
					className={cl(styles.day, {
						[styles.available]:
							(availableDays & Math.pow(2, idx)) === Math.pow(2, idx)
					})}
				>
					{day}
				</div>
			))}
			<Image
				alt={'radius'}
				width={29}
				height={23}
				priority
				src={'/images/core/product/radius.svg'}
				className={styles.radiusLeft}
			/>
			<Image
				alt={'radius'}
				width={29}
				height={23}
				priority
				src={'/images/core/product/radius.svg'}
				className={styles.radiusRight}
			/>
		</div>
	)
}
