import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface Image {
	id: string
	path: string
}

export interface ProductProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	id: string
	name: string
	description?: string
	author?: string
	basePrice: number
	discount?: number
	discountDate?: string
	quantity: number
	countInCart?: number
	availableDays: number
	dateEnd?: string
	favorite?: boolean
	image?: Image
	rating?: number
	// typeWeight: 'кг' | 'гр'
	weight: number
}
