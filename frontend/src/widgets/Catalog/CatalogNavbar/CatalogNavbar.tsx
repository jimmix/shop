import { CatalogNavbarProps } from '@/widgets/Catalog/CatalogNavbar/CatalogNavbar.props'
import { CatalogNavbarItem } from '@/widgets/Catalog/CatalogNavbar/CatalogNavbarItem/CatalogNavbarItem'

import styles from './CatalogNavbar.module.scss'

export const CatalogNavbar = ({
	catalogList
}: CatalogNavbarProps): JSX.Element => {
	return (
		<div className={styles.container}>
			{catalogList.map(item => (
				<CatalogNavbarItem
					name={item.name}
					littleImage={item.littleImage}
					categoryChildren={item.children}
					key={item.id}
					id={item.id}
				/>
			))}
		</div>
	)
}
