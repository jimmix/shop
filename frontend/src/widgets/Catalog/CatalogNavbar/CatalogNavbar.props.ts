import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { Category } from '@/entities/Category/types/Category'

export interface CatalogNavbarProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	catalogList: Category[]
}
