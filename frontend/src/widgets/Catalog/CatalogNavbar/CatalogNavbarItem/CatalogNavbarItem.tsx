import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'

import { CatalogNavbarItemProps } from '@/widgets/Catalog/CatalogNavbar/CatalogNavbarItem/CatalogNavbarItem.props'

import { Category } from '@/entities/Category/types/Category'

import { config } from '@/shared/constaints/Config'

import styles from './CatalogNabarItem.module.scss'
import cl from 'classnames'

export const CatalogNavbarItem = ({
	name,
	categoryChildren,
	littleImage,
	id,
	...props
}: CatalogNavbarItemProps): JSX.Element => {
	const [childrens, setChildrens] = useState<Category[]>([])
	const toggleChidlrens = () => {
		if (!categoryChildren.length) {
			return
		}

		if (childrens.length) {
			setChildrens([])
			return
		}

		setChildrens(categoryChildren)
	}
	return (
		<div {...props}>
			<div className={styles.container}>
				<Link className={styles.containerCategory} href={`/catalog/${id}`}>
					{littleImage ? (
						<Image
							src={`${config.backendStoragePath}/${littleImage}`}
							alt={'Изображение категории'}
							width={40}
							height={40}
						/>
					) : (
						<div className={styles.img}></div>
					)}
					<div className={styles.title}>{name}</div>
				</Link>
				{categoryChildren?.length ? (
					<div className={styles.containerArrow} onClick={toggleChidlrens}>
						<div className={cl(styles.arrow, {
							[styles.active]: childrens.length > 0
						})}></div>
					</div>
				) : (
					<></>
				)}
			</div>
			{childrens.length ? (
				<div className={styles.childrenContainer}>
					{childrens.map(item => (
						<CatalogNavbarItem
							name={item.name}
							categoryChildren={item.children}
							littleImage={null}
							key={item.id}
							id={item.id}
						/>
					))}
				</div>
			) : (
				<></>
			)}
		</div>
	)
}
