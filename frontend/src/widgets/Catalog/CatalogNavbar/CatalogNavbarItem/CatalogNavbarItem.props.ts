import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { Category } from '@/entities/Category/types/Category'

export interface CatalogNavbarItemProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	name: string
	littleImage: string | null
	categoryChildren: Category[]
	id: string
}
