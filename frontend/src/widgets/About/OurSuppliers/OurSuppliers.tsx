import cl from 'classnames'
import Image from 'next/image'

import { AndMore } from '@/widgets/About/OurSuppliers/AndMore'

import styles from './OurSuppliers.module.scss'

export const OurSuppliers = (): JSX.Element => {
	return (
		<section>
			<div className={styles.container}>
				<Image
					alt={'Наши поставщики'}
					src={'/images/aboutUs/OurSuppliers/ourSupplier.webp'}
					width={520}
					height={454}
					className={styles.photo}
				/>
				<div className={styles.containerText}>
					<div className={styles.title}>Наши поставщики</div>
					<div className={styles.description}>
						Все они маленькие производители и хозяйства. Делают продукты малыми
						объемами, поэтому их не так просто найти, а в больших магазинах и
						вовсе не встретить.
					</div>
					<div className={cl(styles.description, styles.indent)}>
						Но это не мешает производителям делать продукты с душой, чтобы они
						получались очень вкусными и качественными. Мы проверили и готовы
						ручаться за результат.
					</div>
				</div>
			</div>
			<AndMore />
		</section>
	)
}
