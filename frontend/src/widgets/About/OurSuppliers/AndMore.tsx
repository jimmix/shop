import cl from 'classnames'
import Image from 'next/image'

import styles from './OurSuppliers.module.scss'

export const AndMore = (): JSX.Element => {
	return (
		<div className={styles.container}>
			<div className={styles.containerText}>
				<div className={styles.title}>И ещё...</div>
				<div className={styles.description}>
					Мы работаем с маленькими производствами, поэтому нужные продукты есть
					в наличии не так часто, как хотелось бы.
				</div>
				<div className={cl(styles.description, styles.indent)}>
					Часто речь идет о совсем малом количестве, но мы активно ищем новые
					производства и расширяем границы.
				</div>
				<div className={cl(styles.description, styles.indent)}>
					Доставка доступна не во все дни, потому что в основном, мы забираем
					продукты от производителя и сразу доставляем вам, ведь речь идет о
					свежести.
				</div>
			</div>
			<Image
				alt={'Наши поставщики'}
				src={'/images/aboutUs/OurSuppliers/andMore.png'}
				width={500}
				height={414}
				className={styles.photo}
			/>
		</div>
	)
}
