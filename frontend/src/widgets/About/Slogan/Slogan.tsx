import styles from './Slogan.module.scss'

export const Slogan = (): JSX.Element => {
	return (
		<>
			<section className={styles.container}>
				<div className={styles.containerLeft}>
					<p className={styles.sloganTitle}>
						Сервис по доставке настоящих продуктов
					</p>
					<p className={styles.sloganDescription}>
						Мы действительно заботимся о качестве продуктов, ведь они влияют на
						наш внешний вид, наше здоровье и иммунитет
					</p>
				</div>
				<div className={styles.photoContainer}>
					<img
						src={'/images/aboutUs/about_space.webp'}
						alt={'Фото для лозунга'}
						className={styles.photo}
					/>
				</div>
			</section>
		</>
	)
}

