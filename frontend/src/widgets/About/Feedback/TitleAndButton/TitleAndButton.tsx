import cl from 'classnames'
import { MutableRefObject, createRef, useState } from 'react'

import { FeedbackItem } from '@/widgets/About/Feedback/FeedbackItem/FeedbackItem'
import { feedbackList } from '@/widgets/About/Feedback/FeedbackList/feedbackList'

import { HTag } from '@/shared/ui/HTag/HTag'
import { ButtonItem } from '@/shared/ui/Sliders/ButtonItem/ButtonItem'
import { ButtonListContainer } from '@/shared/ui/Sliders/ButtonListContainer/ButtonListContainer'
import { SliderPerPageAuto } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto'

import styles from './TitleAndButton.module.scss'

export const TitleAndButton = (): JSX.Element => {
	const [isStart, setIsStart] = useState(true)
	const [isEnd, setIsEnd] = useState(true)
	const prevRef = createRef() as MutableRefObject<HTMLButtonElement>
	const nextRef = createRef() as MutableRefObject<HTMLButtonElement>
	return (
		<>
			<div className={styles.container}>
				<HTag tag={'h1'}>Что люди говорят о нас?</HTag>
				<ButtonListContainer>
					<ButtonItem arrow={'left'} innerRef={prevRef} isDisabled={isStart} />
					<ButtonItem arrow={'right'} innerRef={nextRef} isDisabled={isEnd} />
				</ButtonListContainer>
			</div>
			<div
				className={cl(styles.containerList)}
				style={{ maxWidth: '100%', minWidth: '100%' }}
			>
				<SliderPerPageAuto
					items={feedbackList.map((item, idx) => (
						<FeedbackItem
							key={idx}
							personName={item.personName}
							dateFeedback={item.dateFeedback}
							feedback={item.feedback}
						/>
					))}
					nextRef={nextRef}
					prevRef={prevRef}
					setIsEnd={setIsEnd}
					setIsStart={setIsStart}
				/>
			</div>
		</>
	)
}
