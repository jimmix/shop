import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface FeedbackItemProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	personName: string
	dateFeedback: string
	feedback: string
}
