import cl from 'classnames'
import Image from 'next/image'

import { FeedbackItemProps } from '@/widgets/About/Feedback/FeedbackItem/FeedbackItem.props'

import styles from './FeedbackItem.module.scss'
import {useEffect, useState} from "react";

export const FeedbackItem = ({
	personName,
	dateFeedback,
	feedback,
	className,
	...props
}: FeedbackItemProps): JSX.Element => {
	const [color, setColor] = useState<string>('')
	const colors: string[] = ['#98678c', '#e68484', '#84cbe6', '#93e684', '#e4c64c']

	useEffect(() => {
			setColor(colors[rand(colors.length - 1)])
	}, []);

	const persons = personName.split(' ')
	const firstWord = persons[0].split('')[0] // И
	const secondWord = persons[1].split('')[0]
	const rand = (max: number) => Math.floor(Math.random() * max)

	return (
		<div className={cl(styles.container, className)} {...props}>
			<div className={styles.personContainer}>
				<div
					className={styles.photo}
					style={{background: color}}
				><p className={styles.name}>{firstWord}{secondWord}</p></div>
				<div>
					<div className={styles.personName}>{personName}</div>
					<div className={styles.dateFeedback}>{dateFeedback}</div>
				</div>
			</div>
			<div className={styles.feedback}>{feedback}</div>
		</div>
	)
}
