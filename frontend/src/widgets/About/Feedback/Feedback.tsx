import { useState } from 'react'

import { feedbackList } from '@/widgets/About/Feedback/FeedbackList/feedbackList'
import { TitleAndButton } from '@/widgets/About/Feedback/TitleAndButton/TitleAndButton'

import { FeedbackContext } from '@/entities/Main/AboutUs/Feedback/FeedbackContext'

export const Feedback = (): JSX.Element => {
	const [slide, setSlide] = useState(0)
	const [tick, setTick] = useState(0)
	return (
		<FeedbackContext.Provider
			value={{
				feedbackList,
				slide,
				setSlide,
				tick,
				setTick
			}}
		>
			<TitleAndButton />
		</FeedbackContext.Provider>
	)
}
