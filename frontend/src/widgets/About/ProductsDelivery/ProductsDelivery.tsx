import { CardList } from '@/widgets/About/ProductsDelivery/CardList/CardList'
import { Label } from '@/widgets/About/ProductsDelivery/Label/Label'

import styles from './ProductsDelivery.module.scss'

export const ProductsDelivery = (): JSX.Element => {
	return (
		<div className={styles.container}>
			<Label />
			<CardList />
		</div>
	)
}
