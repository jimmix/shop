import { CardItem } from '@/widgets/About/ProductsDelivery/CardItem/CardItem'
import { cardList } from '@/widgets/About/ProductsDelivery/CardList/cardList'

import styles from './CardList.module.scss'

export const CardList = (): JSX.Element => {
	return (
		<div className={styles.container}>
			{cardList.map(item => (
				<CardItem
					imagePath={item.imagePath}
					title={item.title}
					key={item.title}
				/>
			))}
		</div>
	)
}
