interface cardItem {
	title: string
	imagePath: string
}

export const cardList: cardItem[] = [
	{
		title: 'Свежие',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/bread.png'
	},
	{
		title: 'Натуральные',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/organic.png'
	},
	{
		title: 'Полезные',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/pomegranate.png'
	},
	{
		title: 'Без ГМО',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/no-gmo.png'
	},
	{
		title: 'Вкусные',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/apple.png'
	},
	{
		title: 'Здоровые',
		imagePath: '/images/aboutUs/ProductsDelivery/CardItems/diet.png'
	}
]
