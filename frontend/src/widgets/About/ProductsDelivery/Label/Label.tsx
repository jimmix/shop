import Image from 'next/image'
import { Slide } from 'react-awesome-reveal'

import styles from './Label.module.scss'

export const Label = (): JSX.Element => {
	return (
		<Slide
			direction={'up'}
			triggerOnce={true}
			cascade={true}
			delay={100}
			fraction={0.2}
		>
			<div className={styles.container}>
				<div>
					<div className={styles.title}>Какие продукты мы доставим?</div>
					<div className={styles.description}>
						С заботой к нашим покупателям! Мы тщательно все проверяем перед
						отправкой и работаем с душой. Кушаем сами, делимся с вами!
					</div>
				</div>
				<Image
					alt={'Корзина продуктов'}
					src={'/images/aboutUs/ProductsDelivery/card.png'}
					width={344}
					height={285}
					className={styles.photo}
				/>
			</div>
		</Slide>
	)
}
