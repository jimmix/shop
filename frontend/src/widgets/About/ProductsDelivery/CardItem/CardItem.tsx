import Image from 'next/image'

import { CardItemProps } from '@/widgets/About/ProductsDelivery/CardItem/CardItem.props'

import styles from './CardItem.module.scss'

export const CardItem = ({
	title,
	imagePath,
	...props
}: CardItemProps): JSX.Element => {
	return (
		<div className={styles.container}>
			<div className={styles.image}>
				<Image
					alt={'Изображение карточки'}
					width={60}
					height={60}
					src={imagePath}
				/>
			</div>
			<div className={styles.title}>{title}</div>
		</div>
	)
}
