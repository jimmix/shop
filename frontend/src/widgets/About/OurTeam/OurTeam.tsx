import { PersonCard } from '@/widgets/About/OurTeam/PersonCard/PersonCard'

import { HTag } from '@/shared/ui/HTag/HTag'
import { SliderPerPageAuto } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto'

import styles from './OurTeam.module.scss'

export const OurTeam = (): JSX.Element => {
	const items = [
		<PersonCard
			fullName={'Екатерина Колодезева'}
			imagePath={'/images/aboutUs/OurTeam/katya_kolodezevaa.png'}
		/>,
		<PersonCard
			fullName={'Андрей Скворцов'}
			imagePath={'/images/aboutUs/OurTeam/andrey_sk.jpg'}
		/>,
	]
	return (
		<div className={styles.container}>
			<HTag tag={'h1'} style={{marginBottom: 40}}>
				Ваша молодая и<br />
				активная команда Фер.Мир
			</HTag>
			<SliderPerPageAuto items={items} />
		</div>
	)
}
