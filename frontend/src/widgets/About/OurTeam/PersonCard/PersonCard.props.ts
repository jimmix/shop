import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface PersonCardProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	fullName: string
	imagePath: string
}
