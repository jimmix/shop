import Image from 'next/image'

import { PersonCardProps } from '@/widgets/About/OurTeam/PersonCard/PersonCard.props'

import styles from './PersonCard.module.scss'

export const PersonCard = ({
	fullName,
	imagePath
}: PersonCardProps): JSX.Element => {
	return (
		<div className={styles.container}>
			<Image
				alt={'Сотрудник'}
				src={imagePath}
				className={styles.photoContainer}
				width={400}
				height={660}
			/>
			<div className={styles.name}>{fullName}</div>
		</div>
	)
}
