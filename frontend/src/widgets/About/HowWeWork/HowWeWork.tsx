import React from 'react'

import { articleList } from '@/widgets/About/HowWeWork/ArticleList'
import { CardItem } from '@/widgets/About/HowWeWork/CardItem/CardItem'
import { ProgressCircle } from '@/widgets/About/HowWeWork/Progress/ProgressCircle/ProgressCircle'
import { ProgressLine } from '@/widgets/About/HowWeWork/Progress/ProgressLine/ProgressLine'
import { UseProgressFill } from '@/widgets/About/HowWeWork/Progress/hooks/UseProgressFill'

import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './HowWeWork.module.scss'

export const HowWeWork = (): JSX.Element => {
	UseProgressFill()
	return (
		<div className={styles.wrapper}>
			<HTag tag={'h2'}>Как мы работаем</HTag>
			<div className={styles.container} id={'progress-how'}>
				<div className={styles.flexLeft}>
					<CardItem
						title={articleList[0].title}
						description={articleList[0].description}
						pathImage={articleList[0].imagePath}
						widthImage={articleList[0]?.widthImage || 56}
						heightImage={articleList[0]?.heightImage || 56}
					/>
					<ProgressCircle
						stage={1}
						id={'circle-start'}
						children={<ProgressLine />}
					/>
				</div>
				<div className={styles.flexRight}>
					<ProgressCircle stage={2} />
					<CardItem
						title={articleList[1].title}
						description={articleList[1].description}
						pathImage={articleList[1].imagePath}
						widthImage={articleList[1]?.widthImage || 56}
						heightImage={articleList[1]?.heightImage || 56}
					/>
				</div>
				<div className={styles.flexLeft}>
					<CardItem
						title={articleList[2].title}
						description={articleList[2].description}
						pathImage={articleList[2].imagePath}
						widthImage={articleList[2]?.widthImage || 56}
						heightImage={articleList[2]?.heightImage || 56}
					/>
					<ProgressCircle stage={3} />
				</div>
				<div className={styles.flexRight}>
					<ProgressCircle stage={4} id={'circle-end'} />
					<CardItem
						title={articleList[3].title}
						description={articleList[3].description}
						pathImage={articleList[3].imagePath}
						widthImage={articleList[3]?.widthImage || 56}
						heightImage={articleList[3]?.heightImage || 56}
					/>
				</div>
			</div>
		</div>
	)
}
