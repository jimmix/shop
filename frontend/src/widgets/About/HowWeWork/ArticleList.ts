interface IArticleItem {
	title: string
	description: string
	imagePath: string
	widthImage?: number
	heightImage?: number
}

export const articleList: IArticleItem[] = [
	{
		title: 'Ищем и отбираем производителей',
		description:
			'Мы поддерживаем маленьких производителей, семейные фермы, небольшие хозяйства. Таких продуктов не найти в других магазинах.',
		imagePath: '/images/aboutUs/search.png'
	},
	{
		title: 'Проверка качества',
		description:
			'Заботимся как о качестве продукции так и покупателях.  Проверяем техническую документацию и проводим исследование продукции поставщика.',
		imagePath: '/images/aboutUs/flask.png'
	},
	{
		title: 'Сотрудничество',
		description:
			'Мы очень внимательно следим за качеством продуктов и регулярно проводим проверки чтобы убедиться, что продукция продолжает соответствовать заявленному качеству.',
		imagePath: '/images/aboutUs/Handshake.png',
		widthImage: 64,
		heightImage: 64
	},
	{
		title: 'Доставка',
		description:
			'Доставка с улыбкой до вашей квартиры чтобы вы могли заняться полезными делами пока продукты едут к вам.',
		imagePath: '/images/aboutUs/delivery-truck.png'
	}
]
