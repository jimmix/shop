import Image from 'next/image'
import { Slide } from 'react-awesome-reveal'

import { CardItemProps } from '@/widgets/About/HowWeWork/CardItem/CardItem.props'

import styles from './CardItem.module.scss'

export const CardItem = ({
	title,
	description,
	pathImage,
	widthImage = 56,
	heightImage = 56
}: CardItemProps): JSX.Element => {
	return (
		<Slide
			direction={'up'}
			triggerOnce={true}
			cascade={true}
			delay={100}
			fraction={0.2}
		>
			<div className={styles.container}>
				<div className={styles.image}>
					<Image
						alt={'Описание иконки'}
						src={pathImage}
						width={widthImage}
						height={heightImage}
					/>
				</div>
				<div>
					<div className={styles.title}>{title}</div>
					<div className={styles.description}>{description}</div>
				</div>
			</div>
		</Slide>
	)
}
