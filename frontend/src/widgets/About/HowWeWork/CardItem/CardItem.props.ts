import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface CardItemProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	title: string
	description: string
	pathImage: string
	widthImage?: number
	heightImage?: number
}
