import styles from './ProgressLine.module.scss'

export const ProgressLine = (): JSX.Element => {
	return <div className={styles.line} id={'about-progress-line'} />
}
