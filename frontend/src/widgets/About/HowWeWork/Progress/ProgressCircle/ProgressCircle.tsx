import cl from 'classnames'

import { ProgressCircleProps } from '@/widgets/About/HowWeWork/Progress/ProgressCircle/ProgressCircle.props'

import styles from './ProgressCircle.module.scss'

export const ProgressCircle = ({
	stage,
	className,
	...props
}: ProgressCircleProps): JSX.Element => {
	return (
		<div
			className={cl(styles.circle, className)}
			data-value={stage}
			{...props}
		/>
	)
}
