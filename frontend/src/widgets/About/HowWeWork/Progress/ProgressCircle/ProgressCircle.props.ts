import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface ProgressCircleProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	stage: number
}
