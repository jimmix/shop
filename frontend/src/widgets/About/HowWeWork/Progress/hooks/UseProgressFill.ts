import { useEffect } from 'react'

export function UseProgressFill() {
	useEffect(() => {
		const circleStart = document.querySelector(
			'#circle-start'
		) as HTMLDivElement

		const circleEnd = document.querySelector('#circle-end') as HTMLDivElement

		const progressLine = document.querySelector(
			'#about-progress-line'
		) as HTMLDivElement

		const callBack = (event: Event | null = null) => {
			const currentWidth =
				circleEnd.getBoundingClientRect().top -
				circleStart.getBoundingClientRect().top
			progressLine.style.height = currentWidth + 'px'
		}

		callBack()

		window.addEventListener('resize', callBack)
		return () => {
			window.removeEventListener('resize', callBack)
		}
	}, [])
}
