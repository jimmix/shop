import cl from 'classnames'
import Link from 'next/link'

import { FooterListProps } from '@/widgets/Footer/Navigation/FooterList/FooterList.props'

import styles from './FooterList.module.scss'

export const FooterList = ({
	pages,
	isActive,
	...props
}: FooterListProps): JSX.Element => {
	return (
		<div
			className={cl(styles.footerList, {
				[styles.active]: isActive
			})}
			{...props}
		>
			{pages.map((page, idx) => (
				<Link
					key={page.title}
					className={styles.href}
					href={page.href}
					scroll={page?.scroll !== false}
				>
					{page.title}
				</Link>
			))}
		</div>
	)
}
