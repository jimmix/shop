import { IFooterItem } from '@/entities/Footer/FooterList'

export interface FooterListProps {
	pages: IFooterItem[]
	isActive: boolean
}
