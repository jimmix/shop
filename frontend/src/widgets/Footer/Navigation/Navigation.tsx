import { FooterItem } from '@/widgets/Footer/Navigation/FooterItem/FooterItem'

import { footerList } from '@/entities/Footer/FooterList'

import styles from './Navigation.module.scss'

export const Navigation = (): JSX.Element => {
	return (
		<div className={styles.list}>
			{footerList.map((list, idx) => (
				<FooterItem key={list.title} footerList={list} idx={idx} />
			))}
		</div>
	)
}
