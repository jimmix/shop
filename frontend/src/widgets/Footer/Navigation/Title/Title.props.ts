import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface TitleProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	isActive: boolean
	setIsActive: (isActive: boolean) => void
}
