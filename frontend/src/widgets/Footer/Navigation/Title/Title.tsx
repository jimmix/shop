import cl from 'classnames'
import Image from 'next/image'

import { TitleProps } from '@/widgets/Footer/Navigation/Title/Title.props'

import styles from './Title.module.scss'

export const Title = ({
	children,
	isActive,
	setIsActive,
	...props
}: TitleProps): JSX.Element => {
	const onClickArrowHandler = () => {
		if (window.innerWidth <= 598) {
			setIsActive(!isActive)
		}
	}

	return (
		<div className={styles.container} onClick={onClickArrowHandler} {...props}>
			<div className={styles.title}>{children}</div>
			<div
				className={cl(styles.arrow, {
					[styles.active]: isActive
				})}
			>
				<Image
					alt={'стрелка'}
					src={'/images/footer-arrow.svg'}
					width={12}
					height={12}
				/>
			</div>
		</div>
	)
}
