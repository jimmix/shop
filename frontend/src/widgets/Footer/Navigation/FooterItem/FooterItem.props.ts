import { IFooterList } from '@/entities/Footer/FooterList'

export interface FooterItemProps {
	footerList: IFooterList
	idx: number
}
