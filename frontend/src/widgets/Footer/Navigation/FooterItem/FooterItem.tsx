import { useState } from 'react'

import { FooterItemProps } from '@/widgets/Footer/Navigation/FooterItem/FooterItem.props'
import { FooterList } from '@/widgets/Footer/Navigation/FooterList/FooterList'
import { Title } from '@/widgets/Footer/Navigation/Title/Title'

import styles from './FooterIten.module.scss'

export const FooterItem = ({
	footerList,
	idx
}: FooterItemProps): JSX.Element => {
	const [isActive, setIsActive] = useState<boolean>(idx === 0)

	return (
		<div className={styles.container}>
			<Title isActive={isActive} setIsActive={setIsActive}>
				{footerList.title}
			</Title>
			<FooterList isActive={isActive} pages={footerList.items} />
		</div>
	)
}
