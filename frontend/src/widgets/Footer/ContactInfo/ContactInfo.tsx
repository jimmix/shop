import cl from 'classnames'

import { VkLogo } from '@/shared/lib/icons/VkLogo/VkLogo'

import styles from './ContactInfo.module.scss'
import Image from "next/image";

export const ContactInfo = (): JSX.Element => {
	return (
		<div>
		<div className={styles.container}>
			<VkLogo className={styles.vk} />
			<a className={styles.phone} href='tel:88007162030'>
				8 800 716 20 30
			</a>
			<p className={styles.info}>Звонок бесплатный</p>
			<p className={styles.info}>Ежедневно с 10 до 18</p>
			<a
				className={cl(styles.info, styles.email)}
				href='mailto:FerMir@gmail.com'
			>
				FerMir@gmail.com
			</a>
		</div>
			<div className={styles.fondContainer}>
				<Image src={'/images/fond.svg'} alt={'фонд'} width={73} height={40}/>
				<p className={styles.fond}>
					Проект реализован при поддержке
					Фонда Содействия Инновациям
				</p>
			</div>
		</div>
	)
}
