import styles from './Copyright.module.scss'

export const Copyright = (): JSX.Element => {
	return (
		<div className={styles.container}>
			<div className={styles.copyright}>
				<p className={styles.content}>Фер.Мир © 2023</p>
			</div>
		</div>
	)
}
