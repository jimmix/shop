import cl from 'classnames'

import { ContactInfo } from '@/widgets/Footer/ContactInfo/ContactInfo'
import { Copyright } from '@/widgets/Footer/Copyright/Copyright'
import { FooterProps } from '@/widgets/Footer/Footer.props'
import { Logo } from '@/widgets/Footer/Logo/Logo'
import { Navigation } from '@/widgets/Footer/Navigation/Navigation'
import { WithVkLogo } from '@/widgets/Footer/WithVkLogo/WithVkLogo'

import styles from './Footer.module.scss'

export const Footer = ({ className, ...props }: FooterProps): JSX.Element => {
	return (
		<footer className={cl(styles.footer, className)} {...props}>
			<div className={styles.content}>
				<Navigation />
				<WithVkLogo>
					<ContactInfo />
				</WithVkLogo>
			</div>
			<Copyright />
			<Logo />
		</footer>
	)
}
