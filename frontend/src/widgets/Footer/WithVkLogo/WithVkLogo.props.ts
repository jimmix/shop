import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface WithVkLogoProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}
