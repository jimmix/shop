import { WithVkLogoProps } from '@/widgets/Footer/WithVkLogo/WithVkLogo.props'

import { VkLogo } from '@/shared/lib/icons/VkLogo/VkLogo'

import styles from './WithVkLogo.module.scss'

export const WithVkLogo = ({
	children,
	...props
}: WithVkLogoProps): JSX.Element => {
	return (
		<div className={styles.container} {...props}>
			{children}
			<VkLogo className={styles.vklogo} />
		</div>
	)
}
