import styles from './Logo.module.scss'

export const Logo = (): JSX.Element => {
	return <div className={styles.logo}>Ф</div>
}
