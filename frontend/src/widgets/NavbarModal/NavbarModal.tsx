import cl from 'classnames'
import { useEffect } from 'react'

import { Modal } from '@/shared/ui/Modal/Modal'

import { NavbarItems } from './NavbarItems/NavbarItems'
import styles from './NavbarModal.module.scss'
import { NavbarModalProps } from './NavbarModal.props'
import {useSelector} from "react-redux";
import {getUserAuthData} from "@/entities/User/model/selectors/getUserAuthData/getUserAuthData";

export const NavbarModal = ({
	isOpen,
	onClose,
	...props
}: NavbarModalProps): JSX.Element => {
	const authUser = useSelector(getUserAuthData)

	useEffect(() => {
		if (isOpen) {
			document.body.style.overflow = 'hidden'
		}

		return () => {
			document.body.style.overflow = 'auto'
		}
	})

	return (
		<Modal
			{...props}
			isOpen={isOpen}
			onClose={onClose}
			contentClassName={styles.content}
			containerClassName={styles.containerModal}
		>
			<div {...props} className={cl(styles.container)}>
				<div className={styles.user}>
					<div className={styles.cardUser}>
						<div className={styles.userFill}>
							<span>{authUser?.name.charAt(0) ?? 'Ф'}</span>
						</div>
						<div className={styles.username}>{authUser?.name}</div>
					</div>
				</div>
				<NavbarItems onClose={onClose} />
			</div>
		</Modal>
	)
}
