import cl from 'classnames'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'

import { userActions } from '@/entities/User/model/slice/userSlice'

import styles from './NavbarItems.module.scss'
import { NavbarItemsProps } from './NavbarItems.props'
import { FavoriteIcon } from './icons/FavoriteIcon'
import { LogoutIcon } from './icons/LogoutIcon'
import { MainIcon } from './icons/MainIcon'
import { OrderIcon } from './icons/OrderIcon'
import { SettingIcon } from './icons/SettingIcon'

export const NavbarItems = ({
	onClose,
	...props
}: NavbarItemsProps): JSX.Element => {
	const dispatch = useDispatch()
	const router = useRouter()

	const logout = async () => {
		onClose()
		await router.push('/')
		dispatch(userActions.logout())
	}

	const redirectCabinet = () => {
		router.push('/cabinet/setting')
	}

	return (
		<div {...props} className={cl(styles.container)}>
			<div className={styles.containerMain}>
				<div className={styles.item}>
					<div className={styles.title}>Главная</div>
					<div className={styles.iconContainer}>
						<MainIcon className={cl(styles.icon, styles.iconMain)} />
					</div>
				</div>

				<div className={styles.item}>
					<div className={styles.title}>Избранное</div>
					<div className={styles.iconContainer}>
						<FavoriteIcon className={styles.icon} />
					</div>
				</div>

				<div className={styles.item}>
					<div className={styles.title}>Заказы</div>
					<div className={styles.iconContainer}>
						<OrderIcon className={styles.iconFill} />
					</div>
				</div>

				<div className={styles.item} onClick={redirectCabinet}>
					<div className={styles.title}>Настройки</div>
					<div className={styles.iconContainer}>
						<SettingIcon className={styles.iconFill} />
					</div>
				</div>
			</div>
			<div className={styles.containerLogout} onClick={logout}>
				<div className={styles.item}>
					<div className={styles.title}>Выход</div>
					<div className={styles.iconContainer}>
						<LogoutIcon className={styles.iconFill} />
					</div>
				</div>
			</div>
		</div>
	)
}
