import { SVGProps } from 'react'

export const OrderIcon = ({
	...props
}: SVGProps<SVGSVGElement>): JSX.Element => {
	return (
		<svg
			version='1.1'
			x='0px'
			y='0px'
			viewBox='0 0 256 256'
			enableBackground={'new 0 0 256 256'}
			{...props}
		>
			<g>
				<g>
					<g>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							fill='green'
							d='M51.5,10.3c-3.7,0.9-8.4,5.6-9.4,9.3C41.7,21,41.6,56,41.6,129l0.1,107.4l1.1,2c1.2,2.3,4.1,5.2,6.6,6.5c1.7,0.9,4.7,0.9,78,1.1l76.3,0.1l2.6-1.2c3-1.3,5.6-3.8,7-6.5c0.9-1.9,0.9-4,1.1-86.9c0.1-77.4,0-85.2-0.7-87.4c-0.7-2.3-2.9-4.6-25.1-26.8c-17.4-17.5-24.9-24.8-26.7-25.7l-2.5-1.3L106.2,10C77.1,10,52.4,10.1,51.5,10.3z M149.1,46.8l0.1,28.3l28.3,0.1l28.3,0.1l0.1,79.9l0.1,79.9l-1.2,1.2l-1.2,1.2h-75.5c-73.7,0-75.6,0-76.7-0.9l-1.2-0.9l-0.1-107c-0.1-69.9,0-107.4,0.4-108.2c0.2-0.7,1-1.5,1.6-1.8c0.7-0.2,21.1-0.4,49.1-0.3l47.9,0.1L149.1,46.8z M181.9,42.3c13,13,23.6,23.8,23.6,24c0,0.2-10.8,0.4-24,0.4h-24v-24c0-13.2,0.1-24,0.3-24C158.2,18.7,169,29.4,181.9,42.3z'
						/>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							fill='green'
							d='M64.8,108.9v4.2H128h63.2v-4.2v-4.2H128H64.8V108.9z'
						/>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							d='M64.8,134.1v4.2H128h63.2v-4.2v-4.2H128H64.8V134.1z'
						/>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							fill='green'
							d='M64.9,155.7c-0.1,0.4-0.2,2.3-0.1,4.3l0.1,3.7l63.1,0.2l63.1,0.1v-4.4v-4.4h-63C75.9,155.1,65.1,155.2,64.9,155.7z'
						/>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							fill='green'
							d='M64.8,185v4.2H128h63.2V185v-4.2H128H64.8V185z'
						/>
						<path
							stroke='green'
							strokeWidth={'3'}
							strokeLinejoin={'round'}
							fill='green'
							d='M64.8,210.2v4.2H128h63.2v-4.2V206H128H64.8V210.2z'
						/>
					</g>
				</g>
			</g>
		</svg>
	)
}
