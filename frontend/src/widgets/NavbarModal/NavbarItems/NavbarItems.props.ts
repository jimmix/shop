import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface NavbarItemsProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	onClose: () => void
}
