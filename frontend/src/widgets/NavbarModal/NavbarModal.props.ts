import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface NavbarModalProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	isOpen: boolean
	onClose: () => void
}
