import { ArticleProps } from '@/widgets/Main/AbouUs/Article/Article.props'

import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './Article.module.scss'

export const Article = ({ title, description }: ArticleProps): JSX.Element => {
	return (
		<div>
			<HTag className={styles.title} tag={'h3'}>
				{title}
			</HTag>
			<p className={styles.description}>{description}</p>
		</div>
	)
}
