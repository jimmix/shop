export interface ArticleProps {
	title: string
	description: string
}
