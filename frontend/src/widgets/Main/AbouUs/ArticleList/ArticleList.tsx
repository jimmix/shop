import { Article } from '@/widgets/Main/AbouUs/Article/Article'

import { articleList } from '@/entities/Main/AboutUs/ArticleList'

import styles from './ArticleList.module.scss'

export const ArticleList = (): JSX.Element => {
	return (
		<div className={styles.list}>
			{articleList.items.map((article, idx) => (
				<Article
					key={idx}
					description={article.description}
					title={article.title}
				/>
			))}
		</div>
	)
}
