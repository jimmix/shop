import { ArticleList } from '@/widgets/Main/AbouUs/ArticleList/ArticleList'

import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './AboutUs.module.scss'

export const AboutUs = (): JSX.Element => {
	return (
		<div className={styles.container}>
			<HTag tag={'h1'} className={styles.h1}>
				О нас
			</HTag>
			<ArticleList />
		</div>
	)
}
