import { Category } from '@/entities/Category/types/Category'

import styles from './CatalogList.module.scss'
import { CategoryItem } from './CategoryItem/CategoryItem'

interface CatalogListI {
	categories: Category[]
}

export const CatalogList = ({ categories }: CatalogListI): JSX.Element => {
	return (
		<>
			<div className={styles.title}>Каталог</div>
			<div className={styles.container}>
				{categories.map(category => (
					<CategoryItem
						key={category.id}
						className={styles.catalogItem}
						name={category.name}
						pathImage={category.bigImage}
						id={category.id}
					/>
				))}
			</div>
			<></>
		</>
	)
}
