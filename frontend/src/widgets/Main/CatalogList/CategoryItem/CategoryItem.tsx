import cl from 'classnames'

import { CategoryItemProps } from '@/widgets/Main/CatalogList/CategoryItem/CategoryItem.props'

import { config } from '@/shared/constaints/Config'

import styles from './CategoryItem.module.scss'
import {useRouter} from "next/router";

export const CategoryItem = ({
	name,
	id,
	pathImage,
	className
}: CategoryItemProps): JSX.Element => {
	const router = useRouter()
	const goToCatalog = async () => {
		await router.push('catalog/' + id)
	}

	return (
		<div
			className={cl(styles.container, className)}
			style={{
				backgroundImage: `url(${config.backendStoragePath}/${pathImage})`,
				color: 'black'
			}}
			onClick={goToCatalog}
		>
			<div className={styles.name}>{name}</div>
		</div>
	)
}
