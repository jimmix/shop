import cl from 'classnames'
import Image from 'next/image'
import { useContext, useMemo } from 'react'

import AnswerProps from '@/widgets/Main/QuestionsAndAnswers/Answers/Answer.props'
import { QuestionMobile } from '@/widgets/Main/QuestionsAndAnswers/Questions/QuestionMobile/QuestionMobile'

import { AnswerContext } from '@/entities/Main/QuestionsAndAnswers/AnswerList/Service/AnswerContext'

import styles from './Answer.module.scss'

export const Answer = ({
	answer,
	pathImage,
	idx,
	question
}: AnswerProps): JSX.Element => {
	const { index, setIndex } = useContext(AnswerContext)
	const isActive = idx === index

	return useMemo(
		() => (
			<div>
				<div
					className={cl(styles.container, {
						[styles.active]: isActive
					})}
					onClick={() => setIndex(idx)}
				>
					<div className={styles['container-image']}>
						<Image
							className={styles.image}
							src={pathImage}
							alt={'Изображение вопроса'}
							width={150}
							height={150}
						/>
					</div>
					<div className={styles.answer}>{answer}</div>
				</div>
				<QuestionMobile
					answer={answer}
					isActive={isActive}
					question={question}
				/>
			</div>
		),
		[isActive]
	)
}
