export default interface AnswerProps {
	pathImage: string
	answer: string
	idx: number
	question: string
}
