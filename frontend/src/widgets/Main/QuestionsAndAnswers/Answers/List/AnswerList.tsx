import { Answer } from '@/widgets/Main/QuestionsAndAnswers/Answers/Answer'
import styles from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers.module.scss'

import { answerList } from '@/entities/Main/QuestionsAndAnswers/AnswerList/AnswerList'

export const AnswerList = (): JSX.Element => {
	return (
		<div className={styles.answers}>
			{answerList.items.map((answer, idx) => (
				<Answer
					answer={answer.question}
					pathImage={answer.imagePath}
					key={answer.question}
					question={answer.answer}
					idx={idx}
				/>
			))}
		</div>
	)
}
