import { useMemo, useState } from 'react'

import { ContainerBottom } from '@/widgets/Main/QuestionsAndAnswers/Containers/Bottom/ContainerBottom'
import { ContainerTop } from '@/widgets/Main/QuestionsAndAnswers/Containers/Top/ContainerTop'

import { answerList } from '@/entities/Main/QuestionsAndAnswers/AnswerList/AnswerList'
import { IAnswer } from '@/entities/Main/QuestionsAndAnswers/AnswerList/AnswerList.interface'
import { AnswerContext } from '@/entities/Main/QuestionsAndAnswers/AnswerList/Service/AnswerContext'

const maxLength = answerList.items.length - 1

export const QuestionsAndAnswers = (): JSX.Element => {
	const [index, setIndex] = useState(0)
	const answer: IAnswer = useMemo(() => answerList.items[index], [index])

	return (
		<AnswerContext.Provider value={{ index, setIndex, answer, maxLength }}>
			<ContainerTop />
			<ContainerBottom />
		</AnswerContext.Provider>
	)
}
