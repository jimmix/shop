import cl from 'classnames'
import { useContext, useMemo } from 'react'

import { AnswerContext } from '@/entities/Main/QuestionsAndAnswers/AnswerList/Service/AnswerContext'

import styles from './Question.module.scss'

export const Question = (): JSX.Element => {
	const { answer } = useContext(AnswerContext)

	return useMemo(
		() => (
			<div className={cl(styles.container)}>
				<p className={cl(styles.question)}>{answer.question}</p>
				<p className={cl(styles.answer)}>{answer.answer}</p>
			</div>
		),
		[answer]
	)
}
