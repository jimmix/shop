import { QuestionMobileProps } from '@/widgets/Main/QuestionsAndAnswers/Questions/QuestionMobile/QuestionMobile.props'

import styles from './QuestionMobile.module.scss'

export const QuestionMobile = ({
	answer,
	question,
	isActive
}: QuestionMobileProps): JSX.Element => {
	return isActive ? (
		<div className={styles.container}>
			<div className={styles.answer}>{answer}</div>
			<div className={styles.question}>{question}</div>
		</div>
	) : (
		<></>
	)
}
