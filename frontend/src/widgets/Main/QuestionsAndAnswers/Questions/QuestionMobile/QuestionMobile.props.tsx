import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface QuestionMobileProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	question: string
	answer: string
	isActive: boolean
}
