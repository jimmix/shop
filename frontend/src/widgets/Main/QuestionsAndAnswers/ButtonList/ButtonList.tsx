import { ButtonArrow } from '@/widgets/Main/QuestionsAndAnswers/ButtonList/ButtonArrow/ButtonArrow'
import styles from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers.module.scss'

export const ButtonList = (): JSX.Element => {
	return (
		<div className={styles['btn-list']}>
			<ButtonArrow active={'left'} />
			<ButtonArrow active={'right'} />
		</div>
	)
}
