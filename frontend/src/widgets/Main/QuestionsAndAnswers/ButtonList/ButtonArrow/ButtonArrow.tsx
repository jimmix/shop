import cl from 'classnames'
import { useContext, useMemo } from 'react'

import { ButtonArrowProps } from '@/widgets/Main/QuestionsAndAnswers/ButtonList/ButtonArrow/ButtonArrow.props'
import styles from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers.module.scss'

import { AnswerContext } from '@/entities/Main/QuestionsAndAnswers/AnswerList/Service/AnswerContext'

import { Button } from '@/shared/ui/Button/Button'

export const ButtonArrow = ({
	active,
	...props
}: ButtonArrowProps): JSX.Element => {
	const { index, setIndex, maxLength } = useContext(AnswerContext)

	const isDisabled = useMemo(() => {
		switch (true) {
			case active === 'left' && index === 0:
				return true
			case active === 'right' && index === maxLength:
				return true
			default:
				return false
		}
	}, [index])

	const onClickHandler = () => {
		switch (true) {
			case active === 'left' && !isDisabled:
				setIndex(index - 1)
				break
			case active === 'right' && !isDisabled:
				setIndex(index + 1)
				break
		}
	}

	return useMemo(
		() => (
			<Button
				appearance={isDisabled ? 'ghost' : 'primary'}
				className={cl(styles.btn, styles[`btn-${active}`], {
					[styles.disabled]: isDisabled
				})}
				onClick={() => onClickHandler()}
				{...props}
			>
				<div className={styles[`arrow-${active}`]}></div>
			</Button>
		),
		[index]
	)
}
