import cl from 'classnames'

import { AnswerList } from '@/widgets/Main/QuestionsAndAnswers/Answers/List/AnswerList'
import { Question } from '@/widgets/Main/QuestionsAndAnswers/Questions/Question'
import styles from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers.module.scss'

import { VkLogo } from '@/shared/lib/icons/VkLogo/VkLogo'

export const ContainerBottom = (): JSX.Element => {
	return (
		<div>
			<div className={cl(styles['container-bottom'])}>
				<AnswerList />
				<div className={styles.question}>
					<Question />
					<a href='https://vk.com' target={'_blank'}>
						<VkLogo className={styles.vkLogo} />
					</a>
				</div>
			</div>
		</div>
	)
}
