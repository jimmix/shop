import cl from 'classnames'

import { ButtonList } from '@/widgets/Main/QuestionsAndAnswers/ButtonList/ButtonList'
import styles from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers.module.scss'

import { VkLogo } from '@/shared/lib/icons/VkLogo/VkLogo'
import { HTag } from '@/shared/ui/HTag/HTag'

export const ContainerTop = (): JSX.Element => {
	return (
		<div className={cl(styles['container-top'])} id={'questions'}>
			<HTag tag={'h1'}>Вопросы и ответы</HTag>
			<ButtonList />
			<VkLogo className={styles.vkLogoMobile} />
		</div>
	)
}
