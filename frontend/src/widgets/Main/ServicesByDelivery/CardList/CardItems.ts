interface CardItem {
	imagePath: string
	title: string
	description: string
}

interface CardList {
	items: CardItem[]
}

export const list: CardList = {
	items: [
		{
			title: 'С душой',
			description:
				'Мы хотим, чтобы продукты, которые вы выбираете, вас радовали',
			imagePath: '/images/main/servicesByDelivery/like.png'
		},
		{
			title: 'Такое сложно найти',
			description:
				'Поставщики нашей продукции - маленькие хозяйства, фермы',
			imagePath: '/images/main/servicesByDelivery/coins.png'
		},
		{
			title: 'Тщательная проверка',
			description: 'Продукция попадает к вам после процедуры оценки контроля качества',
			imagePath: '/images/main/servicesByDelivery/salad.png'
		},
		{
			title: 'Качественные продукты',
			description:
				'Только свежее, вкусное, натуральное - без ГМО и добавок',
			imagePath: '/images/main/servicesByDelivery/farm.png'
		}
	]
}
