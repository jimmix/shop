import cl from 'classnames'

import { CardItem } from '@/widgets/Main/ServicesByDelivery/CardItem/CardItem'

import { list } from './CardItems'
import styles from './CardList.module.scss'

export const CardList = (): JSX.Element => {
	return (
		<div className={cl(styles.list)}>
			{list.items.map(item => (
				<CardItem
					key={item.title}
					title={item.title}
					description={item.description}
					imagePath={item.imagePath}
				></CardItem>
			))}
		</div>
	)
}
