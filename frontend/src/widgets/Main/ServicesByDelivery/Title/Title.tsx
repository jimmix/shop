import cl from 'classnames'

import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './Title.module.scss'

export const Title = (): JSX.Element => {
	return (
		<div>
			<HTag tag={'h1'}>
				Сервис по доставке{' '}
				<span className={cl(styles.primary)}>настоящих продуктов</span>
			</HTag>
		</div>
	)
}
