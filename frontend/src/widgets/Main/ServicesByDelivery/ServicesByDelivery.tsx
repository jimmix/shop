import cl from 'classnames'

import { CardList } from '@/widgets/Main/ServicesByDelivery/CardList/CardList'
import { Title } from '@/widgets/Main/ServicesByDelivery/Title/Title'

import styles from './ServicesByDelivery.module.scss'

export const ServicesByDelivery = (): JSX.Element => {
	return (
		<div className={cl(styles.container)}>
			<Title />
			<CardList />
		</div>
	)
}
