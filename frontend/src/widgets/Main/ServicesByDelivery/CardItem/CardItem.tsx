import cl from 'classnames'
import Image from 'next/image'

import { CardItemProps } from '@/widgets/Main/ServicesByDelivery/CardItem/CardItem.props'

import styles from './CardItem.module.scss'

export const CardItem = ({
	imagePath,
	title,
	description,
	className,
	...props
}: CardItemProps): JSX.Element => {
	return (
		<div className={cl(className, styles.container)} {...props}>
			<p className={cl(styles.title)}>{title}</p>
			<p className={cl(styles.description)}>{description}</p>
			<div className={styles.image}>
				<Image src={imagePath} alt={'Изображение'} width={150} height={150} />
			</div>
		</div>
	)
}
