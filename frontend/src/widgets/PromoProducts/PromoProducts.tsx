import cl from 'classnames'
import { MutableRefObject, createRef, useState } from 'react'

import { Product } from '@/widgets/Core/Product/Product'

import { HTag } from '@/shared/ui/HTag/HTag'
import { ButtonItem } from '@/shared/ui/Sliders/ButtonItem/ButtonItem'
import { ButtonListContainer } from '@/shared/ui/Sliders/ButtonListContainer/ButtonListContainer'
import { SliderPerPageAuto } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto'

import styles from './PromoProducts.module.scss'
import { PromoProductsProps } from './PromoProducts.props'

export const PromoProducts = ({
	title,
	products,
	...props
}: PromoProductsProps): JSX.Element => {
	const [isStart, setIsStart] = useState(true)
	const [isEnd, setIsEnd] = useState(true)
	const prevRef = createRef() as MutableRefObject<HTMLButtonElement>
	const nextRef = createRef() as MutableRefObject<HTMLButtonElement>

	return (
		<>
			<div className={styles.containerHeader}>
				<HTag tag={'h1'}>{title}</HTag>
				<ButtonListContainer>
					<ButtonItem arrow={'left'} innerRef={prevRef} isDisabled={isStart} />
					<ButtonItem arrow={'right'} innerRef={nextRef} isDisabled={isEnd} />
				</ButtonListContainer>
			</div>
			<div
				className={cl(styles.containerList)}
				style={{ maxWidth: '100%', minWidth: '100%' }}
			>
				<SliderPerPageAuto
					items={products.map(product => (
						<Product
							author={product?.provider}
							availableDays={product.availableDays}
							basePrice={product.basePrice}
							id={product.id}
							name={product.name}
							discount={product.discount}
							quantity={product.quantity}
							weight={product.weight}
							image={product.image}
							key={product.id}
						/>
					))}
					nextRef={nextRef}
					prevRef={prevRef}
					setIsEnd={setIsEnd}
					setIsStart={setIsStart}
				/>
			</div>
		</>
	)
}
