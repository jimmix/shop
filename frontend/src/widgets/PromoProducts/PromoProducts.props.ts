import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { Product } from '@/entities/Product/types/product'

export interface PromoProductsProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	products: Product[]
	title: string
}
