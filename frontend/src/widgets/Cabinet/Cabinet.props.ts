import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface CabinetProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	tab: string
}
