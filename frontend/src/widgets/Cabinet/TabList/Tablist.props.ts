import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface TablistProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    tab: string
}