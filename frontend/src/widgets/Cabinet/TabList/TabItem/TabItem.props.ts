import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface TabItemProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	title: string
	imagePath: string
	active: boolean
	link?: string
	tab: string
}
