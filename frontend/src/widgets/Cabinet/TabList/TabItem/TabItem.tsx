import cl from 'classnames'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'

import { userActions } from '@/entities/User/model/slice/userSlice'

import styles from './TabItem.module.scss'
import { TabItemProps } from './TabItem.props'

export const TabItem = ({
	active,
	imagePath,
	title,
	link,
	tab,
	...props
}: TabItemProps): null | JSX.Element => {
	const router = useRouter()
	const dispatch = useDispatch()

	const onClickHandler = (e: React.MouseEvent) => {
		if (tab === 'exit') {
			e.preventDefault()
			dispatch(userActions.logout())
			router.push('/')
		}
	}

	return (
		<Link
			className={cl(styles.container, {
				[styles.activeContainer]: active
			})}
			href={link || '/'}
			onClick={onClickHandler}
		>
			<div
				className={cl(styles.icon, {
					[styles.activeIcon]: active
				})}
			>
				<Image src={imagePath} alt={'icon'} width={26} height={26} />
			</div>
			<div className={styles.title}>{title}</div>
		</Link>
	)
}
