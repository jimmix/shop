import cl from 'classnames'

import { TabItem } from './TabItem/TabItem'
import styles from './Tablist.module.scss'
import { TablistProps } from './Tablist.props'
import tabList from './constaints/tabList'

export const Tablist = ({ tab, ...props }: TablistProps): JSX.Element => {
	return (
		<div {...props} className={cl(styles.container)}>
			{tabList.map(item => (
				<TabItem
					title={item.title}
					imagePath={item.imagePath}
					key={item.title}
					active={item.tab === tab}
					tab={item.tab}
					link={item?.link}
				/>
			))}
		</div>
	)
}
