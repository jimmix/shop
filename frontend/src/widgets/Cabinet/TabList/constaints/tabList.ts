interface ITabItem {
	title: string
	imagePath: string
	tab: string
	link?: string
}

const tabList: ITabItem[] = [
	{
		title: 'Личный кабинет',
		imagePath: '/images/cabinet/icons/man.png',
		tab: 'setting',
		link: '/cabinet/setting'
	},
	{
		title: 'Мои отзывы',
		imagePath: '/images/cabinet/icons/finger.png',
		tab: 'feedback',
		link: '/cabinet/feedback'
	},
	{
		title: 'Рефералы',
		imagePath: '/images/cabinet/icons/ref.png',
		tab: 'ref',
		link: '/cabinet/ref'
	},
	{
		title: 'Система лояльности',
		imagePath: '/images/cabinet/icons/surprice.png',
		tab: 'loyalty-system',
		link: '/cabinet/loyalty-system'
	},
	{
		title: 'Пароль',
		imagePath: '/images/cabinet/icons/lock.png',
		tab: 'change-password',
		link: '/cabinet/change-password'
	},
	{
		title: 'Выход',
		imagePath: '/images/cabinet/icons/exit.png',
		tab: 'exit'
	}
]

export default tabList
