import cl from 'classnames'

import styles from './Cabinet.module.scss'
import { CabinetProps } from './Cabinet.props'
import { Content } from './Content/Content'
import { Tablist } from './TabList/Tablist'

export const Cabinet = ({
	children,
	tab,
	...props
}: CabinetProps): JSX.Element => {
	return (
		<section>
			<div className={styles.title}>Настройки</div>
			<div {...props} className={cl(styles.container)}>
				<Tablist tab={tab} />
				{children}
			</div>
		</section>
	)
}
