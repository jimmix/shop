import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface SettingContentProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}
