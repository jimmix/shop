import cl from 'classnames'
import { useState } from 'react'

import { Detail } from './Detail/Detail'
import { Edit } from './Edit/Edit'
import styles from './SettingContent.module.scss'
import { SettingContentProps } from './SettingContent.props'

type Mode = 'detail' | 'edit'
export const SettingContent = ({
	...props
}: SettingContentProps): JSX.Element => {
	const [mode, setMode] = useState<Mode>('detail')

	const setEditMode = () => {
		setMode('edit')
	}

	const setDetailMode = () => {
		setMode('detail')
	}

	return (
		<div {...props} className={cl(styles.container)}>
			{mode === 'detail' ? (
				<Detail onSwapMode={setEditMode} />
			) : (
				<Edit onSwapMode={setDetailMode} />
			)}
		</div>
	)
}
