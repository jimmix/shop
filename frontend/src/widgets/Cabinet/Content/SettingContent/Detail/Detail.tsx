import Image from 'next/image'
import Skeleton from 'react-loading-skeleton'
import { useSelector } from 'react-redux'

import { getUserAuthData } from '@/entities/User/model/selectors/getUserAuthData/getUserAuthData'

import { config } from '@/shared/constaints/Config'
import { Button } from '@/shared/ui/Button/Button'

import styles from '../SettingContent.module.scss'

import { DetailProps } from './Detail.props'

export const Detail = ({ onSwapMode, ...props }: DetailProps): JSX.Element => {
	const authUser = useSelector(getUserAuthData)
	const isLoading = authUser?.id
	return (
		<>
			<div className={styles.userContainer}>
				<div className={styles.imageContainer}>
					{!isLoading ? (
						<Skeleton className={styles.skeleton} />
					) : authUser?.imagePath ? (
						<Image
							className={styles.image}
							src={`${config.backendStoragePath}/${authUser.imagePath}`}
							alt={'Фотография пользователя'}
							width={140}
							height={140}
							priority

						/>
					) : (
						<div className={styles.userFill}>
							<span>{authUser?.name.charAt(0) ?? 'Ф'}</span>
						</div>
					)}
				</div>

				<div>
					{!isLoading ? (
						<Skeleton width={250} />
					) : (
						<div className={styles.name}>{authUser?.name}</div>
					)}
					{!isLoading ? (
						<Skeleton width={150} />
					) : (
						<div className={styles.city}>Новосибирск</div>
					)}
				</div>
			</div>
			<div className={styles.formContainer}>
				<div className={styles.propertyValueContainer}>
					<div className={styles.property}>Контактный телефон</div>
					<div className={styles.value}>{authUser?.phone || ''}</div>
				</div>

				<div className={styles.propertyValueContainer}>
					<div className={styles.property}>E-mail</div>
					{!isLoading ? (
						<Skeleton width={250} />
					) : (
						<div className={styles.value}>{authUser?.email || ''}</div>
					)}
				</div>

				<div className={styles.propertyValueContainer}>
					<div className={styles.property}>Адрес</div>
					<div className={styles.value}>
						{!isLoading ? (
							<Skeleton width={250} />
						) : (
							'Пискаревский проспект д.25 к.3 кв.371'
						)}
					</div>
				</div>
			</div>
			<Button className={styles.btn} onClick={onSwapMode}>
				Редактировать
			</Button>
		</>
	)
}
