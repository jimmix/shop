import cl from 'classnames'
import Image from 'next/image'
import React, { MutableRefObject, useEffect, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import Skeleton from 'react-loading-skeleton'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'

import { getIsUpdatedUser } from '@/entities/User/model/selectors/getIsUpdatedUser/getIsUpdatedUser'
import { getUserAuthData } from '@/entities/User/model/selectors/getUserAuthData/getUserAuthData'
import { updateUserData } from '@/entities/User/model/services/updateUserData'
import { userActions } from '@/entities/User/model/slice/userSlice'

import { Input } from '@/shared/ui/Input/Input'
import { InputSubmit } from '@/shared/ui/InputSubmit/InputSubmit'

import styles from '../SettingContent.module.scss'

import editStyles from './Edit.module.scss'
import { EditProps } from './Edit.props'
import { config } from '@/shared/constaints/Config'

const resolveTypeImage = [
	'image/svg+xml',
	'image/svg',
	'image/jpeg',
	'image/png'
]

const defaultImagePath = '/images/add-photo.png'

export const Edit = ({ onSwapMode, ...props }: EditProps): JSX.Element => {
	const authUser = useSelector(getUserAuthData)
	const isUpdatedUser = useSelector(getIsUpdatedUser)
	const ref = useRef<HTMLInputElement>() as MutableRefObject<HTMLInputElement>
	const [phone, setPhone] = useState<string>(authUser?.phone || '')
	const [address, setAddress] = useState<string>(authUser?.address || '')
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm()
	const isLoading = authUser?.id
	const dispatch = useDispatch()

	const [file, setFile] = useState<string>(
		authUser?.imagePath
			? `${config.backendStoragePath}/${authUser.imagePath}`
			: defaultImagePath
	)

	useEffect(() => {
		if (isUpdatedUser) {
			onSwapMode()
			dispatch(userActions.setIsUpdatedUser(false))
		}
	}, [isUpdatedUser])

	const onSubmit = () => {
		if (!Object.keys(errors).length) {
			const files = ref.current.files
			const image = files?.length ? files[0] : null

			// @ts-ignore
			dispatch(updateUserData({ phone, address, image }))
		}
	}

	const onChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (!e.target.files?.length) {
			setFile(defaultImagePath)
			return
		}
		const typeFile = e.target.files[0].type
		if (!resolveTypeImage.includes(typeFile)) {
			setFile(defaultImagePath)
			toast.warning('Разрешение файла должно быть svg/png/jpg/jpeg')
			return
		}

		setFile(URL.createObjectURL(e.target.files[0]))
	}

	return (
		<>
			<div className={styles.userContainer}>
				<div className={styles.imageContainer}>
					<label htmlFor={'myfile'} className={editStyles.fileLabel}>
						<Image
							src={file}
							alt={'изображение пользователя'}
							width={140}
							height={140}
							className={styles.image}
						/>
					</label>
					<input
						id='myfile'
						type='file'
						className={editStyles.fileInput}
						onChange={onChangeFile}
						ref={ref}
					/>
				</div>

				<div>
					{!isLoading ? (
						<Skeleton width={250} />
					) : (
						<div className={styles.name}>{authUser?.name}</div>
					)}
					{!isLoading ? (
						<Skeleton width={150} />
					) : (
						<div className={styles.city}>Новосибирск</div>
					)}
				</div>
			</div>
			<form className={styles.formContainer} onSubmit={handleSubmit(onSubmit)}>
				<div
					className={cl(
						styles.propertyValueContainer,
						editStyles.propertyValueContainer
					)}
				>
					<div className={styles.property}>Контактный телефон</div>
					<Input
						className={styles.value}
						value={phone}
						label='phone'
						register={register}
						onChangeValue={phone => {
							setPhone(phone)
						}}
						options={{
							minLength: {
								value: 4,
								message: 'Минимальная длина символов 4'
							},
							pattern: {
								value: /^(8|\+7|7)*\d{10}$/,
								message: 'Номер телефона является невалидным'
							}
						}}
						error={errors?.phone?.message}
					/>
				</div>

				<div
					className={cl(
						styles.propertyValueContainer,
						editStyles.propertyValueContainer
					)}
				>
					<div className={styles.property}>E-mail</div>
					{!isLoading ? (
						<Skeleton width={250} />
					) : (
						<div className={styles.value}>{authUser?.email || ''}</div>
					)}
				</div>

				<div
					className={cl(
						styles.propertyValueContainer,
						editStyles.propertyValueContainer
					)}
				>
					<div className={styles.property}>Адрес</div>
					{!isLoading ? (
						<Skeleton className={styles.value} width={250} />
					) : (
						<Input
							className={styles.value}
							onChangeValue={address => {
								setAddress(address)
							}}
							label='address'
							value={address}
							register={register}
							options={{
								minLength: {
									value: 10,
									message: 'Минимальная длина символов 10'
								},
								maxLength: {
									value: 1000,
									message: 'Максимальная длина символов 1000'
								}
							}}
							error={errors?.address?.message}
						/>
					)}
				</div>
				<InputSubmit
					type={'submit'}
					className={cl(editStyles.btn, styles.btn)}
					value={'Сохранить'}
				/>
			</form>
		</>
	)
}
