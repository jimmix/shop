import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface EditProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
        onSwapMode: () => void
    }
