import cl from 'classnames'

import styles from './Content.module.scss'
import { ContentProps } from './Content.props'
import { SettingContent } from './SettingContent/SettingContent'

export const Content = ({ children, ...props }: ContentProps): JSX.Element => {
	return (
		<div {...props} className={cl(styles.container)}>
			{children}
		</div>
	)
}
