import { Product } from '@/widgets/Core/Product/Product'
import { ProductListProps } from '@/widgets/ProductList/ProductList.props'

export const ProductList = ({ products, className }: ProductListProps): JSX.Element => {
	return (
		<div className={className}>
			{products.map(product => (
				<Product
					id={product.id}
					name={product.name}
					author={product.provider}
					basePrice={product.basePrice}
					quantity={product.quantity}
					availableDays={product.availableDays}
					weight={product.weight}
					image={product.image}
					discount={product.discount}
					discountDate={product.discountDate}
					key={product.id}
				/>
			))}
		</div>
	)
}
