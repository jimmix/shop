import { createContext } from 'react'

import { answerList } from '@/entities/Main/QuestionsAndAnswers/AnswerList/AnswerList'
import { IAnswer } from '@/entities/Main/QuestionsAndAnswers/AnswerList/AnswerList.interface'

export interface IAnswerContext {
	answer: IAnswer
	index: number
	setIndex: (idx: number) => void
	maxLength: number
}

const context: IAnswerContext = {
	answer: answerList.items[0],
	index: 0,
	setIndex: () => {},
	maxLength: answerList.items.length
}

export const AnswerContext = createContext<IAnswerContext>(context)
