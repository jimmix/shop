export interface IAnswer {
	imagePath: string
	question: string
	answer: string
}

export interface IAnswerList {
	items: IAnswer[]
}
