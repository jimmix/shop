import { createContext } from 'react'

import { FeedbackItem } from '@/widgets/About/Feedback/FeedbackList/feedbackList'

export interface IFeedbackContext {
	feedbackList: FeedbackItem[]
	tick: number
	setTick: (tick: number) => void
	slide: number
	setSlide: (slide: number) => void
}

const context: IFeedbackContext = {
	feedbackList: [],
	tick: 0,
	setTick: (tick: number) => {},
	slide: 0,
	setSlide: (slide: number) => {}
}

export const FeedbackContext = createContext<IFeedbackContext>(context)
