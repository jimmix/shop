import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

import { config } from '@/shared/constaints/Config'

import { categoryActions } from '../../slice/categorySlice'
import { Category } from '../../types/Category'

export const getCategories = createAsyncThunk<
	Category,
	undefined,
	{ rejectValue: string }
>('category/all', async (_, thunkAPI) => {
	try {
		const response = await axios.get<Category[]>(`${config.api}/category`)

		if (!response.data) {
			throw new Error('Ошибка авторизации. Пустое тело ответа')
		}

		thunkAPI.dispatch(categoryActions.setCategories(response.data))
		return response.config.data
	} catch (error) {
		const msg =
			// @ts-ignore
			error?.response?.data?.message ||
			'Непредвиденная ошибка при получении категорий товара'
		return thunkAPI.rejectWithValue(msg)
	}
})
