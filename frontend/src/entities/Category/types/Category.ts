export interface Category {
	id: string
	name: string
	littleImage: string | null
	bigImage: string
	parent?: Category
	children: Category[]
}

export interface CategorySchema {
	categories: Category[]
	isLoaded?: boolean
	isLoading: boolean
	error: string | null
}
