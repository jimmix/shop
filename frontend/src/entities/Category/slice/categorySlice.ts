import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { getCategories } from '@/entities/Category/services/getCategories/getCategories'

import { Category, CategorySchema } from '../types/Category'

const initialState: CategorySchema = {
	categories: [],
	isLoaded: false,
	isLoading: false,
	error: null
}

export const categorySlice = createSlice({
	name: 'category',
	initialState,
	reducers: {
		setCategories: (state, action: PayloadAction<Category[]>) => {
			state.categories = action.payload
		},
		setIsLoaded: (state, action: PayloadAction<boolean>) => {
			state.isLoaded = action.payload
		}
	},
	extraReducers: builder => {
		builder
			.addCase(getCategories.pending, (state, action) => {
				state.error = null
				state.isLoaded = false
				state.isLoading = true
			})
			.addCase(getCategories.fulfilled, (state, action) => {
				state.error = null
				state.isLoaded = true
				state.isLoading = false
			})
			.addCase(getCategories.rejected, (state, action) => {
				state.error = action.payload || null
				state.isLoaded = true
				state.isLoading = false
			})
	}
})

export const { actions: categoryActions } = categorySlice
export const { reducer: categoryReducers } = categorySlice
