import { createSelector } from 'reselect'

import { getCategoryState } from '@/entities/Category/selectors/getCategoryState/getCategoryState'

export const getCategoriesState = createSelector(
	getCategoryState,
	c => c.categories
)
