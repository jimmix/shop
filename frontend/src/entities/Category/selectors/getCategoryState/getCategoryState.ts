import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'

export const getCategoryState = (state: StateSchema) => state.category
