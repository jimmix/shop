import { createSelector } from 'reselect'

import { getCartState } from '@/entities/Cart/selectors/getCartState/getCartState'

export const getCartProductState = createSelector(getCartState, c => c.products)
