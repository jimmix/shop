import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'

export const getCartState = (state: StateSchema) => state.cart
