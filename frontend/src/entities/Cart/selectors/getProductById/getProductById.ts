import { createSelector } from 'reselect'

import { getCartProductState } from '@/entities/Cart/selectors/getCartProductState/getCartProductState'

export const getProductById = (id: string) =>
	createSelector(getCartProductState, p =>
		p.find(item => item.product.id === id)
	)
