export interface ProductItem {
	id: string
	price: number
	weight: string
	imagePath?: string
	name: string
}

export interface CartItem {
	product: ProductItem
	count: number
}

export interface CartSchema {
	products: CartItem[]
}
