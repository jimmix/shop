import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { CartItem, CartSchema, ProductItem } from '@/entities/Cart/types/Cart'

import { CART_LOCALSTORAGE_ITEMS } from '@/shared/const/localstorage'

const getCartItemsFromLocalstorage: () => CartItem[] = () => {
	if (typeof localStorage === 'undefined') {
		return []
	}

	const cart = localStorage.getItem(CART_LOCALSTORAGE_ITEMS)
	return cart ? JSON.parse(cart) : []
}

const initialState: CartSchema = {
	products: getCartItemsFromLocalstorage()
}

const updateLocalStorage: (items: CartItem[]) => void = items => {
	localStorage.set(CART_LOCALSTORAGE_ITEMS, JSON.stringify(items))
}

export const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		addProduct: (state, action: PayloadAction<ProductItem>) => {
			const productIdx = state.products.findIndex(
				p => p.product.id === action.payload.id
			)
			if (-1 !== productIdx) {
				state.products[productIdx].count += 1
				localStorage.setItem(
					CART_LOCALSTORAGE_ITEMS,
					JSON.stringify(state.products)
				)
				return
			}

			const products: CartItem = {
				product: action.payload,
				count: 1
			}

			state.products.push(products)
			localStorage.setItem(
				CART_LOCALSTORAGE_ITEMS,
				JSON.stringify(state.products)
			)
		},
		removeProduct: (state, action: PayloadAction<string>) => {
			state.products = state.products.filter(p => p.product.id !== action.payload)
			localStorage.setItem(
				CART_LOCALSTORAGE_ITEMS,
				JSON.stringify(state.products)
			)
		},
		decrementProduct: (state, action: PayloadAction<ProductItem>) => {
			const productsIdx = state.products.findIndex(
				p => p.product.id === action.payload.id
			)

			if (-1 === productsIdx || !state.products[productsIdx]?.count) {
				return
			}

			if (state.products[productsIdx].count === 1) {
				state.products = state.products.filter(p => p.product.id !== action.payload.id)
				localStorage.setItem(
					CART_LOCALSTORAGE_ITEMS,
					JSON.stringify(state.products)
				)
				return;
			}

			state.products[productsIdx].count -= 1
			localStorage.setItem(
				CART_LOCALSTORAGE_ITEMS,
				JSON.stringify(state.products)
			)
		}
	},
	extraReducers: builder => {}
})

export const { actions: cartActions } = cartSlice
export const { reducer: cartReducers } = cartSlice
