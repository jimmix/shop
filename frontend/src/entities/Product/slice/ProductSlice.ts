import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { Product, ProductSchema } from '../types/product'

const initialState: ProductSchema = {
	newProducts: {
		items: [],
		isLoaded: false,
		isLoading: false,
		error: null
	},
	hotProducts: {
		items: [],
		isLoaded: false,
		isLoading: false,
		error: null
	},
	hitProducts: {
		items: [],
		isLoaded: false,
		isLoading: false,
		error: null
	},
	products: {
		items: [],
		isLoaded: false,
		isLoading: false,
		error: null
	},
	isLoading: false,
	error: null
}

export const productSlice = createSlice({
	name: 'product',
	initialState,
	reducers: {
		setNewProducts: (state, action: PayloadAction<Product[]>) => {
			state.newProducts.items = action.payload
		},
		setHotProducts: (state, action: PayloadAction<Product[]>) => {
			state.hotProducts.items = action.payload
		},
		setHitProducts: (state, action: PayloadAction<Product[]>) => {
			state.hitProducts.items = action.payload
		},
		setProducts: (state, action: PayloadAction<Product[]>) => {
			state.products.items = action.payload
		}
	},
	extraReducers: builder => {}
})

export const { actions: productActions } = productSlice
export const { reducer: productReducers } = productSlice
