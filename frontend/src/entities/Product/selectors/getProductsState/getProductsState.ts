import { createSelector } from 'reselect'

import { getProductState } from '@/entities/Product/selectors/getProductState/getProductState'

export const getProductsState = createSelector(
	[getProductState],
	p => p.products
)
