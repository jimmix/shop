import { createSelector } from 'reselect'

import { getProductState } from '@/entities/Product/selectors/getProductState/getProductState'

export const getHotProductsState = createSelector(
	[getProductState],
	p => p.hotProducts
)
