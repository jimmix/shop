import { createSelector } from 'reselect'

import { getProductState } from '@/entities/Product/selectors/getProductState/getProductState'

export const getHitProductsState = createSelector(
	[getProductState],
	p => p.hitProducts
)
