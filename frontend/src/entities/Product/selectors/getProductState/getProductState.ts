import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'

export const getProductState = (state: StateSchema) => state.product
