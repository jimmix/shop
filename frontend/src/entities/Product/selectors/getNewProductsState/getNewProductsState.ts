import { createSelector } from 'reselect'

import { getProductState } from '@/entities/Product/selectors/getProductState/getProductState'

export const getNewProductsState = createSelector(
	[getProductState],
	p => p.newProducts
)
