import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

import { config } from '@/shared/constaints/Config'

import { productActions } from '../../slice/ProductSlice'
import { Product } from '../../types/product'

export interface IFilterParamsProduct {
	availableDays?: number
	order?: string
	sort?: 'ASC' | 'DESC'
	categoryId?: string
}

export interface IQueryParamsProduct {
	filters?: IFilterParamsProduct
	limit: number
	page: number
}

export const getProducts = createAsyncThunk<
	Product,
	IQueryParamsProduct,
	{ rejectValue: string }
>('products/filters', async (params, thunkAPI) => {
	try {
		const response = await axios.get<Product[]>(`${config.api}/products`, {
			params
		})

		if (!response.data) {
			throw new Error('Ошибка при получении товаров с сервера')
		}

		thunkAPI.dispatch(productActions.setProducts(response.data))
		return response.config.data
	} catch (error) {
		const msg =
			// @ts-ignore
			error?.response?.data?.message ||
			'Непредвиденная ошибка при получении категорий товара'
		return thunkAPI.rejectWithValue(msg)
	}
})
