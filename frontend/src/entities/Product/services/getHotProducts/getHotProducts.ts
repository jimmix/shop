import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

import { config } from '@/shared/constaints/Config'

import { productActions } from '../../slice/ProductSlice'
import { Product } from '../../types/product'

export const getHotProducts = createAsyncThunk<
	Product,
	undefined,
	{ rejectValue: string }
>('products/hot', async (_, thunkAPI) => {
	try {
		const response = await axios.get<Product[]>(`${config.api}/products`, {
			params: {
				filters: {
					order: 'discount',
					sort: 'DESC',
					availableDays: null
				}
			}
		})

		if (!response.data) {
			throw new Error('Ошибка при получении новых товаров с сервера')
		}

		thunkAPI.dispatch(productActions.setHotProducts(response.data))
		return response.config.data
	} catch (error) {

		const msg =
			// @ts-ignore
			error?.response?.data?.message ||
			'Непредвиденная ошибка при получении категорий товара'
		return thunkAPI.rejectWithValue(msg)
	}
})
