export interface ImageInterface {
	id: string
	path: string
}
export interface Product {
	id: string
	createdAt: string
	updatedAt: string
	name: string
	description: string
	basePrice: number
	discount: number
	discountDate: string
	quantity: number
	weight: number
	availableDays: number
	image: ImageInterface
	provider?: string
}

export interface ProductsItems {
	items: Product[]
	isLoaded: boolean
	isLoading: boolean
	error: string | null
}

export interface Category {
	id: string
	name: string
	littleImage?: string
	bigImage: string
	parent?: Category
	children?: Category[]
}

export interface ProductSchema {
	newProducts: ProductsItems
	hotProducts: ProductsItems
	hitProducts: ProductsItems
	products: ProductsItems
	isLoaded?: boolean
	isLoading: boolean
	error: string | null
}
