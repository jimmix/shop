import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { toast } from 'react-toastify'

import { User } from '@/entities/User/model/types/User'

import { USER_LOCALSTORAGE_KEY_TOKEN } from '@/shared/const/localstorage'
import { config } from '@/shared/constaints/Config'
import authAxios from '@/shared/lib/authAxios/AuthAxios'

import { userActions } from '../slice/userSlice'

interface updateUserDataProps {
	address?: string
	phone?: string
	image?: File
}
interface IToken {
	token: string
}

export const updateUserData = createAsyncThunk<
	User,
	updateUserDataProps,
	{ rejectValue: string }
>('user/updateUserData', async ({ address, phone, image }, thunkAPI) => {
	try {
		const token: string | null = localStorage.getItem(
			USER_LOCALSTORAGE_KEY_TOKEN
		)
		if (!token) {
			throw new Error()
		}

		const response = await authAxios.put<IToken>(`${config.api}/users`, {
			address,
			phoneNumber: phone,
			image
		})

		if (response.status !== 200) {
			throw new Error()
		}

		if (!response.data) {
			throw new Error('Ошибка авторизации. Пустое тело ответа')
		}
		localStorage.setItem(USER_LOCALSTORAGE_KEY_TOKEN, JSON.stringify(response.data.token))

		const authUser = jwtDecode<User>(response.data.token)
		thunkAPI.dispatch(userActions.setAuthData(authUser))

		toast.success('Данные успешно обновлены')
		thunkAPI.dispatch(userActions.setIsUpdatedUser(true))
		return response.config.data
	} catch (error) {
		const msg =
			// @ts-ignore
			error?.response?.data?.message ||
			'Ошибка обновления данных пользователя. Проверьте отправленные данные.'
		toast.error(msg)

		return thunkAPI.rejectWithValue(msg)
	}
})
