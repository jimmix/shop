import { createSelector } from 'reselect'

import { getUserState } from '@/entities/User/model/selectors/GetUserState/getUserState'

export const getUserAuthData = createSelector([getUserState], u => u?.authData)
