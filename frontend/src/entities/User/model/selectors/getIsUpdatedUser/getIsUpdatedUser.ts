import { createSelector } from 'reselect'

import { getUserState } from '@/entities/User/model/selectors/GetUserState/getUserState'

export const getIsUpdatedUser = createSelector(
	[getUserState],
	u => u?.isUpdatedUser
)
