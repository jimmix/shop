export interface User {
	id: string
	phone?: string
	email?: string
	roles: string[]
	imagePath: string
	name: string
	address?: string
}

export interface UserSchema {
	authData?: User
	isUpdatedUser?: boolean
}
