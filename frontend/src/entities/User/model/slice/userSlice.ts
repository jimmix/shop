import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import jwtDecode, { JwtPayload } from 'jwt-decode'

import { User, UserSchema } from '@/entities/User/model/types/User'

import { USER_LOCALSTORAGE_KEY_TOKEN } from '@/shared/const/localstorage'

const initialState: UserSchema = {}

export const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		setAuthData: (state, action: PayloadAction<User>) => {
			state.authData = action.payload
		},
		initAuthData: state => {
			const token: string | null = localStorage.getItem(
				USER_LOCALSTORAGE_KEY_TOKEN
			)

			if (token) {
				state.authData = jwtDecode<User>(token)
			}
		},
		setIsUpdatedUser: (state, action: PayloadAction<boolean>) => {
			state.isUpdatedUser = action.payload
		},
		logout: state => {
			state.authData = undefined
			localStorage.removeItem(USER_LOCALSTORAGE_KEY_TOKEN)
		}
	}
})

export const { actions: userActions } = userSlice
export const { reducer: userReducers } = userSlice
