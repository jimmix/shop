export interface IFooterList {
	title: string
	items: IFooterItem[]
}

export interface IFooterItem {
	title: string
	href: string
	scroll?: boolean
}

export const footerList: IFooterList[] = [
	{
		title: 'Помощь',
		items: [
			{
				title: 'Главная',
				href: '/'
			},
			{
				title: 'Каталог',
				href: '/catalog'
			},
			{
				title: 'Рецепты',
				href: '/receipt'
			},
			{
				title: 'Доставка',
				href: '/delivery'
			}
		]
	},
	{
		title: 'Фер.Мир',
		items: [
			{
				title: 'О нас',
				href: '/about_us'
			},
			{
				title: 'Вопросы и ответы',
				href: '/#questions',
				scroll: false
			},
			{
				title: 'Контакты',
				href: '/contacts'
			}
		]
	},
	{
		title: 'Поставщики',
		items: [
			{
				title: 'Стать поставщиком',
				href: '/postavshik'
			},
			{
				title: 'О поставщиках',
				href: '/about_postavshik'
			},
			{
				title: 'Отзывы',
				href: '/about_us#feedback'
			}
		]
	},
	{
		title: 'Личный кабинет',
		items: [
			{
				title: 'Избранное',
				href: '/love'
			},
			{
				title: 'Заказы',
				href: '/opportunity'
			},
			{
				title: 'Настройки',
				href: '/cabinet/setting'
			}
		]
	}
]
