import getConfig from 'next/config'

export interface IConfig {
	backendStoragePath: string
	api: string
}

const { publicRuntimeConfig } = getConfig()

export const config: IConfig = {
	backendStoragePath: publicRuntimeConfig.BACKEND_STORAGE_PATH as string,
	api: publicRuntimeConfig.APP_URL as string
}
