import axios from 'axios'

import {
	USER_LOCALSTORAGE_KEY_TOKEN,
	USER_LOCALSTORAGE_REFRESH_TOKEN
} from '@/shared/const/localstorage'
import { config } from '@/shared/constaints/Config'

const authAxios = axios.create({
	baseURL: config.api,
	withCredentials: true
})

authAxios.interceptors.request.use(config => {
	config.headers.Authorization = `Bearer ${localStorage.getItem(
		USER_LOCALSTORAGE_KEY_TOKEN
	)}`
	return config
})

authAxios.interceptors.response.use(
	config => {
		return config
	},
	async error => {
		const originalRequest = error.config
		if (error.response.status == 401 && originalRequest.config?._isRetry) {
			originalRequest._isRetry = true
			try {
				const response = await axios.get(`${config.api}/auth/refresh`, {
					withCredentials: true
				})
				localStorage.setItem(
					USER_LOCALSTORAGE_REFRESH_TOKEN,
					response.data.accessToken
				)

				return authAxios.request(originalRequest)
			} catch (error) {}
		}

		throw error
	}
)

export default authAxios
