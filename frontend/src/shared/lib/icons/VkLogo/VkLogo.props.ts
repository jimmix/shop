import { SVGProps } from 'react'

export interface VkLogoProps extends SVGProps<SVGSVGElement> {}
