import { RefObject, useEffect } from 'react'

export function useOnClickOutside(
	ref: RefObject<HTMLDivElement>,
	parentRef: RefObject<HTMLDivElement> | null | undefined,
	handler: any
) {
	useEffect(() => {
		const listener = (event: Event) => {
			// Do nothing if clicking ref's element or descendent elements
			if (
				!ref.current ||
				ref.current.contains(event.target as Node) ||
				parentRef?.current?.contains(event.target as Node)
			) {
				return
			}
			handler(event)
		}
		document.addEventListener('mousedown', listener)
		document.addEventListener('touchstart', listener)
		return () => {
			document.removeEventListener('mousedown', listener)
			document.removeEventListener('touchstart', listener)
		}
	}, [ref, handler])
}
