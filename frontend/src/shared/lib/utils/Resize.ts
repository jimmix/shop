import { DeviceType } from '@/shared/lib/types/DeviceType'

export function getCurrentDeviceType(): DeviceType {
	const width = window?.innerWidth
	if (!width) {
		return 'Desktop'
	}

	switch (true) {
		case width <= 997 && width > 597:
			return 'Table'
		case width <= 597:
			return 'Mobile'
		default:
			return 'Desktop'
	}
}
