import cl from 'classnames'

import { HtagProps } from '@/shared/ui/HTag/Htag.props'

import styles from './HTag.module.scss'

export const HTag = ({
	tag,
	children,
	className,
	...props
}: HtagProps): JSX.Element => {
	switch (tag) {
		case 'h1':
			return (
				<div className={cl(styles.h1, className)} {...props}>
					{children}
				</div>
			)
		case 'h2':
			return (
				<div className={cl(styles.h2, className)} {...props}>
					{children}
				</div>
			)
		case 'h3':
			return (
				<div className={cl(styles.h3, className)} {...props}>
					{children}
				</div>
			)
		default:
			return <></>
	}
}
