import cn from 'classnames'

import { ButtonProps } from '@/shared/ui/Button/Button.props'

import styles from './Button.module.scss'

export const Button = ({
	children,
	appearance = 'primary',
	radius = 'sm',
	className,
	innerRef = undefined,
	disabled = undefined,
	...props
}: ButtonProps): JSX.Element => {
	return (
		<button
			disabled={disabled ? true : false}
			className={cn(styles.button, className, {
				[styles.primary]: appearance === 'primary',
				[styles.ghost]: appearance === 'ghost',
				[styles.black]: appearance === 'black',
				[styles.red]: appearance === 'red',
				[styles.borderMd]: radius === 'md',
				[styles.borderLg]: radius === 'lg',
				[styles.borderXl]: radius === 'xl',
				[styles.disabled]: disabled
			})}
			{...props}
			ref={innerRef}
		>
			{children}
		</button>
	)
}
