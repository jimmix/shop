import {
	ButtonHTMLAttributes,
	DetailedHTMLProps,
	MutableRefObject,
	ReactNode
} from 'react'

export interface ButtonProps
	extends DetailedHTMLProps<
		ButtonHTMLAttributes<HTMLButtonElement>,
		HTMLButtonElement
	> {
	children: ReactNode
	appearance?: 'primary' | 'ghost' | 'red' | 'black'
	radius?: 'sm' | 'md' | 'lg' | 'xl'
	innerRef?: MutableRefObject<HTMLButtonElement>
	disabled?: boolean
}
