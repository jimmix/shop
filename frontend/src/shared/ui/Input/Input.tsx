import cl from 'classnames'
import { memo } from 'react'

import styles from './Input.module.scss'
import { InputProps } from './Input.props'

export const Input = memo(
	({
		className,
		onChange,
		onChangeValue,
		register,
		label,
		options,
		error,
		...props
	}: InputProps): JSX.Element => {
		const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
			const value: string = e.target.value as string
			onChangeValue?.(value)
		}

		return register ? (
			<div className={styles.container}>
				<input
					{...props}
					{...register(label, options ? options : {})}
					className={cl(className, styles.input, {
						[styles.error]: typeof error === 'string'
					})}
					onChange={onChangeHandler}
				/>
				{typeof error === 'string' ? (
					<span className={styles.messageError}>{error}</span>
				) : (
					<></>
				)}
			</div>
		) : (
			<>
				<input
					{...props}
					className={cl(styles.input, className)}
					onChange={onChangeHandler}
				/>
			</>
		)
	}
)
