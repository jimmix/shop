import { DetailedHTMLProps, InputHTMLAttributes } from 'react'
import {
	FieldError,
	FieldErrorsImpl,
	Merge,
	Path,
	UseFormRegister
} from 'react-hook-form'

export interface InputProps
	extends DetailedHTMLProps<
		InputHTMLAttributes<HTMLInputElement>,
		HTMLInputElement
	> {
	onChangeValue?: (value: string) => void
	label?: string
	register?: any
	options?: any
	error?:
		| string
		| FieldError
		| Merge<FieldError, FieldErrorsImpl<any>>
		| undefined
}
