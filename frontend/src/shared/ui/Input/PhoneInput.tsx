// import React, { MutableRefObject, memo, useEffect, useRef } from 'react'

// import { Input } from './Input'
// import { InputProps } from './Input.props'

// export const PhoneInput = memo(
// 	({
// 		className,
// 		onChange,
// 		onChangeValue,
// 		...props
// 	}: InputProps): JSX.Element => {
// 		const formatPhoneNumber = (value: string) => {
// 			value = value.replace(/\D/g, '')
// 			return value.replace(
// 				/^(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})$/,
// 				'+7 $2 $3 $4 $5'
// 			)
// 		}
// 		const ref = useRef<HTMLInputElement>() as MutableRefObject<HTMLInputElement>

// 		useEffect(() => {
// 			const handlerListener = (
// 				event: React.KeyboardEvent<HTMLInputElement>
// 			) => {
// 				const value = event.currentTarget.value
// 				const lastChar = value.slice(-1)
// 				const isEmpty = '' === lastChar
// 				const isDigit = /\d/.test(lastChar)
// 			}
// 			// @ts-ignore
// 			ref.current.addEventListener('keydown', handlerListener)

// 			return () => {
// 				// @ts-ignore
// 				ref.current.removeEventListener('keydown', handlerListener)
// 			}
// 		}, [])
// 		return (
// 			// @ts-ignore
// 			<Input
// 				ref={ref}
// 				{...props}
// 				onChangeValue={onChangeValue}
// 				onChange={onChange}
// 			/>
// 		)
// 	}
// )
