import {
	DetailedHTMLProps,
	InputHTMLAttributes,
	MutableRefObject,
	ReactNode
} from 'react'

export interface InputSubmitProps
	extends DetailedHTMLProps<
		InputHTMLAttributes<HTMLInputElement>,
		HTMLInputElement
	> {
	appearance?: 'primary' | 'ghost' | 'red' | 'black'
	radius?: 'sm' | 'md' | 'lg' | 'xl'
	innerRef?: MutableRefObject<HTMLInputElement>
	disabled?: boolean
}
