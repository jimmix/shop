import cn from 'classnames'

import { InputSubmitProps } from '@/shared/ui/InputSubmit/InputSubmit.props'

import styles from './InputSubmit.module.scss'

export const InputSubmit = ({
	appearance = 'primary',
	radius = 'sm',
	className,
	innerRef = undefined,
	disabled = undefined,
	...props
}: InputSubmitProps): JSX.Element => {
	return (
		<input
			disabled={disabled ? true : false}
			className={cn(styles.button, className, {
				[styles.primary]: appearance === 'primary',
				[styles.ghost]: appearance === 'ghost',
				[styles.black]: appearance === 'black',
				[styles.red]: appearance === 'red',
				[styles.borderMd]: radius === 'md',
				[styles.borderLg]: radius === 'lg',
				[styles.borderXl]: radius === 'xl',
				[styles.disabled]: disabled
			})}
			{...props}
			type='submit'
			ref={innerRef}
		/>
	)
}
