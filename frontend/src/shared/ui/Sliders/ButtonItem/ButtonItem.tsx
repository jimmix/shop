import cl from 'classnames'

import { Button } from '@/shared/ui/Button/Button'
import { ButtonItemProps } from '@/shared/ui/Sliders/ButtonItem/ButtonItem.props'

import styles from './ButtomItem.module.scss'

export const ButtonItem = ({
	arrow,
	className,
	innerRef,
	isDisabled = false,
	...props
}: ButtonItemProps): JSX.Element => {
	return (
		<Button
			appearance={isDisabled ? 'ghost' : 'primary'}
			className={cl(
				styles.btn,
				styles[`btn-${arrow}`],
				{
					[styles.disabled]: isDisabled
				},
				className
			)}
			{...props}
			innerRef={innerRef}
		>
			<div className={styles[`arrow-${arrow}`]}></div>
		</Button>
	)
}
