import { DetailedHTMLProps, HTMLAttributes, MutableRefObject } from 'react'

export interface ButtonItemProps
	extends DetailedHTMLProps<
		HTMLAttributes<HTMLButtonElement>,
		HTMLButtonElement
	> {
	arrow: 'left' | 'right'
	innerRef: MutableRefObject<HTMLButtonElement>
	isDisabled?: boolean
}
