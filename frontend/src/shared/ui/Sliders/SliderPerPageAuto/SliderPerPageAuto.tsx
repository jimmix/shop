import cl from 'classnames'
import { Navigation } from 'swiper'
import 'swiper/css'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Swiper as SwiperClass } from 'swiper/types'

import { default_breakpoints } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto.constants'
import { SliderPerPageAutoProps } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto.props'

import styles from './SliderPerPageAuto.module.scss'

export const SliderPerPageAuto = ({
	className,
	items,
	breakpoints = default_breakpoints,
	spaceBetween = 20,
	slidesPerView = 'auto',
	nextRef,
	prevRef,
	setIsStart,
	setIsEnd
}: SliderPerPageAutoProps): JSX.Element => {
	const onBeforeInitHandler = (swiper: SwiperClass) => {
		if (!nextRef || !prevRef) {
			return
		}

		/** @ts-ignore */
		swiper.params.navigation.nextEl = nextRef.current
		/** @ts-ignore */
		swiper.params.navigation.prevEl = prevRef.current
	}

	const navigationHandler = () =>
		nextRef && prevRef
			? {
					nextEl: nextRef.current,
					prevEl: prevRef.current
			  }
			: {}

	const onProgressHandler = (swiper: SwiperClass): void => {
		if (!setIsStart || !setIsEnd) {
			return
		}

		setIsStart(swiper.isBeginning)
		setIsEnd(swiper.isEnd)
	}

	return (
		<Swiper
			className={cl(styles.container, className, 'mySwiper')}
			slidesPerView={slidesPerView}
			spaceBetween={spaceBetween}
			breakpoints={breakpoints}
			modules={[Navigation]}
			navigation={navigationHandler()}
			onBeforeInit={onBeforeInitHandler}
			onProgress={onProgressHandler}
		>
			{items.map((item, idx) => (
				<SwiperSlide key={idx} className={styles.default}>
					{item}
				</SwiperSlide>
			))}
		</Swiper>
	)
}
