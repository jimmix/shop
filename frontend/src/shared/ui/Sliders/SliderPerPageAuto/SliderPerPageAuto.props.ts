import {
	DetailedHTMLProps,
	HTMLAttributes,
	MutableRefObject,
	ReactNode
} from 'react'
import { SwiperOptions } from 'swiper'

export interface IBreakpointsSwiper {
	[width: number]: SwiperOptions
	[ratio: string]: SwiperOptions
}
export interface SliderPerPageAutoProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	items: ReactNode[]
	breakpoints?: IBreakpointsSwiper
	spaceBetween?: number
	slidesPerView?: number | 'auto'
	prevRef?: MutableRefObject<HTMLButtonElement>
	nextRef?: MutableRefObject<HTMLButtonElement>
	setIsStart?: (start: boolean) => void
	setIsEnd?: (end: boolean) => void
}
