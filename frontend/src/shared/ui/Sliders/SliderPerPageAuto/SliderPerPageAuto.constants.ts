import { IBreakpointsSwiper } from '@/shared/ui/Sliders/SliderPerPageAuto/SliderPerPageAuto.props'

export const default_breakpoints: IBreakpointsSwiper = {
	997: {
		slidesPerView: 'auto',
		spaceBetween: 20
	},
	598: {
		slidesPerView: 'auto',
		spaceBetween: 16
	},
	300: {
		slidesPerView: 'auto',
		spaceBetween: 10
	}
}
