import cl from 'classnames'

import { ButtonListContainerProps } from '@/shared/ui/Sliders/ButtonListContainer/ButtonListContainer.props'

import styles from './ButtonListContainer.module.scss'

export const ButtonListContainer = ({
	children,
	className,
	...props
}: ButtonListContainerProps): JSX.Element => {
	return (
		<div className={cl(styles.btnList, className)} {...props}>
			{children}
		</div>
	)
}
