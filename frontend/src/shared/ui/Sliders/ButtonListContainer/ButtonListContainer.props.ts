import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface ButtonListContainerProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}
