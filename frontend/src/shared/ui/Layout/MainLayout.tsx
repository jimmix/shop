import cl from 'classnames'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { userActions } from '@/entities/User/model/slice/userSlice'

import { MainLayoutProps } from '@/shared/ui/Layout/MainLayout.props'

import styles from './MainLayout.module.scss'

export const MainLayout = ({
	children,
	className,
	...props
}: MainLayoutProps): JSX.Element => {
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(userActions.initAuthData())
	}, [dispatch])

	return (
		<main className={cl(styles.layout, className)} {...props}>
			{children}
		</main>
	)
}
