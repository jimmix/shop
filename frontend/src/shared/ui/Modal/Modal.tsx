import cl from 'classnames'
import React, { useCallback, useEffect } from 'react'

import styles from './Modal.module.scss'
import { ModalProps } from './Modal.props'

export const Modal = ({
	children,
	isOpen,
	onClose,
	className,
	contentClassName,
	containerClassName,
	...props
}: ModalProps): JSX.Element => {
	const mods: Record<string, boolean> = {
		[styles.opened]: isOpen || false
	}
	const closeHandler = () => {
		if (onClose) {
			onClose()
		}
	}
	useEffect(() => {
		document.body.style.overflow = isOpen ? 'hidden' : 'auto'

		return () => {
			document.body.style.overflow = isOpen ? 'hidden' : 'auto'
		}
	}, [isOpen]);
	const onContentClick = (e: React.MouseEvent) => {
		e.stopPropagation()
	}

	const onKeyDown = useCallback(
		(e: React.KeyboardEvent) => {
			if (e.key === 'Escape') {
				closeHandler()
			}
		},
		[closeHandler]
	)

	useEffect(() => {
		if (isOpen) {
			// @ts-ignore
			window.addEventListener('keydown', onKeyDown)
		}
		return () => {
			if (isOpen) {
				// @ts-ignore
				window.removeEventListener('keydown', onKeyDown)
			}
		}
	}, [isOpen])

	return (
		<div {...props} className={cl(styles.Modal, mods, className)}>
			<div className={styles.overlay} onClick={closeHandler}>
				<div className={cl(styles.container, containerClassName || '')}>
					<div
						className={cl(styles.content, contentClassName || '')}
						onClick={onContentClick}
					>
						{children}
					</div>
				</div>
			</div>
		</div>
	)
}
