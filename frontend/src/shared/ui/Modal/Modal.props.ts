import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface ModalProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	isOpen?: boolean
	onClose?: () => void
	contentClassName?: string
	containerClassName?: string
}
