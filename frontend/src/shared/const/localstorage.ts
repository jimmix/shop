export const USER_LOCALSTORAGE_KEY_TOKEN = 'user_access_token'
export const USER_LOCALSTORAGE_REFRESH_TOKEN = 'user_refresh_token'
export const CART_LOCALSTORAGE_ITEMS = 'cart_items'
