import { LoginSchema } from '@/features/AuthByEmail/model/types/LoginSchema'
import { CartModalSchema } from '@/features/CartModal/model/types/CartModalSchema'
import { RegisterSchema } from '@/features/RegisterByEmail/model/types/RegisterSchema'

import { CartSchema } from '@/entities/Cart/types/Cart'
import { CategorySchema } from '@/entities/Category/types/Category'
import { ProductSchema } from '@/entities/Product/types/product'
import { UserSchema } from '@/entities/User/model/types/User'

export interface StateSchema {
	user: UserSchema
	login: LoginSchema
	register: RegisterSchema
	category: CategorySchema
	product: ProductSchema
	cart: CartSchema
	cartModal: CartModalSchema
}
