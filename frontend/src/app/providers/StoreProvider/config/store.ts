import { ReducersMapObject, configureStore } from '@reduxjs/toolkit'

import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'

import { loginReduser } from '@/features/AuthByEmail/model/slice/LoginSlice'
import { cartModalReducers } from '@/features/CartModal/model/slice/CartModalSlice'
import { registerReducer } from '@/features/RegisterByEmail/model/slice/RegisterSlice'

import { cartReducers } from '@/entities/Cart/slice/cartSlice'
import { categoryReducers } from '@/entities/Category/slice/categorySlice'
import { productReducers } from '@/entities/Product/slice/ProductSlice'
import { userReducers } from '@/entities/User/model/slice/userSlice'

export function createReduxStore(initialState?: StateSchema) {
	const rootReducers: ReducersMapObject<StateSchema> = {
		user: userReducers,
		login: loginReduser,
		register: registerReducer,
		category: categoryReducers,
		product: productReducers,
		cart: cartReducers,
		cartModal: cartModalReducers
	}

	return configureStore<StateSchema>({
		reducer: rootReducers,
		devTools: true, //todo: prod/dev,
		preloadedState: initialState
	})
}
