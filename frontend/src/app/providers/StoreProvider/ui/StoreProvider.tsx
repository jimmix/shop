import { createWrapper } from 'next-redux-wrapper'
import { ReactNode } from 'react'
import { Provider } from 'react-redux'

import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'
import { createReduxStore } from '@/app/providers/StoreProvider/config/store'

interface StoreProviderProps {
	children?: ReactNode
	initialState?: StateSchema
}

export const StoreProvider = ({
	children,
	initialState
}: StoreProviderProps): JSX.Element => {
	const store = createReduxStore()

	return <Provider store={store}>{children}</Provider>
}

const store = () => createReduxStore()

export const wrapper = createWrapper(store, { debug: false })
