import { createReduxStore } from '@/app/providers/StoreProvider/config/store'
import { StoreProvider } from '@/app/providers/StoreProvider/ui/StoreProvider'

export { StoreProvider, createReduxStore }
