import { Cabinet } from '@/widgets/Cabinet/Cabinet'
import { Content } from '@/widgets/Cabinet/Content/Content'
import { SettingContent } from '@/widgets/Cabinet/Content/SettingContent/SettingContent'
import { PageLayout } from '@/widgets/ui/PageLayout/PageLayout'

export default function Setting() {
	return (
		<PageLayout>
			<Cabinet tab={'setting'}>
				<Content>
					<SettingContent />
				</Content>
			</Cabinet>
		</PageLayout>
	)
}
