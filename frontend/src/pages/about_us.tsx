import { Feedback } from '@/widgets/About/Feedback/Feedback'
import { HowWeWork } from '@/widgets/About/HowWeWork/HowWeWork'
import { OurSuppliers } from '@/widgets/About/OurSuppliers/OurSuppliers'
import { OurTeam } from '@/widgets/About/OurTeam/OurTeam'
import { ProductsDelivery } from '@/widgets/About/ProductsDelivery/ProductsDelivery'
import { Slogan } from '@/widgets/About/Slogan/Slogan'
import { QuestionsAndAnswers } from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers'
import { PageLayout } from '@/widgets/ui/PageLayout/PageLayout'

export default function Home() {
	return (
		<>
			<PageLayout>
				<Slogan />
				<HowWeWork />
				<ProductsDelivery />
				<OurSuppliers />
				<Feedback />
				<QuestionsAndAnswers />
				<OurTeam />
			</PageLayout>
		</>
	)
}


