import { ThunkDispatch } from '@reduxjs/toolkit'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { wrapper } from '@/app/providers/StoreProvider/ui/StoreProvider'

import { CatalogNavbar } from '@/widgets/Catalog/CatalogNavbar/CatalogNavbar'
import { ProductList } from '@/widgets/ProductList/ProductList'
import { PageLayout } from '@/widgets/ui/PageLayout/PageLayout'

import { getCategoriesState } from '@/entities/Category/selectors/getCategories/getCategoriesState'
import { getCategories } from '@/entities/Category/services/getCategories/getCategories'
import { categoryActions } from '@/entities/Category/slice/categorySlice'
import { Category } from '@/entities/Category/types/Category'
import { getProductsState } from '@/entities/Product/selectors/getProductsState/getProductsState'
import {
	IQueryParamsProduct,
	getProducts
} from '@/entities/Product/services/getProducts/getProducts'
import { productActions } from '@/entities/Product/slice/ProductSlice'
import { Product } from '@/entities/Product/types/product'

import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './index.module.scss'

export const getServerSideProps = wrapper.getServerSideProps(
	store => async context => {
		const queryProducts: IQueryParamsProduct = {
			page: 1,
			limit: 20,
			filters: {
				categoryId: context.query.id as string
			}
		}
		await Promise.all([
			store.dispatch(getCategories()),
			store.dispatch(getProducts(queryProducts))
		])
		const categories = store.getState().category.categories
		const products = store.getState().product.products.items
		return {
			props: {
				categories,
				products,
				cpage: 1,
				climit: 20
			}
		}
	}
)

export default function Catalog({
	categories,
	products,
	cpage,
	climit
}: {
	categories: Category[]
	products: Product[]
	cpage: number
	climit: number
}): JSX.Element {
	const dispatch = useDispatch<ThunkDispatch<any, any, any>>()
	const categoriesState = useSelector(getCategoriesState)
	const productsState = useSelector(getProductsState)
	const [page, setPage] = useState<number>(cpage)
	const [limit, setLimit] = useState<number>(climit)

	useEffect(() => {
		if (!categoriesState.length) {
			dispatch(categoryActions.setCategories(categories))
		}

		if (!productsState.items.length) {
			dispatch(productActions.setProducts(products))
		}
	}, [dispatch])

	return (
		<PageLayout>
			<HTag tag={'h1'} style={{ marginTop: 50 }}>
				Каталог
			</HTag>
			<section className={styles.container}>
				<CatalogNavbar catalogList={categories} />
				{products.length ? (
					<ProductList products={products} className={styles.products}/>
				) : (
					<HTag tag={'h1'}>Товаров не найдено</HTag>
				)}
			</section>
		</PageLayout>
	)
}
