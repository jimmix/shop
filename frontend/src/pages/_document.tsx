import { Head, Html, Main, NextScript } from 'next/document'

export default function Document() {
	return (
		<Html lang='ru'>
			<Head>
				<link rel='stylesheet' href='/css/_fonts.css' />
			</Head>
			<body>
				<Main />
				<NextScript />
			</body>
		</Html>
	)
}
