import type { AppProps } from 'next/app'
import 'react-loading-skeleton/dist/skeleton.css'
import { Provider } from 'react-redux'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { StoreProvider } from '@/app/providers/StoreProvider'
import { wrapper } from '@/app/providers/StoreProvider/ui/StoreProvider'
import '@/app/styles/globals.scss'

export default function App({ Component, pageProps, ...rest }: AppProps) {
	const { store, props } = wrapper.useWrappedStore(rest)
	return (
		<Provider store={store}>
			<Component {...pageProps} className={'test'} />
			<ToastContainer
				position='top-right'
				autoClose={3000}
				hideProgressBar
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
				theme='light'
				limit={5}
			/>
		</Provider>
	)
}
