import { ThunkDispatch } from '@reduxjs/toolkit'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { wrapper } from '@/app/providers/StoreProvider/ui/StoreProvider'

import { AboutUs } from '@/widgets/Main/AbouUs/AboutUs'
import { CatalogList } from '@/widgets/Main/CatalogList/CatalogList'
import { QuestionsAndAnswers } from '@/widgets/Main/QuestionsAndAnswers/QuestionsAndAnswers'
import { ServicesByDelivery } from '@/widgets/Main/ServicesByDelivery/ServicesByDelivery'
import { PromoProducts } from '@/widgets/PromoProducts/PromoProducts'
import { PageLayout } from '@/widgets/ui/PageLayout/PageLayout'

import { getCategoriesState } from '@/entities/Category/selectors/getCategories/getCategoriesState'
import { getCategories } from '@/entities/Category/services/getCategories/getCategories'
import { categoryActions } from '@/entities/Category/slice/categorySlice'
import { Category } from '@/entities/Category/types/Category'
import { getHotProductsState } from '@/entities/Product/selectors/getHotProductsState/getHotProductsState'
import { getNewProductsState } from '@/entities/Product/selectors/getNewProductsState/getNewProductsState'
import { getHotProducts } from '@/entities/Product/services/getHotProducts/getHotProducts'
import { getNewProducts } from '@/entities/Product/services/getNewProducts/getNewProducts'
import { productActions } from '@/entities/Product/slice/ProductSlice'
import { Product as ProductEntity } from '@/entities/Product/types/product'

export const getServerSideProps = wrapper.getServerSideProps(
	store => async context => {
		await Promise.all([
			store.dispatch(getCategories()),
			store.dispatch(getNewProducts()),
			store.dispatch(getHotProducts())
		])
		const newProducts = store.getState().product.newProducts.items
		const hotProducts = store.getState().product.hotProducts.items
		const categories = store.getState().category.categories
		return {
			props: {
				categories,
				newProducts,
				hotProducts
			}
		}
	}
)

export default function Home({
	categories,
	newProducts,
	hotProducts
}: {
	categories: Category[]
	newProducts: ProductEntity[]
	hotProducts: ProductEntity[]
}) {
	// @ts-ignore
	// const { isLoading, data } = useGetProductsQuery()
	// // @ts-ignore
	// const products: ProductEntity[] = data
	const dispatch = useDispatch<ThunkDispatch<any, any, any>>()
	const categoriesState = useSelector(getCategoriesState)
	const newProductsState = useSelector(getNewProductsState)
	const hotProductsState = useSelector(getHotProductsState)

	useEffect(() => {
		if (!categoriesState.length) {
			dispatch(categoryActions.setCategories(categories))
		}
		if (!newProductsState.items.length) {
			dispatch(productActions.setNewProducts(newProducts))
		}
		if (!hotProductsState.items.length) {
			dispatch(productActions.setHotProducts(hotProducts))
		}
	}, [dispatch])

	return (
		<>
			<PageLayout>
				<ServicesByDelivery />
				<PromoProducts products={hotProducts} title={'Горячее предложение'} />
				<CatalogList categories={categories} />
				<PromoProducts products={newProducts} title={'Новинки'} />
				<QuestionsAndAnswers />
				<AboutUs />
			</PageLayout>
		</>
	)
}
