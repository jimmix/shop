import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { toast } from 'react-toastify'

import { userActions } from '@/entities/User/model/slice/userSlice'
import { User } from '@/entities/User/model/types/User'

import { USER_LOCALSTORAGE_KEY_TOKEN } from '@/shared/const/localstorage'
import { config } from '@/shared/constaints/Config'

interface registerByEmailProps {
	email: string
	password: string
	confirmPassword: string
	code: string
	name: string
}

interface IToken {
	token: string
}

const ERROR_DEFAULT =
	'Непредвиденная ошибка сервера при регистрации, проверьте свои данные'
export const registerByEmail = createAsyncThunk<
	User,
	registerByEmailProps,
	{ rejectValue: string }
>(
	'registartion/email',
	async ({ email, password, confirmPassword, code, name }, thunkAPI) => {
		try {
			const response = await axios.post<IToken>(
				`${config.api}/auth/registration/email`,
				{
					email,
					password,
					confirmPassword,
					code,
					name
				}
			)
			if (!response.data) {
				throw new Error(ERROR_DEFAULT)
			}
			localStorage.setItem(
				USER_LOCALSTORAGE_KEY_TOKEN,
				JSON.stringify(response.data.token)
			)
			const authUser = jwtDecode<User>(response.data.token)
			thunkAPI.dispatch(userActions.setAuthData(authUser))

			return response.config.data
		} catch (error) {
			// @ts-ignore
			const msg = error?.response?.data?.message || ERROR_DEFAULT
			toast.error(msg)

			return thunkAPI.rejectWithValue(msg)
		}
	}
)
