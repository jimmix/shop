import cl from 'classnames'
import { useSelector } from 'react-redux'

import { getUserAuthData } from '@/entities/User/model/selectors/getUserAuthData/getUserAuthData'

import { Modal } from '@/shared/ui/Modal/Modal'

import { FerMirWrapper } from '../FerMirWrapper/FerMirWrapper'
import { LoginForm } from '../LoginForm/LoginForm'

import styles from './LoginModal.module.scss'
import { LoginModalProps } from './LoginModal.props'

export const LoginModal = ({
	isOpen,
	onClose,
	onSwapHandler,
	...props
}: LoginModalProps): JSX.Element => {
	const authUser = useSelector(getUserAuthData)
	if (authUser?.id && isOpen) {
		onClose()
	}

	return (
		<Modal
			{...props}
			isOpen={isOpen}
			onClose={onClose}
			contentClassName={styles.content}
		>
			<div className={cl(styles.container)}>
				<FerMirWrapper onSwapHandler={onSwapHandler} />
				<LoginForm onClose={onClose} />
			</div>
		</Modal>
	)
}
