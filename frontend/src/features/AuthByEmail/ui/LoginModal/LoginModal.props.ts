import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface LoginModalProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
		isOpen: boolean
		onClose: () => void
		onSwapHandler: () => void
	}
