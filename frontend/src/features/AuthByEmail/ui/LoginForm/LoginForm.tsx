import { memo, useCallback, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'

import { Input } from '@/shared/ui/Input/Input'
import { InputSubmit } from '@/shared/ui/InputSubmit/InputSubmit'

import { getLoginState } from '../../model/selectors/getLoginState/getLoginState'
import { loginByEmail } from '../../model/services/loginByEmail/loginByEmail'
import { loginActions } from '../../model/slice/LoginSlice'

import styles from './LoginForm.module.scss'
import { LoginFormProps } from './LoginForm.props'

export const LoginForm = memo(
	({ onClose, ...props }: LoginFormProps): JSX.Element => {
		const dispatch = useDispatch()
		const { email, password, error, isLoading, isLogged } =
			useSelector(getLoginState)

		const onChangeEmail = useCallback(
			(email: string) => {
				dispatch(loginActions.setEmail(email))
			},
			[dispatch]
		)
		const onChangePassword = useCallback(
			(password: string) => {
				dispatch(loginActions.setPassword(password))
			},
			[dispatch]
		)

		const onLoginClick = useCallback(() => {
			// @ts-ignore
			dispatch(loginByEmail({ email, password }))
		}, [dispatch, email, password])

		const {
			register,
			handleSubmit,
			formState: { errors }
		} = useForm()

		const onSubmit = () => {
			if (!Object.keys(errors).length) {
				onLoginClick()
			}
		}

		return (
			<div className={styles.container} {...props}>
				<div className={styles.containerContent}>
					<p className={styles.title}>Авторизация</p>
					<p className={styles.info}>
						Для входа необходимо ввести адрес электронной почты и пароль
					</p>
					<form onSubmit={handleSubmit(onSubmit)}>
						<div className={styles.inputContainer}>
							<Input
								type='text'
								className={styles.input}
								placeholder='Email'
								onChangeValue={onChangeEmail}
								value={email}
								label='email'
								register={register}
								options={{
									minLength: {
										value: 4,
										message: 'Минимальная длина символов 4'
									},
									required: 'Поле обязательно для заполнения',
									pattern: {
										value: /^[^@\s]+@[^@\s]+\.[^@\s]+$/,
										message: 'Адрес электронной почты является не валидным'
									}
								}}
								error={errors?.email?.message}
							/>
							<Input
								type='password'
								className={styles.input}
								placeholder='Пароль'
								onChangeValue={onChangePassword}
								value={password}
								label={'password'}
								register={register}
								options={{
									required: 'Поле обязательно для заполнения'
								}}
								error={errors?.password?.message}
							/>
						</div>
						<p className={styles.forgotPassword}>Забыли пароль?</p>

						<InputSubmit
							className={styles.btn}
							disabled={isLoading}
							type='submit'
							value={'Войти'}
						/>
					</form>
				</div>
			</div>
		)
	}
)
