import cl from 'classnames'

import { Button } from '@/shared/ui/Button/Button'

import styles from './FerMirWrapper.module.scss'
import { FerMirWrapperProps } from './FerMirWrapper.props'

export const FerMirWrapper = ({
	onSwapHandler,
	...props
}: FerMirWrapperProps): JSX.Element => {
	return (
		<div {...props} className={cl(styles.wrapper)}>
			<p className={styles.title}>Регистрация</p>
			<p className={styles.info}>
				Для создания аккаунта необходимо пройти процедуру регистрации
			</p>
			<Button onClick={onSwapHandler} className={styles.btn}>
				Зарегистрироваться
			</Button>
		</div>
	)
}
