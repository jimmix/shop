import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface FerMirWrapperProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
		onSwapHandler: () => void
	}
