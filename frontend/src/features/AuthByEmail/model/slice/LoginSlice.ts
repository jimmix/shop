import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { loginByEmail } from '../services/loginByEmail/loginByEmail'
import { LoginSchema } from '../types/LoginSchema'

const initialState: LoginSchema = {
	email: '',
	password: '',
	isLoading: false,
	isLogged: false,
	error: null
}

export const loginSlice = createSlice({
	name: 'loginEmail',
	initialState,
	reducers: {
		setEmail: (state, action: PayloadAction<string>) => {
			state.email = action.payload
		},
		setPassword: (state, action: PayloadAction<string>) => {
			state.password = action.payload
		},
		initState: state => {
			state.email = ''
			state.password = ''
			state.isLoading = false
			state.error = null
			state.isLogged = false
		},
		setIsLogged: (state, action: PayloadAction<boolean>) => {
			state.isLogged = action.payload
		}
	},
	extraReducers: builder => {
		builder
			.addCase(loginByEmail.pending, (state, action) => {
				state.error = null
				state.isLoading = true
			})
			.addCase(loginByEmail.fulfilled, (state, action) => {
				state.isLoading = false
			})
			.addCase(loginByEmail.rejected, (state, action) => {
				state.isLoading = false
				state.error = action.payload || null
			})
	}
})

export const { actions: loginActions } = loginSlice
export const { reducer: loginReduser } = loginSlice
