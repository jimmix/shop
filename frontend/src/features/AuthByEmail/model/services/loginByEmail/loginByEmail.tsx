import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { toast } from 'react-toastify'

import { userActions } from '@/entities/User/model/slice/userSlice'
import { User } from '@/entities/User/model/types/User'

import { USER_LOCALSTORAGE_KEY_TOKEN } from '@/shared/const/localstorage'
import { config } from '@/shared/constaints/Config'

import { loginActions } from '../../slice/LoginSlice'

interface loginByUserNameProps {
	email: string
	password: string
}

interface IToken {
	token: string
}

export const loginByEmail = createAsyncThunk<
	User,
	loginByUserNameProps,
	{ rejectValue: string }
>('login/loginByEmail', async ({ email, password }, thunkAPI) => {
	try {
		const response = await axios.post<IToken>(
			`${config.api}/auth/login/email`,
			{
				email: email,
				password
			}
		)
		if (!response.data) {
			throw new Error('Ошибка авторизации. Пустое тело ответа')
		}
		localStorage.setItem(
			USER_LOCALSTORAGE_KEY_TOKEN,
			JSON.stringify(response.data.token)
		)

		const authUser = jwtDecode<User>(response.data.token)
		thunkAPI.dispatch(loginActions.setIsLogged(true))
		thunkAPI.dispatch(userActions.setAuthData(authUser))
		thunkAPI.dispatch(loginActions.initState())

		toast.success('Авторизация прошла успешно')
		return response.config.data
	} catch (error) {
		// @ts-ignore
		const msg = error?.response?.data?.message || 'Ошибка авторизации'
		toast.error(msg)

		return thunkAPI.rejectWithValue(msg)
	}
})
