import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { toast } from 'react-toastify'

import { config } from '@/shared/constaints/Config'

interface sendCodeRequest {
	email: string
}

interface sendCodeResponse {
	status: boolean
}

export const sendCodeByEmail = createAsyncThunk<
	sendCodeResponse,
	sendCodeRequest,
	{ rejectValue: string }
>('registartion/code', async ({ email }, thunkAPI) => {
	interface errorResponse {
		response?: {
			data?: {
				message?: any
			}
		}
	}

	try {
		const response = await axios.post<sendCodeResponse>(
			`${config.api}/auth/email/code`,
			{
				email: email
			}
		)
		if (!response.data?.status) {
			throw new Error('Ошибка отправки сообщения на почту, попробуйте позже')
		}

		return { status: true }

		// @ts-ignore
	} catch (error: errorResponse) {
		const msg =
			error?.response?.data?.message ||
			'Ошибка отправки сообщения на почту, попробуйте позже'
		toast.error(msg)

		return thunkAPI.rejectWithValue(msg)
	}
})
