export interface RegisterSchema {
	email: string
	password: string
	confirmPassword: string
	code?: string
	isLoading?: boolean
	error?: string
	name: string
	id?: string
}
