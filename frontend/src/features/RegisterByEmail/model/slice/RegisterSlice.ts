import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { registerByEmail } from '../services/registerByEmail/registerByEmail'
import { sendCodeByEmail } from '../services/sendCodeByEmail/sendCodeByEmail'
import { RegisterSchema } from '../types/RegisterSchema'

const initialState: RegisterSchema = {
	email: '',
	password: '',
	isLoading: false,
	confirmPassword: '',
	name: ''
}

export const registerSlice = createSlice({
	name: 'register',
	initialState,
	reducers: {
		setEmail: (state, action: PayloadAction<string>) => {
			state.email = action.payload
		},
		setPassword: (state, action: PayloadAction<string>) => {
			state.password = action.payload
		},
		setConfirmPassword: (state, action: PayloadAction<string>) => {
			state.confirmPassword = action.payload
		},
		setCode: (state, action: PayloadAction<string>) => {
			state.code = action.payload
		},
		setName: (state, action: PayloadAction<string>) => {
			state.name = action.payload
		},
		initState: state => {
			state = initialState
		}
	},
	extraReducers: builder => {
		builder
			.addCase(registerByEmail.pending, (state, action) => {
				state.error = undefined
				state.isLoading = true
			})
			.addCase(registerByEmail.fulfilled, (state, action) => {
				state.isLoading = false
			})
			.addCase(registerByEmail.rejected, (state, action) => {
				state.isLoading = false
				state.error = action.payload
			})

			.addCase(sendCodeByEmail.pending, (state, action) => {
				state.error = undefined
				state.isLoading = true
			})
			.addCase(sendCodeByEmail.fulfilled, (state, action) => {
				state.isLoading = false
			})
			.addCase(sendCodeByEmail.rejected, (state, action) => {
				state.isLoading = false
				state.error = action.payload
			})
	}
})

export const { actions: registerAction } = registerSlice
export const { reducer: registerReducer } = registerSlice
