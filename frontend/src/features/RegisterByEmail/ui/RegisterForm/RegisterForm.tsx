import { memo, useCallback } from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'

import { Input } from '@/shared/ui/Input/Input'
import { InputSubmit } from '@/shared/ui/InputSubmit/InputSubmit'

import { getRegisterState } from '../../model/selectors/getRegisterState/getRegisterState'
import { registerByEmail } from '../../model/services/registerByEmail/registerByEmail'
import { registerAction } from '../../model/slice/RegisterSlice'

import styles from './RegisterForm.module.scss'
import { RegisterFormProps } from './RegisterForm.props'

export const RegisterForm = memo(
	({ ...props }: RegisterFormProps): JSX.Element => {
		const dispatch = useDispatch()
		const { email, password, confirmPassword, isLoading, error, name } =
			useSelector(getRegisterState)

		const onChangeEmail = useCallback(
			(email: string) => {
				dispatch(registerAction.setEmail(email))
			},
			[dispatch]
		)

		const onChangePassword = useCallback(
			(password: string) => {
				dispatch(registerAction.setPassword(password))
			},
			[dispatch]
		)

		const onChangeConfirmPassword = useCallback(
			(confirmPassword: string) => {
				dispatch(registerAction.setConfirmPassword(confirmPassword))
			},
			[dispatch]
		)

		const onChangeName = useCallback(
			(name: string) => {
				dispatch(registerAction.setName(name))
			},
			[dispatch]
		)

		const onRegisterAction = useCallback(() => {
			dispatch(
				// @ts-ignore
				registerByEmail({
					email,
					password,
					confirmPassword,
					name
				})
			)
		}, [dispatch, email, confirmPassword, name])

		const {
			register,
			handleSubmit,
			formState: { errors: errors }
		} = useForm()

		const onSubmit = () => {
			if (!Object.keys(errors).length) {
				onRegisterAction()
			}
		}

		return (
			<div className={styles.container} {...props}>
				<div className={styles.containerContent}>
					<p className={styles.title}>Регистрация по почте</p>
					<p className={styles.info}>
						Для создания аккаунта необходимо пройти процедуру регистрации
					</p>

					<form onSubmit={handleSubmit(onSubmit)}>
						<div className={styles.inputContainer}>
							<Input
								type='text'
								className={styles.input}
								placeholder='Email'
								onChangeValue={onChangeEmail}
								value={email}
								label='email'
								register={register}
								options={{
									minLength: {
										value: 4,
										message: 'Минимальная длина символов 4'
									},
									required: 'Поле обязательно для заполнения',
									pattern: {
										value: /^[^@\s]+@[^@\s]+\.[^@\s]+$/,
										message: 'Адрес электронной почты является не валидным'
									}
								}}
								error={errors?.email?.message}
							/>
							<Input
								type='text'
								className={styles.input}
								placeholder='Ваше имя'
								onChangeValue={onChangeName}
								value={name}
								label={'name'}
								register={register}
								options={{
									required: 'Поле обязательно для заполнения'
								}}
								error={errors?.name?.message}
							/>
							<Input
								type='password'
								className={styles.input}
								placeholder='Пароль'
								onChangeValue={onChangePassword}
								value={password}
								label={'password'}
								register={register}
								options={{
									required: 'Поле обязательно для заполнения'
								}}
								error={errors?.password?.message}
							/>
							<Input
								type='password'
								className={styles.input}
								placeholder='Подтвердите пароль'
								onChangeValue={onChangeConfirmPassword}
								value={confirmPassword}
								label={'confirmPassword'}
								register={register}
								options={{
									required: 'Поле обязательно для заполнения'
								}}
								error={errors?.confirmPassword?.message}
							/>
						</div>
						<InputSubmit
							className={styles.btn}
							disabled={isLoading}
							type='submit'
							value={'Зарегистрироваться'}
						/>
					</form>
				</div>
			</div>
		)
	}
)
