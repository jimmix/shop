import cl from 'classnames'
import { useSelector } from 'react-redux'

import { getUserAuthData } from '@/entities/User/model/selectors/getUserAuthData/getUserAuthData'

import { Modal } from '@/shared/ui/Modal/Modal'

import { FerMirWrapper } from '../FerMirWrapper/FerMirWrapper'
import { RegisterForm } from '../RegisterForm/RegisterForm'

import styles from './RegisterModal.module.scss'
import { RegisterModalProps } from './RegisterModal.props'

export const RegisterModal = ({
	isOpen,
	onClose,
	onSwapHandler,
	...props
}: RegisterModalProps): JSX.Element => {
	const authUser = useSelector(getUserAuthData)
	if (authUser?.id && isOpen) {
		onClose()
	}

	return (
		<Modal
			{...props}
			isOpen={isOpen}
			onClose={onClose}
			contentClassName={styles.content}
		>
			<div className={cl(styles.container)}>
				<RegisterForm />
				<FerMirWrapper onSwapHandler={onSwapHandler} />
			</div>
		</Modal>
	)
}
