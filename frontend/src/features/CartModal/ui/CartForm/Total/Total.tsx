import cl from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'

import { cartModalActions } from '@/features/CartModal/model/slice/CartModalSlice'

import { getCartProductState } from '@/entities/Cart/selectors/getCartProductState/getCartProductState'

import { Button } from '@/shared/ui/Button/Button'
import { HTag } from '@/shared/ui/HTag/HTag'

import styles from './Total.module.scss'
import {useCallback} from "react";

export const Total = (): JSX.Element => {
	const items = useSelector(getCartProductState)
	const dispatch = useDispatch()

	const priceDelivery = 199
	const priceProducts = items.reduce((acc, item) => {
		const price = item.product.price
		const count = item.count
		return Number(acc) + Number(price * count)
	}, 0)

	const totalPrice = Number(priceDelivery + priceProducts)

	const isDisabled = totalPrice < 900

	const goToOrder = useCallback(() => {
		if (isDisabled) {
			toast.error('Сумма заказа должна быть больше 900 рублей')
			return
		}

		dispatch(cartModalActions.setIsOpen(false))
	}, [dispatch])

	const closeModal = useCallback(() => {
		dispatch(cartModalActions.setIsOpen(false))
	}, [dispatch])

	return (
		<div className={cl(styles.container)}>
			<HTag tag={'h1'}>Итог</HTag>
			<div className={styles.optionsContainer}>
				<div className={styles.containerOptionPrice}>
					<p className={styles.option}>Стоимость продуктов</p>
					<p className={styles.price}>{priceProducts} ₽</p>
				</div>
				<div className={styles.containerOptionPrice}>
					<p className={styles.option}>Доставка</p>
					<p className={styles.price}>{priceDelivery} ₽</p>
				</div>
				<div className={cl(styles.containerOptionPrice, styles.total)}>
					<p className={styles.option}>Итоговая стоимость</p>
					<p className={styles.price}>
						{Number(priceProducts + priceDelivery)} ₽
					</p>
				</div>
				<Button
					className={styles.btn}
					disabled={isDisabled}
					onClick={goToOrder}
				>
					Оформить заказ
				</Button>
			</div>
			<div className={styles.closeModal} onClick={closeModal}></div>
		</div>
	)
}
