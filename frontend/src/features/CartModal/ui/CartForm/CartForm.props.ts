import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface CartFormProps
	extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}
