import Image from 'next/image'
import {useDispatch, useSelector} from 'react-redux'
import cl from 'classnames'

import {getCartProductState} from '@/entities/Cart/selectors/getCartProductState/getCartProductState'

import {config} from '@/shared/constaints/Config'
import {HTag} from '@/shared/ui/HTag/HTag'

import styles from './History.module.scss'
import {useCallback} from "react";
import {cartActions} from "@/entities/Cart/slice/cartSlice";
import {ProductItem} from "@/entities/Cart/types/Cart";

export const History = (): JSX.Element => {
    const products = useSelector(getCartProductState)
    const dispatch = useDispatch()

    const removeProduct = useCallback((id: string) => {
        dispatch(cartActions.removeProduct(id))
    }, [dispatch])

    const decrementProduct = useCallback((product: ProductItem) => {
        dispatch(cartActions.decrementProduct(product))
    }, [dispatch])

    const addProduct = useCallback((product: ProductItem) => {
        dispatch(cartActions.addProduct(product))
    }, [dispatch])

    return (
        <div className={styles.container}>
            <HTag tag={'h1'}>Корзина</HTag>
            <div className={styles.gridContainer}>
                <div className={styles.breadcrumb}>Продукт</div>
                <div className={styles.breadcrumb}>Количество</div>
                <div className={cl(styles.breadcrumb, styles.totalPrice)}>Итоговая цена</div>
				<div className={styles.breadcrumb}></div>
                {products.map(item => (
                    <>
                        <div key={item.product.id} className={styles.productContainer}>
                            <Image
                                src={`${config.backendStoragePath}/${item.product.imagePath}`}
                                alt={'Изображение товара'}
                                width={140}
                                height={94}
								className={styles.img}
                            />
                            <div>
                                <p className={styles.productName}>{item.product.name}</p>
                                <p className={styles.price}>
                                    {item.product.price + ' / ' + item.product.weight}
                                </p>
                            </div>
                        </div>
                        <div className={styles.countContainer}>
                            <p className={styles.minus} onClick={() => decrementProduct(item.product)}>-</p>
                            <p className={styles.count}>{item.count}</p>
                            <p className={styles.plus} onClick={() => addProduct(item.product)}>+</p>
                        </div>
						<p className={styles.price}>{(Number(item.product.price) * Number(item.count)) + ' ₽'}</p>
                        <div className={styles.close} onClick={() => removeProduct(item.product.id)}>
                            <svg
                                xmlns='http://www.w3.org/2000/svg'
                                width='24'
                                height='24'
                                viewBox='0 0 24 24'
                                fill='none'
                            >
                                <rect
                                    x='5.63672'
                                    y='7.05029'
                                    width='2'
                                    height='16'
                                    rx='1'
                                    transform='rotate(-45 5.63672 7.05029)'
                                    fill='#CDCDCD'
                                />
                                <rect
                                    x='7.05078'
                                    y='18.3638'
                                    width='2'
                                    height='16'
                                    rx='1'
                                    transform='rotate(-135 7.05078 18.3638)'
                                    fill='#CDCDCD'
                                />
                            </svg>
                        </div>
                    </>
                ))}
            </div>
        </div>
    )
}
