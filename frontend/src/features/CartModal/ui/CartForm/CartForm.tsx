import { History } from '@/features/CartModal/ui/CartForm/History/History'
import { Total } from '@/features/CartModal/ui/CartForm/Total/Total'

import styles from './CartForm.module.scss'

export const CartForm = (): JSX.Element => {
	return (
		<div className={styles.container}>
			<History />
			<Total />
		</div>
	)
}
