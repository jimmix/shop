import { useSelector } from 'react-redux'

import { getCartModalState } from '@/features/CartModal/model/selectors/getCartModalState/getCartModalState'
import { CartForm } from '@/features/CartModal/ui/CartForm/CartForm'
import styles from './CardModal.module.scss'
import { Modal } from '@/shared/ui/Modal/Modal'

export const CartModal = ({onClose}: {onClose: () => void}): JSX.Element => {
	const { isOpen } = useSelector(getCartModalState)

	return <Modal onClose={onClose} isOpen={isOpen} contentClassName={styles.content}>{isOpen && <CartForm />}</Modal>
}
