import { PayloadAction, createSlice } from '@reduxjs/toolkit'

import { CartModalSchema } from '@/features/CartModal/model/types/CartModalSchema'

const initialState: CartModalSchema = {
	isOpen: false
}

export const cartModalSlice = createSlice({
	name: 'cartModal',
	initialState,
	reducers: {
		setIsOpen: (state, action: PayloadAction<boolean>) => {
			state.isOpen = action.payload
		}
	},
	extraReducers: builder => {}
})

export const { actions: cartModalActions } = cartModalSlice
export const { reducer: cartModalReducers } = cartModalSlice
