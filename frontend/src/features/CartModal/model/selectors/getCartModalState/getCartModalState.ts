import { StateSchema } from '@/app/providers/StoreProvider/config/StateSchema'

export const getCartModalState = (state: StateSchema) => state.cartModal
